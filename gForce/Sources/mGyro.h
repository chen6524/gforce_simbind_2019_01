/******************************************************************************
**
*  
*
* History:
*
* 11/01/2012 -- first version, ver.1.0  H.YAN
* 
*
*******************************************************************************
*/

#ifndef _MGYRO_H
#define _MGYRO_H
#include "typedef.h"

typedef enum 
{
	GYRO_STATE_NONE,
    GYRO_STATE_INIT,
    GYRO_STATE_WAIT, 
    GYRO_STATE_IDLE,
    GYRO_STATE_READ, 
    GYRO_STATE_CALC,
    GYRO_STATE_SAVE,
    GYRO_STATE_SOFT_RESET
}GYRO_STATE;


typedef enum 
{
    LG_STATE_INIT,
    LG_STATE_CAL_START,
    LG_STATE_CAL_CAC,
    LG_STATE_CAL_END,
    LG_STATE_IDLE
    
    
}LOWG_STATE;

typedef struct 
{
	int16_t x;
	int16_t y;
	int16_t z;

} GYRO_DATA;

typedef struct 
{
	int16_t x;
	int16_t y;
	int16_t z;

} LOWG_DATA;

typedef struct 
{
	GYRO_DATA maxAcc;
	GYRO_DATA fifoData[32];

} GYRO_INFO;

extern GYRO_INFO gyroInfo[];
extern volatile byte lgIsrState;
extern LOWG_DATA lgAccV;

void gyroInit(void);
byte gyroGet(void);
byte gyroCalc(void);
void lowGTask(void);
void lgISR (void);
byte getMaxThresh(void);                              // Added by Jason Chen for preventing wrong recording , 2014.05.06 
void lgAccGetData(void);                                  // Added by Jason Chen for preventing wrong recording , 2014.05.06 

byte lgAccGet(void);
byte lgGyroGet(void);


#endif

