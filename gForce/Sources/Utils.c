/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "gft.h"
#include <hidef.h>
#include "rtc.h"
#include "Utils.h"
#include "RTC_Drv.h"


static byte urtcTime[7]; 
void mTimer_TimeSet (unsigned int *pTimer ) 
{     
     
     DisableInterrupts;
     *pTimer = g_TickSecond;
     EnableInterrupts;        
}

unsigned char mTimer_TimeExpired( unsigned int *pTimer, unsigned int time  ) 
{      
     unsigned int tmpTimer;
     DisableInterrupts;
     tmpTimer = g_TickSecond;
     EnableInterrupts; 
     return ((tmpTimer -*pTimer >= time) ? 1 : 0);	
}


#if 0  // for comparing purpose, 2014.06.24
void CYFISNP_NODE_TimeSet1(WORD *pTimer, WORD time)               // 32767 counts maximum    
{
    WORD tmpTime;
    
    tmpTime = SleepTimer_TickCount;
    
    *pTimer = (WORD)(tmpTime + time);
    if( *pTimer == 0 )
        *pTimer = 1;
}

BOOL CYFISNP_NODE_TimeExpired1(WORD *pTimer) 
{
    WORD tmpTime;
    if( *pTimer == 0 )
        return TRUE;

    tmpTime = SleepTimer_TickCount;
    tmpTime = (WORD)(*pTimer - tmpTime);
    
    if (((BYTE)(tmpTime>>8)&0x80) == 0)
        return FALSE;   // not expired

    *pTimer = 0;
    return TRUE;    // expired
}
#endif


//////////////////////////////////
unsigned int CRC_Value;

void CalculateCRC(unsigned char * Buf, unsigned char Len)
{
 	unsigned char Index1, Index2;
  
	CRC_Value = PRESET_VALUE;
    
	for (Index1=0; Index1 < Len; Index1 ++)
	{
			CRC_Value = CRC_Value ^ (unsigned int) Buf[Index1];
			
			for (Index2 = 0; Index2 < 8; Index2++)
			{
					if (CRC_Value  & 0x0001)
						CRC_Value = (CRC_Value >> 1) ^ POLYNOM;
					else
						CRC_Value = (CRC_Value >> 1);
			}
	}
	
	//The last calculated value
	CRC_Value = ~ CRC_Value;
}

void MakeCRC(unsigned char * Buf, unsigned char Len)
{
  CalculateCRC(Buf, Len - 2);
  
  Buf[Len-2] = CRC_Value & 0xFF;
  Buf[Len-1] = (CRC_Value >> 8) & 0xFF;
}

unsigned char VerifyCRC(unsigned char * Buf, unsigned char Len)
{
  CalculateCRC(Buf, Len);
  return CRC_Value == ~CHECK_VALUE;
}

#if 0
unsigned long ISqrt(unsigned long lv)
{
    unsigned long lr = lv >> 1;
    unsigned char n;

    for(n=32; n>0; n--)
    {
        if((lv & (1 << (n-1))) != 0)
        {
            break;
        }
    }

    lr = 1 << (n / 2 + 1);
    
    if(lr == 0) lr = 1;
    
    for(n=0; n<16; n++)
    {
        if(lv == lr * lr)
        {
            break;
        }
        
        if(lr != 0)
        {
            lr = (lr + lv / lr) / 2;
        }
    }
    
    return lr;
}
#endif
/* From wikipedia
if year modulo 400 is 0 then leap
 else if year modulo 100 is 0 then no_leap
 else if year modulo 4 is 0 then leap
 else no_leap
*/
static unsigned char IsLeapYear(unsigned char y)
{
    unsigned int ty;
    
    ty = 2000 + y;
    
    if((ty % 400) == 0)
    {
        return TRUE;
    }
    else if((ty % 100) == 0)
    {
        return FALSE;
    }
    else if((ty % 4) == 0)
    {
        return TRUE;
    }

    return FALSE;
}

const unsigned char DaysPerMonth[]=
{ // 1   2   3   4   5   6   7   8   9  10  11  12
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
};

unsigned char GetTotalDaysInMonth(unsigned char ThisYear, unsigned char ThisMonth)
{
    if(IsLeapYear(ThisYear) && (ThisMonth == 2))
    {
        return 29;
    }

    return DaysPerMonth[ThisMonth - 1];
}


unsigned char IsValidDateTime(unsigned char year, unsigned char month, unsigned char day, 
                              unsigned char hour, unsigned char minute, unsigned char second)
{      
    if( (year <100) &&   (month <13) &&  (day <= GetTotalDaysInMonth(year, month)) &&   (hour<25)
        && ( (minute <60)   && (second <60)))
        return TRUE;
    else 
        return FALSE;
}

void initBuzz(void)
{
}

void rtcAdjust(unsigned char mSecond)
{
 
 uint16_t tmpS =0;
 uint16_t tmp =0;
 uint16_t tmpM =0;
 uint8_t s =0;
 uint8_t second;
 second  = mSecond&0x7F;
 
 if( mSecond < 128)
 {  
 	    
 	  tmpS = myRTC.Second+second;
		tmp = tmpS%60;
		myRTC.Second = tmpS-tmp*60;
		tmpM =myRTC.Minute +tmp;
		
    if(tmpM >=60)
    {
			myRTC.Minute = tmpM -60;

			if(myRTC.Hour < 24)
			{
				myRTC.Hour++;
			}
			else
			{
				
				myRTC.Hour = 0;
			  if(GetTotalDaysInMonth(myRTC.Year, myRTC.Month)  > myRTC.Day)
					myRTC.Day++;
				else 
				{
				  myRTC.Day = 1;
					if(myRTC.Month <12)
						myRTC.Month++;
					else
					{
						myRTC.Month = 1;
						myRTC.Year++;
					}
				}
			}
					
        }
		else
		{
				myRTC.Minute = tmpM;
		}        
 }
 else
 {	 	
	 tmpM = tmpS%60;
	 tmpS = second - tmp *60;
	 if(tmpS > myRTC.Second)
	 {
	    tmpM++;
	    myRTC.Second = myRTC.Second +60 - tmpS;
	 }
	 else 
	 	  myRTC.Second  = myRTC.Second -tmpS;
	 	 	 
	 if(myRTC.Minute >= tmpM)
	 {
		  myRTC.Minute -=tmpM;
	 }
	 
	 else
	 {
		 myRTC.Minute = myRTC.Minute + 60 - tmpM;
	 
		 if(myRTC.Hour >= 1)
		 {
			 myRTC.Hour--;
	 
		 }
		 else
		 {			 
			 myRTC.Hour = 23;
			 if( myRTC.Day > 1)
			     myRTC.Day--;
			 else 
			 {
			     if(myRTC.Month >1)
			     {
			     	 s = GetTotalDaysInMonth(myRTC.Year, myRTC.Month-1);
				 	   myRTC.Day = s;
			     }
				   else
				 	 {
				 	   myRTC.Year--;
					   myRTC.Month =12;
				 	 }				 				
			 }
		 }				 
	}	 
 }
 urtcTime[0] = myRTC.Year;
 urtcTime[1] = myRTC.Month;
 urtcTime[2] = myRTC.Day ;
 urtcTime[3] = 0;
 urtcTime[4] = myRTC.Hour ;
 urtcTime[5] = myRTC.Minute;
 urtcTime[6] = myRTC.Second;
 
 Set_YearMonthDay((byte *)&urtcTime);	  
 g_Tick1mS =0;
}

#if 0
char rtcPwrOff(byte hour)
{
  char rV = 0;
  if(myRTC.Minute -pwrDownRTC.Minute >2)
	  rV  =1;
  return rV;


  if(myRTC.Hour > pwrDownRTC.Hour)
  {
	  if(myRTC.Hour - pwrDownRTC.Hour >=hour) 
		  rV =1;
  }
  else
  {
    if( 24 - pwrDownRTC.Hour + myRTC.Hour >=hour) 
		rV =1;
  }
  return rV;
}
#endif
