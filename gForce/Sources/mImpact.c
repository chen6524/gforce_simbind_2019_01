/********************************************************************************
*  
* High G accelerometer 
* History:
*
* 11/01/2012 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/

#include "gft.h"
#include "mcu.h"
#include "LSM330DLC.h"
#include <string.h>
#include "mImpact.h"
#include "adc.h"
#include "mFlash.h"
#include "mHid.h"
#include "rtc.h"
#include "utils.h"
#include "gLp.h"
#include "kbi.h"
//Faraz
#include "radio.h"
#include "MAX44000.h"
#include "max14676.h"

#include "bcd_common.h"                        // Added by Jason Chen for time stamp fix, 2013.12.19


#define ACC_RAM_STATE_FULL 0x1
#define ACC_RAM_STATE_CURBUF_HASDATA 0x2
#define ACC_RAM_STATE_CURBUF_FULL 0x4

//extern byte powerCommand;   

static byte accRamState =0;
//test
//word testMsS = 0;
//word testMsE = 0;

static ACC_BD accBD[ACC_MAX_BD_NUM];
ACC_ENTRY accBuff[ACC_MAX_BD_NUM]; 

byte accHitStat = ACC_HIT_STAT_INIT;
byte accHitCnt =0;
byte accHitPreCur =0;
byte accHitPreTotal = 0;
ACC_BD *pAccNowR;
ACC_BD *pAccNowW;
//WORD accMaxValue;
volatile byte accHitOn =0;

GFT_PROFILE usrProfile;

//extern byte field_xmit_enable;                       // Added by Jason Chen for Recording Control, 2014.08.05
extern byte  upload_mode;

void SetUsrProfileLP_Disable(void)                     // Added by Jason Chen for Recording Control, 2019.02.07
{
   LED_BLUE_On();
   Cpu_Delay100US(5000);                               
   usrProfile.lpEnable = GMODE_LP_DISABLE;    
   needSaveProfile     = 1;					                      
   needReboot          = 1;                                
}

int accInitBDs(void)
{
	byte i;
	for ( i = 0; i< ACC_MAX_BD_NUM; i++)
	{
	    accBD[i].pEntry = &accBuff[i];
	    accBD[i].next =   (void *)&accBD[i+1];
		  accBD[i].length = 0; 
		  accBD[i].cstatus = 0;
		  accBD[i].pEntry->offset.size =0;
		  accBD[i].pEntry->maxIndex =0;
		  accBD[i].pGyroInfo = &gyroInfo[i];
	}
	accBD[ACC_MAX_BD_NUM-1].next = (void *)&accBD[0];
	
	pAccNowR = pAccNowW = &accBD[0];
	//accMaxValue._word = 0;
	return(E_OK);
} 

uint32_t gActiveTime;

// Changed by Jason Chen,   2015.02.06		   
#define RECORD_ACTIVE_TIME \
		   if(motionDetected)gActiveTime++; 
		   
// Added by Jason Chen,   2015.02.06		   
#define RECORD_ACTIVE_TIME2 \
		   gActiveTime++; 	   

void imSaveDataItemToEntry(byte type)
{
  volatile byte rtcV =0;   
  if(accRamState & ACC_RAM_STATE_FULL)
  	 return;  // out of ram  and get out of here  	 

  if (gftInfo.proxStat)
  {  
   //if(!ProxIntState)  
     if((!ProxIntState)||(!field_xmit_enable))           // Added by Jason Chen,   2014.08.05     
     {
      accHitStat = ACC_HIT_STAT_INIT;
      return;
     }
  } 
  else                                                  // Added by Jason Chen,   2014.08.05
  {                                                     // 
      if(!field_xmit_enable)                            // 
      {                                                 // 
        accHitStat = ACC_HIT_STAT_INIT;                 // 
        return;                                         // 
      }                                                 // 
  }                                                     // Added by Jason Chen,   2014.08.05

  switch(accHitStat)
  {
	case ACC_HIT_STAT_PRE:
		if(motionDetected )
          RECORD_ACTIVE_TIME2
        //motionDetected =0;                               // Added by Jason Chen,   2015.02.06
		
		if(type == ACC_HIT_DATA_PRE) 
		{
    #if 0		
			 pAccNowW->pEntry->data[accHitPreCur].x = accData.x;
	     pAccNowW->pEntry->data[accHitPreCur].y = accData.y;
	     pAccNowW->pEntry->data[accHitPreCur].z = accData.z;
    #else	                                                                 // Changed by Jason Chen, 02.06     
			 pAccNowW->pEntry->rtcAcc.data[accHitPreCur].x = accData.x;
	     pAccNowW->pEntry->rtcAcc.data[accHitPreCur].y = accData.y;
	     pAccNowW->pEntry->rtcAcc.data[accHitPreCur].z = accData.z;
    #endif	     
	     
			 accHitPreCur++;       
       if(accHitPreTotal < ACC_HIT_PRE_CNT)
         accHitPreTotal++;			 
			 accHitPreCur %=ACC_HIT_PRE_CNT;
			 break;
		}
		else
		{		
            //testMsE = g_Tick; 
			accHitStat = ACC_HIT_STAT_PST_S;
			pAccNowW->pEntry->offset.size = accHitPreCur;
		}
	
	case ACC_HIT_STAT_PST_S:
	  //RECORD_ACTIVE_TIME
	  RECORD_ACTIVE_TIME2                                                 // Changed by Jason Chen, 2015.02.06
      if(accHitPreTotal< ACC_HIT_PRE_CNT)
      {
      #if 0      
          pAccNowW->pEntry->data[accHitPreCur].x = accData.x;
          pAccNowW->pEntry->data[accHitPreCur].y = accData.y;
          pAccNowW->pEntry->data[accHitPreCur].z = accData.z;
      #else                                                                   //Changed by Jason Chen, 2015.02.06
          pAccNowW->pEntry->rtcAcc.data[accHitPreCur].x = accData.x;          
          pAccNowW->pEntry->rtcAcc.data[accHitPreCur].y = accData.y;
          pAccNowW->pEntry->rtcAcc.data[accHitPreCur].z = accData.z;
      #endif                    
	        accHitPreCur++;
          accHitPreCur %=ACC_HIT_PRE_CNT;
          accHitPreTotal++;
          break;
      }
      else  
      {
			    accHitStat = ACC_HIT_STAT_PST;			  
			    pAccNowW->pEntry->offset.size = accHitPreCur;
				
      }
		
    case ACC_HIT_STAT_PST:
	  //RECORD_ACTIVE_TIME
	  RECORD_ACTIVE_TIME2                                                 // Changed by Jason Chen, 2015.02.06
	  #if 0
      pAccNowW->pEntry->data[accHitCnt+ACC_HIT_PRE_CNT].x = accData.x;
	    pAccNowW->pEntry->data[accHitCnt+ACC_HIT_PRE_CNT].y = accData.y;
	    pAccNowW->pEntry->data[accHitCnt+ACC_HIT_PRE_CNT].z = accData.z;
	  #else                                                                        //Changed by Jason Chen, 2015.02.06
      pAccNowW->pEntry->rtcAcc.data[accHitCnt+ACC_HIT_PRE_CNT].x = accData.x;
	    pAccNowW->pEntry->rtcAcc.data[accHitCnt+ACC_HIT_PRE_CNT].y = accData.y;
	    pAccNowW->pEntry->rtcAcc.data[accHitCnt+ACC_HIT_PRE_CNT].z = accData.z;
    #endif	    
	    
	    accHitCnt++;
	    if(accHitCnt >= ACC_HIT_PST_CNT)
	    {
	    
	     (void)gyroGet();
		 __RESET_WATCHDOG();  
		 
#if  ENABLE_FAKE_IMPACT_FILTER	    
       if(  !motionDetected) 
		   {                                                        // Added by Jason Chen for preventing wrong recording , 2014.05.06 
	         accHitStat = ACC_HIT_STAT_INIT;                      // Added by Jason Chen for preventing wrong recording , 2014.05.06 
	         __RESET_WATCHDOG();                                  // Added by Jason Chen for preventing wrong recording , 2014.05.06 
	         pAccNowW->pEntry->magic = 0;
	         pAccNowW->cstatus =  0;
	      }                                                       // Added by Jason Chen for preventing wrong recording , 2014.05.06 
	      else                                                    // Added by Jason Chen for preventing wrong recording , 2014.05.06 
#endif	      
	      {	        	    	    	    
  	  	 //accRamState |= ACC_RAM_STATE_CURBUF_FULL;
  		     accHitStat = ACC_HIT_STAT_END;
  		   //let read the gyro 
  		   //LED_TORCH_On();
  		   
  		     
         //LED_TORCH_Off();                           
         //rtcV = getRtcStamp();                              //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
  		   //if(!rtcV)                                          //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
         //{                                                  //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
         //   accHitStat = ACC_HIT_STAT_INIT;                 //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
  		   //}                                                  //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
         //else                                               //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
           {
  		        pAccNowW->pEntry->magic = ACC_ENTRY_MAGIC;
  	          (void)memcpy((void *)&pAccNowW->pEntry->t, (void *)&myRTC, sizeof(MY_RTC));	          	          
              pAccNowW->pEntry->t.StartMS = g_Tick1mS;	   
           }
	      }
	    }
	    break;
	//case ACC_HIT_STAT_WAIT:
	//	if (type == ACC_HIT_DATA_PRE)
	//		 accHitStat = ACC_HIT_STAT_END;
    //break;
    case ACC_HIT_STAT_END:
		     pAccNowW->cstatus =  ACC_BUFF_CSTAT_FULL_MSK;
		   //pAccNowW->pEntry->t.EndMS = g_Tick1mS;
		     pAccNowW = (ACC_BD *)pAccNowW->next;
	
		     if( pAccNowW->cstatus & ACC_BUFF_CSTAT_FULL_MSK)
		     {
			     accRamState = ACC_RAM_STATE_FULL;
		     }
		     accHitStat = ACC_HIT_STAT_INIT;
	       break;
	    
   case ACC_HIT_STAT_INIT:
		    //rtcV = getRtcStamp();                                //Commented by Jason Chen according to Harry Latest Version, 2014.04.22
        //(void)memset(pAccNowW->pEntry->data, 0x80, ACC_HIT_DATA_LEN*sizeof(ACC_DATA));
        (void)memset(pAccNowW->pEntry->rtcAcc.data, 0x80, ACC_HIT_DATA_LEN*sizeof(ACC_DATA));  // Changed by Jason Chen, 2015.02.06
		    accHitStat = ACC_HIT_STAT_PRE;
		    accHitPreCur =0;
		    accHitCnt =0;
        accHitPreTotal =0;
		    lowGEnable = usrProfile.tR < LOWG_HIGHG_MERGE_THRESHOLD ? 1:0;
		    motionDetected = 0;
	      break;    
  }

}

ACC_DATA tmpBuffer[ACC_HIT_PRE_CNT];
static word AccFindRow(ACC_ENTRY *entry)
{

	byte row;
	byte i,j,c;
	word tMax;
	word tmp;
	row =0;
	tMax = 0;
	j = entry->offset.size;
	c = ACC_HIT_PRE_CNT - j;
	
	if (j  == 0)
		//(void)memcpy((void *)tmpBuffer, (void *)entry->data, ACC_HIT_PRE_CNT* sizeof(ACC_DATA));
	  (void)memcpy((void *)tmpBuffer, (void *)entry->rtcAcc.data, ACC_HIT_PRE_CNT* sizeof(ACC_DATA));   // Changed by Jason Chen, 2015.02.06
		
	else
	{
	#if 0
		(void)memcpy((void *)&tmpBuffer[c], (void *)entry->data, (ACC_HIT_PRE_CNT - c) * sizeof(ACC_DATA));
		(void)memcpy((void *)tmpBuffer, (void *)&entry->data[j], c* sizeof(ACC_DATA));
	#else	                                                                                                        // Changed by Jason Chen, 2015.02.06
		(void)memcpy((void *)&tmpBuffer[c], (void *)entry->rtcAcc.data, (ACC_HIT_PRE_CNT - c) * sizeof(ACC_DATA));
		(void)memcpy((void *)tmpBuffer, (void *)&entry->rtcAcc.data[j], c* sizeof(ACC_DATA));
	#endif
		
	}
	
	//(void)memcpy((void *)entry->data, (void *)tmpBuffer, ACC_HIT_PRE_CNT * sizeof(ACC_DATA));
	(void)memcpy((void *)entry->rtcAcc.data, (void *)tmpBuffer, ACC_HIT_PRE_CNT * sizeof(ACC_DATA));       // Changed by Jason Chen, 2015.02.06
	
	for ( i =0; i< ACC_HIT_DATA_LEN; i++)
	{
        //tmp = accGetSum(entry->data[i].x,entry->data[i].y,entry->data[i].z);
        tmp = accGetSum(entry->rtcAcc.data[i].x,entry->rtcAcc.data[i].y,entry->rtcAcc.data[i].z);       // Changed by Jason Chen, 2015.02.06
        if (tmp >= tMax) 
        {
			row = i;
			tMax = tmp;
        }
	}
	entry->maxIndex = row;
	return tMax;

}

word AccCalc(ACC_ENTRY *entry)
{
	word acc;
	//ACC_DATA *xyz;
	//byte i;
	
	acc = AccFindRow(entry);
	//entry ->maxIndex= i;
	//xyz = &entry->data[i];
	//acc = (word) (accGetAbs(xyz->x) * accGetAbs(xyz->x) + 
	//	accGetAbs(xyz->y) * accGetAbs(xyz->y) + accGetAbs(xyz->z) * accGetAbs(xyz->z));
    //acc = IntegerSqrt(acc);
	#ifdef ENABLE_ENERGY_ME
	if(acc < MMA68_HIT_THR)
	{
		entry->magic = ACC_ENTRY_MAGIC_EE;
		entry->ee.x = entry->dataE[i].x;
		entry->ee.y = entry->dataE[i].y;
		entry->ee.z = entry->dataE[i].z;
	}
	#endif
	entry->mag = acc;
	
	return acc;
}

IMPACT_STATE imState=IM_STATE_SOFT_RESET;
volatile byte myTest;
volatile byte accLive =0;

dword acc2;
byte field_xmit_enable = 1;           // Added by Jason Chen for the control of field xmit, 2014.03.19
extern uint8_t  max17047Exist;        // Added by Jason Chen for, 2014.04.22
extern uint16_t max17047Voltage;      // Added by Jason Chen for, 2014.04.22

word gSleepCnt =0; 
//Faraz
#define USB_OFF       0
#define USB_ON        1
//extern bool Usb_Mode;                    // Commented by Jason Chen because don't need it in this module, 2013.12.13
extern byte Message_Registered;
                                            

word imSleepTimer = 0;
//word imSleepSendTimer = 0;

word imHibernateTimer = 0;
word imSleepTimerTime = IM_SLEEP_TIMER;
byte lpTxEnable = 0;

void imTask(void)
{
    //Faraz 
    byte Return_Value = 0;
    dword wTemp;
    static byte waitLoop = 0;
	//static ACC_STATE lowPriState = ACC_STATE_NONE;
	word accT = 0;
	byte alarmCoefficent =100;
	
	   
	//byte i;
    //
  switch(imState)
  {
	  case IM_STATE_SOFT_RESET:
		    imState=IM_STATE_INIT;
		    break;
	  case IM_STATE_INIT:
		  //IRQSC_IRQACK = 1; //clear irq
		  //Spi2Init();
		    if(usrProfile.mntLoc <2 || usrProfile.mntLoc >=100)
				alarmCoefficent =100;
			else 
				alarmCoefficent = usrProfile.mntLoc;
			
		    wTemp = usrProfile.tAlarm * 100;
			wTemp = wTemp/alarmCoefficent;
		    acc2 = (word)(wTemp * wTemp * ACC_RESOLUTION_SQR);//((MMA68_G_LSB*4)*(MMA68_G_LSB*4));
		    
		    (void)spiFlashGetCurWtAddr();
		    imState = IM_STATE_WAIT;
		    break;
	  case IM_STATE_WAIT:
		    if( waitLoop++ > WAIT_AFTER_RESET)
		    {			
			     imState=IM_STATE_IDLE;
		    }
		    break;		
	  case IM_STATE_IDLE:	
        if(!gftInfo.pwrMode) 
        { 
            mTimer_TimeSet(&imSleepTimer );
            mTimer_TimeSet(&imHibernateTimer );    
        }
        imState=IM_STATE_SLEEP_WAIT;  
    case IM_STATE_SLEEP_WAIT:
        if((pAccNowR->cstatus & ACC_BUFF_CSTAT_FULL_MSK))
		    {	
			     imState=IM_STATE_CALC;
		    }
        //WARNING
        #if  ENABLE_SLEEP_MODE                                                // Jason Chen, Enable Sleep, Sep.12, 2013
        else if(!gftInfo.pwrMode) 
        { 
            #if 0
            if(mTimer_TimeExpired( &imHibernateTimer,IM_HIBERNATE_TIMER))
            {
                needHibernate =1;
            }
            #endif
            
            if(mTimer_TimeExpired( &imSleepTimer, imSleepTimerTime ) )
            {                     
                //gftInfo.lpBusy = FALSE;                                                       // Added by Jason Chen, 2014.05.26                
                //if(!gftInfo.lpBusy ) 
                {
           #if ENABLE_SLEEP_PACKET_SEND                
                    mTimer_TimeSet( &imSleepTimer );                                           // Added by Jason Chen, 2014.01.15 for Sending enter Sleep mode MSG
                                   
                    Power_Off_Message.gid[0] = 0x01;                                           // 0x01 means Sleep Packet, added by Jason Chen, 2014.01.15                                                     
                    Power_Off_Message.gid[1] = max17047Exist|max14676Exist;                             // added battery level value into the special Packet, 20140314   
                    Power_Off_Message.gid[2] = (byte)(batData1>>8);                            
                    Power_Off_Message.gid[3] = (byte)batData1;
                    Power_Off_Message.gid[4] = (byte)(spiFlashCurRdBuffer.accEntryMagicCnt>>8); 
                    Power_Off_Message.gid[5] = (byte)spiFlashCurRdBuffer.accEntryMagicCnt;   
                    if(max17047Exist) 
                    {                      
                      Power_Off_Message.gid[6] = (byte)(max17047Voltage>>8); 
                      Power_Off_Message.gid[7] = (byte)max17047Voltage;                                                 
                    } 
                    else if(max14676Exist)                                                     // Added by Jason Chen, 2016.01.11
                    {                                                                          // Added by Jason Chen, 2016.01.11
                      Power_Off_Message.gid[6] = (byte)(max14676Voltage>>8);                   // Added by Jason Chen, 2016.01.11
                      Power_Off_Message.gid[7] = (byte)max14676Voltage;                        // Added by Jason Chen, 2016.01.11                                             
					  Power_Off_Message.batPT =  (byte)max14676Charge;
					}                                                                          // Added by Jason Chen, 2016.01.11
                    Power_Off_Message.accEntryCnt = spiFlashCurRdBuffer.accEntryCnt;           // Added by Jason Chen, 2016.01.11
                                                                                                                      
                  //lpCmdMail = LP_CMD_PWR_ON;
                    lpCmdMail = LP_CMD_OFF;                // 0x57 for testing, 20140115             
                    lpState   = LP_STATE_IDLE;             
                    
                  //imState   = IM_STATE_IDLE;                                  
                    imState   =IM_STATE_SENDING_SLEEP_MSG;                      
            #else
                    imState = IM_STATE_SLEEP;
            #endif       
			        //if(rtcPwrOff(4))
			       // {                                                   
                   //     powerCommand = 1;   
			       // }
                }                             
            }
            else
            {
                if(IRQSC_IRQF)
                {
                    mTimer_TimeSet( &imSleepTimer );     
					//pwrDownRTC = myRTC;
					pwrOffCnt =0;
                    IRQSC_IRQACK = 1;
                }
            }
        }
        #endif
	      break;
	      
#if ENABLE_SLEEP_PACKET_SEND	      
    case IM_STATE_SENDING_SLEEP_MSG:
        
        if(mTimer_TimeExpired( &imSleepTimer, 3 ))     // Added by Jason Chen, 2014.01.15 for Sending "enter Sleep mode" Packet 
        {
           //if(!gftInfo.lpBusy ) 
           {            
    		     imState = IM_STATE_SLEEP;                          
    		     buzzerOff();                              // Added by Jason Chen, 2014.04.14
           }
        } 
        else 
        {
            if(IRQSC_IRQF)                             // Added by Jason Chen, 2014.03.20, not enter Sleep mode MSG
            {
                mTimer_TimeSet( &imSleepTimer );
                IRQSC_IRQACK = 1;
                
                imState=IM_STATE_SLEEP_WAIT;  
                
                Power_Off_Message.gid[0] = 0x80;       // New Battery level report, 2014.03.20
                lpCmdMail = LP_CMD_OFF;
                lpState=LP_STATE_IDLE;          
            }            
        }
                             
        break;
#endif                        
    case IM_STATE_SLEEP:
		
		
     #if POWER_ON_BAT_REPORT                           // Added by Jason Chen for sending Power-On Packet periodically, 2013.11.26
        lpCmdMail = LP_CMD_PWR_ON;                     
        lpState   = LP_STATE_IDLE;          
          
        imState=IM_STATE_IDLE;
     #else
		    DisableInterrupts;
		    pAccNowR->cstatus = 0;
		    EnableInterrupts;
            motionDetected =0;
        ResetADCTimer(); 
		    gotoStop3();  
			(void)getRtcStamp();
      //reset previous recording 
        imState=IM_STATE_IDLE;
   
     #endif		             
	      break;

	  case IM_STATE_CALC:	
		    __RESET_WATCHDOG();
        accT = AccCalc(pAccNowR->pEntry);
		 
		    (void)gyroCalc();
		 
		    if( (usrProfile.AlarmMode & PROFILE_ALARM_EN_MSK) && acc2 <= accT )
		    {
		        if(profile_alarm_au_backup & PROFILE_ALARM_AU_MSK)
		    
		        //accHitOn =1; //turn on alarm   
		        usrProfile.AlarmMode |= PROFILE_ALARM_ON_MSK;
		        if(usrProfile.AlarmMode & PROFILE_ALARM_LOCK_MSK)
		  	      needSaveProfile = 1;     // Added by Jason Chen, 2014.11.26
		        //usrProfile.AlarmMode |= PROFILE_ALARM_AU_MSK;		     
		      //accMaxValue._word = accT;
		     #ifdef  ENABLE_RADIO               
            (void)memcpy((byte *)&newRTC, (void *)&myRTC, sizeof(MY_RTC));   // Added by Jason Chen, 2015.02.06
         #endif        	
	      }	
		   imState=IM_STATE_SAVE;	
		   break;
	  case IM_STATE_SAVE: //take 0.6ms/256 bytes
        // save data to flash
        // 
      __RESET_WATCHDOG();  
        LED_YELLOW_On();
		
        (void)spiFlashWriteAccBuffer(pAccNowR->pEntry);
		    LED_YELLOW_Off();
		    DisableInterrupts;
		    pAccNowR->cstatus =0;
        pAccNowR->pEntry->magic =0;
		  //pAccNowR->pEntry->offset.timeOff =sessionStartcnt;
		    accRamState =0;
		    EnableInterrupts; 
		    pAccNowR = pAccNowR->next;
		    imState=IM_STATE_IDLE;
		    
        spiMyFlashGetCurWtAddr();                                                            // Added by Jason for Summary Impact Count sent out, 2015.01.08
		  //update session info
		  		  
#if 1
	     ///////////////////////////////////////////////// Added by Jason for Summary sent out, 2015.01.08	     
        lpCmdMail = LP_CMD_SUMMARY;
        //lpCmdMail = LP_CMD_RPT;
       /////////////////////////////////////////////////
#endif	      	      
		  
		  
        break;
	  default:
		    imState=IM_STATE_INIT;
		    break;
  }	
}
