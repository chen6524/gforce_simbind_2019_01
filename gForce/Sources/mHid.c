
/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
* 
********************************************************************************/

#include <MC9S08JM60.h>
#include "typedef.h"
#include "mhid.h"
#include "Usb_drv.h"
#include "kbi.h"
#include "rtc.h"
#include "mFlash.h"
#include <string.h>
#include "mImpact.h"
#include "LSM330DLC.h"
#include "LSM6DS3.h"
#include "MAX44000.h"
#include "rtc_drv.h"
#include "adc.h"
#include "mcu.h"
#include "gft.h"
#include "gLp.h"
#include "mGyro.h"
#include "gLp.h"
#include "Radio.h"
#include "MAX14676.h"                       // Added by Jason Chen, 2016.01.11

#pragma MESSAGE DISABLE C1420


char RecBuf[HID_INT_OUT_EP_SIZE];
char RptBuf[HID_INT_IN_EP_SIZE]={  0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
	                                 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
	                                 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
	                                 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
	                                 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
	                                 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,
	                                 0x00,0x01,0x02,0x03
	                             };

byte hidTimer;
#define HID_TIMER    40

byte needSaveProfile =0;
byte needReboot =0;
byte lpNeedUnbind = 0;
byte needHibernate =0;
byte needEraseFlash =0;
byte pcConnected  = 0;
static byte flash(byte cmd, byte *adr, byte dat);

//static byte myNvramWrite(byte *addr, byte *src, byte len);
//test code
//#define HYAN_TEST

#ifdef HYAN_TEST
ACC_ENTRY test_entry;
#endif

#define CMD_BPROG   0x20
#define CMD_ERASE   0x40
#define ERR_SUCCESS 0
#define ERR_FLASH   1
#define ERR_VERIFY  2

typedef byte(*pFct) (byte,byte*,byte);

byte FIMG_ADDR[64];

GFT_PROFILE usrProfileFlash @ 0x1A00; 

byte gHardInfoFlash[] @0x1960 = {0xFF,0xff,0xff,0xff,0xff,0xff}; //mid

//software major, software minor
//
//
//
//
//Oct 14 2014 3.0: - fix lab test option bug
//                 - merge lowG and high G
//                 
const byte ProductString[] @0x1E00 = {'g','F','T',0x04,0x11};    // Firmware Version
#ifndef BOOTABLE_IMAGE 
//testing purpose
const byte bootloaderHeaderData[]@0xF000 = {'g','F','T',0x01,0x0,0x04,0x0}; //software Major, minor,hardware Major, minor
#endif
byte *bootloaderHeader =(byte*)0xF000;

#define FIMG_SIZE   64  

byte getHardIDMajor(void)
{
     return bootloaderHeader[5];

}
byte getHardIDMinor(void)
{
     return bootloaderHeader[6];
}


static byte flash(byte cmd, byte *adr, byte dat) // 0x3f=63
{
    while( FSTAT_FCBEF==0 )
        ;   // ensure the address, command and data buffers are empty
    // clear any errors
    FSTAT = FSTAT_FACCERR_MASK | FSTAT_FPVIOL_MASK;
    *adr = dat;         // buffer the address and data
    FCMD = cmd;         // issue the command
    FSTAT_FCBEF = 1;    // reset the flag
    // check whether the command sequence was ok
    if( FSTAT_FACCERR || FSTAT_FPVIOL )
        return ERR_FLASH;

    while( FSTAT_FCCF==0 )
        ;   // wait until CCF is cleared and CBEF is set
    return ERR_SUCCESS;
}

byte myNvramWrite(byte *addr, byte *src, byte len)
{
    byte i;
    pFct pflash;
    byte retVal;
    
    DisableInterrupts;
    retVal = ERR_SUCCESS;

    // check if already the same
    if( memcmp(addr, src, len)==0 ) {
        retVal = ERR_SUCCESS;
        goto done;
    }
    #pragma MESSAGE DISABLE C1805

    // copy the function to RAM
    pflash = (pFct)FIMG_ADDR;
    (void)memcpy((void*)pflash, (void*)flash, FIMG_SIZE);

    // Step 1: Erase (512 bytes)
    if( (*pflash)(CMD_ERASE, addr, 0)!=ERR_SUCCESS ) {
        retVal = ERR_FLASH;
        goto done;
    }

    // Step 2: Write the data
    for(i=0; i<len; i++)
        if( (*pflash)(CMD_BPROG, &addr[i], src[i])!=ERR_SUCCESS ) {
            retVal = ERR_FLASH;
            goto done;
        }

    // Step 3: Verify
    if( memcmp(addr, src, len) ) {
        retVal = ERR_VERIFY;
        goto done;
    }

done:

    EnableInterrupts;
    return retVal;
}

void mHidInit(void)
{    
    
#ifdef HYAN_TEST
(void)memset((void*)&test_entry, 5, sizeof(test_entry));
#endif
}

unsigned char UsbPlugIn(void)
{
  return 1;

}

void usrProfileInit(void)
{
	(void)memcpy((void*)&usrProfile, (void*)&usrProfileFlash, sizeof(GFT_PROFILE));
   if(usrProfile.magic != GFT_PROFILE_MAGIC)
   { 
       memset(&usrProfile, 0, sizeof(GFT_PROFILE));
		   usrProfile.magic     = GFT_PROFILE_MAGIC;
		   usrProfile.tAlarm    = IMPACT_ARM_THD;
		   usrProfile.tR        = IMPACT_X_THD;
     //usrProfile.lpEnable  = 0;
		   usrProfile.AlarmMode = PROFILE_ALARM_EN_MSK|PROFILE_ALARM_AU_MSK|PROFILE_ALARM_VI_MSK|PROFILE_ALARM_LOCK_MSK;		   
		 //usrProfile.SID = 0;
		   (void)memcpy((void*)&usrProfile.name, "GFT", 3);
		   
		  // usrProfile.realXmitEnable = 1;
       usrProfile.proxEnable = 0;
 		   myNvramWrite((byte *)&usrProfileFlash,(byte *)&usrProfile,sizeof(GFT_PROFILE));		
   }
   
   field_xmit_enable = 1;                     // fix this flag for always enable, added by Jason Chen, 2014.05.20
   
   profile_alarm_au_backup =  usrProfile.AlarmMode & PROFILE_ALARM_AU_MSK;      // Added by Jason Chen, 2014.11.26
 //usrProfile.tR = 0;
}

byte mHidCmd;

MHID_CMD *pCmd = (MHID_CMD *)RecBuf;
static MHID_RPT *pRpt = (MHID_RPT *)RptBuf;
byte hidHasNewCmd =0;
byte startCal = 0;
void profileSave()
{
	 
	myNvramWrite((byte *)(&usrProfileFlash),(byte *)(&usrProfile),sizeof(GFT_PROFILE));
		
}

void HardInfoSave(byte *mGftID)
{
	
	myNvramWrite((byte *)(&gHardInfoFlash),(byte *)(mGftID),6);		
}


void HidTask(void)
{   
  byte *gftHardID = (byte *)0xF000;
  dword flashID;
  pFct pflash;
    
	byte err;
	unsigned char i=0;
	unsigned char j=0;
	byte cnt=0;
	byte ret =0;
	WORD tmp;
	byte myZero[2] ={0};
	static ACC_ENTRY * pEntry = &spiFlashCurRdBuffer.flashEntry;
	static ACC_DATA *  pAccData = &spiFlashCurRdBuffer.flashData;
    
	byte accNeedSend = 1;
	static byte mHidTaskStat =  MHID_TASK_STAT_GET_ACC_START;
	byte rtcTime[7];
	uint32_t Voltage;                                                             // Added by Jason Chen, 2016.01.11
	
	if (!hidHasNewCmd )
	{
	   return;
	}
  hidHasNewCmd = 0;
   
	switch(pCmd->cmd)
	{
    case MHID_CMD_CAL_START:
        startCal = 1;
        pRpt->h = MHID_CMD_CAL_START;
		    pRpt->cnt = 1;
        break;
    case MHID_CMD_CAL_GET:
        startCal = 1;
        pRpt->h = MHID_CMD_CAL_GET;
		    pRpt->cnt = 6;
        memcpy((void *)pRpt->data, (void *)(&(lgAccV.x)), 6);
        buzzerBeep(100);
        break;
    case MHID_CMD_LP_UNBIND:
		    pRpt->h = MHID_CMD_LP_UNBIND;
		    pRpt->cnt = 1;
        
        lpNeedUnbind =1;
	      break;
	  case MHID_CMD_SET_SN:
        if (pCmd->subCmd == 0)
		    {
			    HardInfoSave((byte*)pCmd->data);
		    }
		
		    pRpt->h = MHID_CMD_SET_SN;
		    pRpt->cnt = 1;
		    accNeedSend = 1;
		    break;
	  case MHID_CMD_GET_SN:
		    pRpt->h = MHID_CMD_GET_SN;
		    pRpt->cnt = 6; 
		    memcpy((void *)pRpt->data, (void *)(&gHardInfoFlash), 6);					
		    break;	
	  case MHID_CMD_SET_USR_PROFILE:
		    if (pCmd->subCmd == 0)
		    {
			    usrProfile.magic = GFT_PROFILE_MAGIC;
			    memcpy((void *)(&usrProfile),(void *)pCmd->data, sizeof(GFT_PROFILE));
		
		    }
		    needReboot =1;
		    needSaveProfile = 1;
		    pRpt->h = MHID_CMD_SET_USR_PROFILE;
		    pRpt->cnt = 1;
		    accNeedSend = 1;
        buzzerBeep(100);
        LED_YELLOW_On();
		    break;
	  case MHID_CMD_GET_USR_PROFILE:
	      pcConnected = 0x01;                                                    // Added by Jason Chen, 2014.05.29
		    pRpt->h = MHID_CMD_GET_USR_PROFILE;
		    (void)memcpy((void*)usrProfile.GID, (void*)gHardInfoFlash, 6 );
		    if(usrProfile.magic != GFT_PROFILE_MAGIC) // not be initialized
		    {
			    pRpt->cnt = 0;
		    }
		    else
		    {
			    pRpt->cnt = sizeof(GFT_PROFILE) + 4; 
			    memcpy((void *)pRpt->data, (void *)(&usrProfile), sizeof(GFT_PROFILE));		
		    }
		    break;
	case MHID_CMD_CHECKBAT:
	      pRpt->h       = MHID_CMD_CHECKBAT;
        pRpt->cnt     = gftInfo.batState;  
        updateBatStatus();                                                    // Added by Jason Chen, 2016.01.11
        Voltage  = (uint32_t)max14676Voltage * 625;                             // Added by Jason Chen, 2016.01.11
        Voltage  = (Voltage/1000+45);                                           // Added by Jason Chen, 2016.01.11   
        BatteryVoltage._word = (word)Voltage;                                   // Added by Jason Chen, 2016.01.11           
		    pRpt->data[1] = BatteryVoltage._byte.byte1;
		    pRpt->data[0] = BatteryVoltage._byte.byte0;
        pRpt->data[2] = max14676Charge;
		    break;
	case MHID_CMD_GET_DEV_INFO:
        pcConnected = 0x01;                 
		    pRpt->h = MHID_CMD_GET_DEV_INFO;
        pRpt->cnt = 0x2;
		    pRpt->data[1] = ProductString[4];
		    pRpt->data[0] = ProductString[3];
        pRpt->data[2] = gftHardID[5];
        pRpt->data[3] = gftHardID[6];
        pRpt->data[4] = 0x2D; //ID
		    break;
	case MHID_CMD_REBOOT:
		    needReboot =1;
		    break;
	case MHID_CMD_START_BOOT:	
		    pflash = (pFct)FIMG_ADDR;
        (void)memcpy((void*)pflash, (void*)flash, FIMG_SIZE);
    
		    DisableInterrupts;
        (*pflash)(CMD_ERASE, (byte*)0x1E00, 0);	
		
		    needReboot =1;
		    EnableInterrupts;
		    pRpt->h = MHID_CMD_START_BOOT;
        cnt = pCmd->subCmd;
		    pRpt->data[0] = ret;
		
		break;
	case MHID_CMD_SET_TIME:
        pRpt->h = MHID_CMD_SET_TIME;	
		    pRpt->cnt = 1;
	
		    myRTC.Year = pCmd->data[0]; 
		    myRTC.Month =pCmd->data[1]; 
		    myRTC.Day =  pCmd->data[2]; 
		    myRTC.Hour = pCmd->data[3]; 
		    myRTC.Minute = pCmd->data[4]; 
		    myRTC.Second = pCmd->data[5]; 

        rtcTime[0] = myRTC.Year;
	      rtcTime[1] = myRTC.Month;
	      rtcTime[2] = myRTC.Day;
		    rtcTime[3] = 0;
   	    rtcTime[4] = myRTC.Hour;
   	    rtcTime[5] = myRTC.Minute;
   	    rtcTime[6] = myRTC.Second;
	
	      Set_YearMonthDay((byte *)&rtcTime);
        g_Tick1mS =0;
		    accNeedSend =1;
		    break;
	case MHID_CMD_GET_TIME:
        pRpt->h = MHID_CMD_GET_TIME;	
		    pRpt->cnt = 1;
		    Get_YearMonthDay((byte *)&rtcTime);
		    myRTC.Year   = BCD2BIN(rtcTime[0]); 
		    myRTC.Month  = BCD2BIN(rtcTime[1]);  
		    myRTC.Day    = BCD2BIN(rtcTime[2]);  
		    myRTC.Hour   = BCD2BIN(rtcTime[4]); 
		    myRTC.Minute = BCD2BIN(rtcTime[5]); 
		    myRTC.Second = BCD2BIN(rtcTime[6]); 
		    memcpy((void *)pRpt->data, (void *)(&myRTC), sizeof(myRTC));
		
		break;

	case MHID_CMD_GET_ACC_ENTRY_CNT:
        #if UPLOAD_BY_WIRE_IN_BINDING_MODE        
           pcConnected = 2;                                                   // Added by Jason Chen, 2015.09.03
        #endif 
		    pRpt->h= MHID_CMD_GET_ACC_ENTRY_CNT;
		    pRpt->cnt = 1;
		    tmp._word = spiFlashCurRdBuffer.accEntryCnt;
		    pRpt->data[0] = tmp._byte.byte1;
		    pRpt->data[1] = tmp._byte.byte0;
		    break;

	case MHID_CMD_GET_ACC_ENTRY:
        
        
		    pRpt->h = MHID_CMD_GET_ACC_ENTRY;
	      if (pCmd->subCmd == 1)
	      {
			    err = spiFlashReadAccEntry(pCmd->data[0]*256 + pCmd->data[1]);
			
			    pRpt->cnt = err == SPIFLASH_RD_SUCESS ?  pCmd->subCmd  : 0; //0 error
			    if (err == SPIFLASH_RD_SUCESS)
			    {
				    (void)memcpy((void *)pRpt->data, (void *)pEntry, 55);//ACC_ENTRY_HEADER_LENGH+20);	

			    }
	      }
		    else if (pCmd->subCmd < 8)
		    {
           
          gftInfo.lpMode  = GMODE_LP_DISABLE; 
			    pRpt->cnt = pCmd->subCmd;
			
			  //(void)memcpy((void *)pRpt->data, (void *)(&pEntry->data[(pCmd->subCmd-2)*20]), 60);	
			    (void)memcpy((void *)pRpt->data, (void *)(&pEntry->rtcAcc.data[(pCmd->subCmd-2)*20]), 60);	

		    }
		    else if (pCmd->subCmd < 12)
		    {
            
			    pRpt->cnt = pCmd->subCmd;		
			    (void)memcpy((void *)pRpt->data, (void *)(&pEntry->dataG[(pCmd->subCmd - 8) * 8]), 48); 

		    }
		
		    break;
	    
	case MHID_CMD_ERASE_ACC_DATA:
	    //spiFlashEraseFlash();
	    //clear alarm interlock
	      usrProfile.AlarmMode &= ~PROFILE_ALARM_ON_MSK;
		  //profileSessionSave(PROFILE_SID_NEW);
	    //profileSave(); 
		    pRpt->h = MHID_CMD_ERASE_ACC_DATA;	
	      pRpt->cnt = 1;
		    pRpt->data[0] = 'S';
		    spiFlashCurWtAddr =0;
		  //needReboot =1;
		    needEraseFlash = 1;
		    needSaveProfile =1;
		    break;
    /*
    data[0] = flash test
    data[1] = 0 id test passed; 1 id fail, 2 erase failed 3 write failed 4 read failed

    data[0] = Acclerometer test
    data[1] = 0 id test passed; 1 id fail, 2 erase failed 3 write failed 4 read failed

    data[0] = Gyro test
    data[1] = 0 id test passed; 1 id fail, 2 erase failed 3 write failed 4 read failed



    */
	case MHID_CMD_TEST:
		   pRpt->h = MHID_CMD_TEST;
		   pRpt->data[0] = pCmd->subCmd;
		   pRpt->cnt =2;

		   switch(pCmd->subCmd)
		   {
			   case MFT_CMD_NULL:
                   (void)memcpy((void *)pRpt->data, (void *)((MHID_CMD2*)pCmd)->addr, cnt);
				   buzzerBeep(100);
				   
				   break;
                
				 case MFT_CMD_FLASH:
				   flashID = spiFlashGetID();
				    
				   if((flashID == (dword)0xC22016)||(flashID == (dword)0xC82016)||(flashID == (dword)0x202016))
				   {
				   	  //spiFlashEraseFlash(); 
                   	  //spiFlashRead(pRpt->data, (dword)((MHID_CMD2*)pCmd)->addr + (dword)((MHID_CMD2*)pCmd)->data[0], cnt);
                      //spiFlashRead(pRpt->data, (dword)0x0, 10);
					  pRpt->data[0] = 0;
				   }
				   else
				   {
				   	  //
						pRpt->data[0] = 1;

				   }
				   buzzerBeep(100);
				   break;
				case MFT_CMD_RADIO_WR:
				   
				   pRpt->data[0] = Radio_SPI_test(); //first spi test
				 
				   if(pRpt->data[0]==0) //if spi is fine then tx data 
				   {
				      
				      Radio_Stack_Write_Lp_Test(pCmd->data[0], pCmd->data[1]);
					  gftInfo.lpMode |= GMODE_LP_TEST;
				   }
				   buzzerBeep(100);
				   break;
				case MFT_CMD_RADIO_RD:   
				   pRpt->data[3] = (byte)(Radio_Stack_Read_Lp_Test(&pRpt->data[0],&pRpt->data[1],&pRpt->data[2])); 			   
				   buzzerBeep(100);
				   break;	
				case MFT_CMD_LED:
					if( pCmd->data[0] ==0)
					{
						accHitOn =0;
						LED_GREEN_On();
						LED_RED_On();
						LED_YELLOW_On();
						mLedState = LED_STATE_WHITE;
					}
					else if( pCmd->data[0] ==1 ) //green led
					{
						accHitOn =0;
						LED_GREEN_On();
						LED_RED_Off();
						LED_YELLOW_Off();                
						mLedState = LED_STATE_GREEN;
					}
					else if (pCmd->data[0] == 2)
					{
						LED_GREEN_Off();
						LED_YELLOW_Off();                
						LED_RED_On();
						mLedState = LED_STATE_RED;
					}
					else
					{
						LED_GREEN_Off();
						LED_YELLOW_On();
						LED_RED_Off();
						mLedState = LED_STATE_YELLOW;
					}
					
					buzzerBeep(100);
			    break;
        case MFT_CMD_ACCE:
        //if(acc_SPIReadReg(ACC_CTRL_REG1)== (ACC_CTRL_REG1,ACC_ODR_400HZ|ACC_XYZAXIS_EN))
     #ifndef ENABLE_6DS3
          if(acc_SPIReadReg(ACC_CTRL_REG1)== (ACC_CTRL_REG1,ACC_ODR_NORMAL_1344HZ|ACC_XYZAXIS_EN))
     #else
      	  if(acc_SPIReadReg(0xF) == 0x69)
     #endif					   	
            pRpt->data[1] = 0;
          else
            pRpt->data[1] = 1;
          buzzerBeep(100);
     			break;
	      case MFT_CMD_BUZZ:
          buzzerBeep(1000);
				//pRpt->data[1] = 0;
				//accNeedSend =0;
				     
			    break;
				case  MFT_CMD_PROX:
					prox_init(1);  
					poxReadADC();
					pRpt->data[1] =   PROX_ADC_Data;
					buzzerBeep(100);
			    break;
				case  MFT_CMD_CHARGER:
					max14676_GetVer();
					pRpt->data[1] =   max14676Exist ? 0 : 1;
					buzzerBeep(100);
			    break;		

				case MFT_CMD_HIGH_G:
				  for(i =0;i<2000;i++)
				  {
				    if(adcTest(&accData))
				   	{
				   		pRpt->data[0] = accData.x;//>>2;
							pRpt->data[1] = accData.y;//>>3;
							pRpt->data[2] = accData.z;//>>3;
								
							break;
				   	}
						else
						{
						  pRpt->data[0] = 9;
							pRpt->data[1] = 9;
							pRpt->data[2] = 9;
						}
				    	
						__RESET_WATCHDOG(); 
				  }
					buzzerBeep(100);
					break;
        case MFT_CMD_GYRO:
     #ifndef ENABLE_6DS3
          if (gyro_SPIReadReg(GYRO_WHO_AM_I) == 0xD4)
     #else
          if(acc_SPIReadReg(0xF) == 0x69)
     #endif					 	
					   pRpt->data[1] = 0;
					else 
					   pRpt->data[1] = 1;
				     buzzerBeep(100);
			    break;
            
			  case MFT_CMD_RTC:
				  buzzerBeep(100);
				  break;
				case MFT_CMD_TORCH_LED:
					LED_ALARM_On();
					for(i =0;i<10;i++)
					{
					   Cpu_Delay100US(5); 
					   __RESET_WATCHDOG();

					}
					buzzerBeep(100);
				  break;
				case MFT_CMD_DEFAULT_SET:
          mLedState = LED_STATE_GREEN;
					
					LED_ALARM_Off();
					buzzerBeep(100);
			  //needReboot = 1;
				//accNeedSend =0;					
		      break;	
				case MFT_CMD_RADIO_TEMP: 
					Get_YearMonthDay((byte *)&rtcTime);
					
					//myRTC.Year   = BCD2BIN(rtcTime[0]); 
			  	//myRTC.Month  = BCD2BIN(rtcTime[1]);  
			  	//myRTC.Day    = BCD2BIN(rtcTime[2]);  
			  	//myRTC.Hour   = BCD2BIN(rtcTime[4]); 
			  	//myRTC.Minute = BCD2BIN(rtcTime[5]); 
			  	//myRTC.Second = BCD2BIN(rtcTime[6]); 
					
					pRpt->data[1] = rtcGetRegisterValue(RTC_CTL_ST1);
					pRpt->data[2] = rtcGetRegisterValue(RTC_CTL_ST2);
					pRpt->data[3] = BCD2BIN(rtcTime[0]); 
					pRpt->data[4] = BCD2BIN(rtcTime[2]);  
					buzzerBeep(100);
          break;
	}		  
	break;
		
	default: //echo back;
		 (void)memcpy((void *)pRpt, (void *)pCmd, sizeof(MHID_CMD));
         pRpt->h |= 0x40; // add a flag  
		 break;
		
	
	} 
  if(accNeedSend)
  {
		pRpt->h |= MHID_CMD_RESPONSE_BIT;
		HID_Transfer_Rpt(RptBuf,HID_INT_IN_EP_SIZE);
  }
	 
}

/******************************************************************************
 * Function:        void HID_Rec_Data(void)
 * Input:           None
 * Output:          None
 * Overview:        Per HID class std. 1.1, it supports bidirectional communication
 *                  this function is used to receive the data transferred by host
 *****************************************************************************/
void HID_Rec_Data(void)
{
  byte Dummy;
  Dummy = HID_Receive_Rpt(RecBuf,HID_INT_OUT_EP_SIZE);
  
  if(Dummy == HID_INT_OUT_EP_SIZE)     
    hidHasNewCmd =1;
  return;
}


