/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#ifndef _MCU_H_
#define _MCU_H_

void McuInit(void);

void ioInit(void);

void Spi2Init(void);

void enableSPI2Interrupt(void);

void I2CInit(void);

void Cpu_Delay100US(word us100);

void prePowerDown(byte mode);

void postPowerDown(byte mode);

/*
** ===================================================================
**     Method      :  McuSetStopMode (component MC9S08JM60_64)
**
**     Description :
**         Sets the low power mode - Stop mode. This method is
**         available only if the STOP instruction is enabled (see
**         <STOP instruction enabled> property).
**         For more information about the stop mode, see the
**         documentation of this CPU.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

#define   McuSetStopMode()  __asm("STOP") /* Set STOP mode */

/*
** ===================================================================
**     Method      :  McuSetWaitMode (component MC9S08JM60_64)
**
**     Description :
**         Sets the low power mode - Wait mode.
**         For more information about the wait mode, see the
**         documentation of this CPU.
**         Release from the Wait mode: Reset or interrupt
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

#define   McuSetWaitMode()  __asm("WAIT") /* Set WAIT mode */

#define ACC_INT_RX_ENABLE()     {SPI2C1_SPIE = 1;}
#define ACC_INT_TX_ENABLE()     {SPI2C1_SPTIE = 1;}


#define ACC_INT_RX_DISABLE()     {SPI2C1_SPIE = 0;}
#define ACC_INT_TX_DISABLE()     {SPI2C1_SPTIE = 0;}

//#define buzzerOn()   {  if( TPM2C0V==0 )(TPM2SC_CLKSA=1,TPM2C0V=TPM2MOD/2);}   
extern void buzzerOn(void);
//#define buzzerOff()  {  if( TPM2C0V ) (TPM2SC_CLKSA=1,TPM2C0V=0); }
extern void buzzerOff(void); 
//#define buzzerStop()   { TPM2SC_CLKSA=0;TPM2C0V=0; }              // Changed by Jason Chen, 2014.04.14


#define BUSCLK           24000000L    //24M   
#define SPICLK           2000000L  //radio lp spi clock

#define INPUT   0
#define OUTPUT  1

#if  defined( GFT_GEN2)
#define LED_GREEN_Off()	      	  {  if( TPM1C2V ) (TPM1C2V = 0); }//PTFD_PTFD0 = 1
#define LED_GREEN_On()		      {  if( TPM1C2V==0 )(TPM1C2V = TPM1MOD/10);}//PTFD_PTFD0 = 0
#define LED_RED_Off()	  	      {  if( TPM1C1V ) (TPM1C1V = 0); }//PTED_PTED3 = 1
#define LED_RED_On()		      {  if( TPM1C1V==0 )(TPM1C1V = TPM2MOD/10);}//PTED_PTED3= 0
#define LED_YELLOW_Off()	      {  if( TPM2C1V ) (TPM2C1V = 0); }//PTFD_PTFD0 = 1
#define LED_YELLOW_On()		      {  if( TPM2C1V==0 )(TPM2C1V = TPM2MOD/5);}//PTFD_PTFD0 = 0
#define CHARGE_HIGH_CUR           {PTAD_PTAD5 =1; PTDD_PTDD7 =0;}
#elif defined(GFT_GENW1)

#define LED_GREEN_Off()	      	  PTDD_PTDD7 = 1
#define LED_GREEN_On()		      PTDD_PTDD7 = 0
#define LED_RED_Off()	  	      PTCD_PTCD2 = 1
#define LED_RED_On()		      PTCD_PTCD2 = 0
#define LED_YELLOW_Off()	      PTAD_PTAD5 = 1
#define LED_YELLOW_On()		      PTAD_PTAD5 = 0
#define LED_EN_On()               PTCD_PTCD5 = 1
#define LED_EN_Off()              PTCD_PTCD5 = 0




#else //GFT_GENW2


#if 1//def JasonCOMMENT
#define LED_GREEN_Off()	      {  (TPM2C1V = 0); PTFD_PTFD5 = 1;}              //PTFD_PTFD5 = 1
#define LED_GREEN_On()		    {  if( TPM2C1V==0 )(TPM2C1V = TPM2MOD/5);}      //PTFD_PTFD5 = 0
#define LED_RED_Off()	  	    {  if( TPM1C1V ) (TPM1C1V = 0);PTED_PTED3 = 1; }//PTED_PTED3 = 1
#define LED_RED_On()		      {  if( TPM1C1V==0 )(TPM1C1V = TPM1MOD/10);}     //PTED_PTED3 = 0
#define LED_YELLOW_Off()	    {  if( TPM1C3V ) (TPM1C3V = 0); PTFD_PTFD1 = 1;}//PTFD_PTFD1 = 1
#define LED_YELLOW_On()		    {  if( TPM1C3V==0 )(TPM1C3V = TPM1MOD/2);}     //PTFD_PTFD1 = 0

#define LED_EN_On()               PTCD_PTCD5 = 1
#define LED_EN_Off()              PTCD_PTCD5 = 0

#else

#define LED_GREEN_Off()	
#define LED_GREEN_On()	
#define LED_RED_Off()	  
#define LED_RED_On()		
#define LED_YELLOW_Off()	    {  if( TPM1C3V ) (TPM1C3V = 0); PTFD_PTFD1 = 1;}//PTFD_PTFD1 = 1
#define LED_YELLOW_On()		    {  if( TPM1C3V==0 )(TPM1C3V = TPM1MOD/2);}     //PTFD_PTFD1 = 0

#define LED_EN_On()    
#define LED_EN_Off()   

#endif




#endif
#define LED_BLUE_On()             LED_YELLOW_On()
#define LED_BLUE_Off()            LED_YELLOW_Off()

#define ENABLE_LOWG_INT    {IRQSC = 0                  \
                                  | IRQSC_IRQIE_MASK   \
                                  | IRQSC_IRQPE_MASK   \
                                  | IRQSC_IRQMOD_MASK  \
                                  | IRQSC_IRQEDG_MASK  \
                                  | IRQSC_IRQPDD_MASK  \
	                              ;}
#define DISABLE_LOWG_INT   {IRQSC_IRQIE = 0 ; IRQSC_IRQACK = 1 ;} //{IRQSC = 0;} //



#define LED_ALARM_On()            PTCD_PTCD5 = 1 
#define LED_ALARM_Off()           PTCD_PTCD5 = 0 




//#define LED_TORCH_On()            PTCD_PTCD3 = 1 
//#define LED_TORCH_Off()           PTCD_PTCD3 = 0

#if defined (GFT_GENW2)
#define BAT_CHARGED      (!PTAD_PTAD0 && PTAD_PTAD5)
#else
#define BAT_CHARGED (!PTAD_PTAD0 && PTFD_PTFD1)
#endif


#if defined(GFT_GENW2)
  #define GYRO_SPI_CS_LOW()     PTCD_PTCD2 =0;
  #define GYRO_SPI_CS_HIGH()    PTCD_PTCD2 =1;
  #define ACC_SPI_CS_LOW()      PTFD_PTFD6 =0;
  #define ACC_SPI_CS_HIGH()     PTFD_PTFD6 =1;
#else
  #define GYRO_SPI_CS_LOW()     PTED_PTED3 =0;
  #define GYRO_SPI_CS_HIGH()    PTED_PTED3 =1;
  #define ACC_SPI_CS_LOW()      PTFD_PTFD6 =0;
  #define ACC_SPI_CS_HIGH()     PTFD_PTFD6 =1;
#endif  
#if defined(GFT_GENW1)|| defined(GFT_GENW2)
#define PWR_SENS_ON()           PTED_PTED1 = 1
#define PWR_SENS_OFF()          PTED_PTED1 = 0
#else
#define PWR_SENS_ON()             
#define PWR_SENS_OFF()            
#endif
#if defined(GFT_GENW2)
#define PWR_PUMP_ON()           PTED_PTED2 = 1
#define PWR_PUMP_OFF()          PTED_PTED2 = 0
#else
#define PWR_PUMP_ON()    
#define PWR_PUMP_OFF()   
#endif


// alarm
#ifdef GFT_GEN2
#define accAlarmOn() (PTGD_PTGD0 == 0)
#else
#define accAlarmOn() (IRQSC_IRQF)
#endif

#ifdef GFT_GEN2
#define KBI_SW_ON  (IRQSC_IRQF)
#define KBI_USB_ON (PTBD_PTBD5 ==1 )

#else


#define KBI_SW_ON    (PTDD_PTDD2 == 0)
#define KBI_PROX_ON  (prox_getpinstatus())      // Changed by Jason Chen, Added Proximity pin to make it working like Button, 2013.11.09
#define KBI_PROX_ON1 (PTGD_PTGD1 == 0)          // Changed by Jason Chen, Added Proximity pin to make it working like Button, 2013.11.11


#define KBI_USB_ON (PTGD_PTGD2 == 1 )
#define KBI_RTC_ON (PTGD_PTGD3 == 1)
#endif

#if defined(GFT_GENW2)
#define RADIO_CS_LOW() PTED_PTED0 = 0
#define RADIO_CS_HI()  PTED_PTED0 = 1
#else
#define RADIO_CS_LOW()  
#define RADIO_CS_HI() 
#endif

#if  defined(GFT_GENW2) || defined(GFT_GENW1)
  #define FLASH_CSPE      PTEPE_PTEPE7          // Pullup resister control
  #define FLASH_CSDD      PTEDD_PTEDD7          // Data Direction --> output
  #define FLASH_CSSE      PTESE_PTESE7          // Slew rate control
  #define FLASH_CSDS      PTEDS_PTEDS7          // Output drive strength control
  #define FLASH_CS_PIN    PTED_PTED7            // Flash Memory Chip Select
  #define FLASH_CS_MASK   0x80
  #define FLASH_CS_PORT   PTED
#else
  #define FLASH_CSPE      PTBPE_PTBPE6          // Pullup resister control
  #define FLASH_CSDD      PTBDD_PTBDD6          // Data Direction --> output
  #define FLASH_CSSE      PTBSE_PTBSE6          // Slew rate control
  #define FLASH_CSDS      PTBDS_PTBDS6          // Output drive strength control
  #define FLASH_CS_PIN    PTBD_PTBD6            // Flash Memory Chip Select
  #define FLASH_CS_MASK   0x40
  #define FLASH_CS_PORT   PTBD
#endif

/* Select SPI FLASH: Chip Select pin low  */
#define SPI_FLASH_CS_LOW()    (FLASH_CS_PIN = 0)

/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_FLASH_CS_HIGH()   (FLASH_CS_PIN = 1)



#define LED_STATE_RED        0
#define LED_STATE_GREEN      1
#define LED_STATE_GREEN_CON  2
#define LED_STATE_YELLOW   0x4
#define LED_STATE_BLUE     0x8
#define LED_STATE_BIND     0x10
#define LED_STATE_WHITE    0x20
#define LED_STATE_TST  0x8

#define SHORT_DELAY()   asm("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;")

extern byte mLedState;
extern byte profile_alarm_au_backup;                  // Added by Jason Chen, 2014.11.26
void buzzerBeep(word time);
void gotoStop3(void);

void gResetRadio(void);                               // Added by Jason chen, 2014.04.10

extern void WaitUntillClockStable(void);
extern void ResetADCTimer(void);                      // Added by Jason chen, 2015.01.14              

#endif
