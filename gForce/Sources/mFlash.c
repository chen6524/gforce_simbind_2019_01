
/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/


#include "mFlash.h"
#include "mImpact.h"
#include "spi_flash.h"
#include <string.h>
#include "mHid.h"
#include "mcu.h"
#include "rtc.h"
//static ACC_DATA flashData;
//static ACC_ENTRY flashEntry;
//static USR_PROFILE flashUsrProfile;

//unsigned long accEntryAddr = 0;
//unsigned long accDataAddr =0;

dword spiFlashCurWtAddr =0;

byte sessionID = 0;
word curSessionIndex = 0;
SPI_FLASH_CUR_RD_BUFF spiFlashCurRdBuffer;
SPI_FLASH_CUR_RD_BUFF spiFlashRdBuffer;

static struct {
    byte Flag;
    byte Spi1C1;
    byte Spi1C2;
    byte Spi1Br;
} Stack_Spi = {0,0,0,0};

static void Radio_Stack_Save_and_Prog_Spi_F(void)
{
    if( Stack_Spi.Flag == 0x55 )
        return;

    // deselect Radio Chip Select & Ext Flash Chip Select
    PTED_PTED0 = 1;     PTEDD_PTEDD0 = OUTPUT;      // PTE0 output, SPI1 SS1 high
    PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, Ext Flash Chip Select

    // save SPI regs here - set a flag that you did it
    Stack_Spi.Spi1C1 = SPI1C1;
    Stack_Spi.Spi1C2 = SPI1C2;
    Stack_Spi.Spi1Br = SPI1BR;
    Stack_Spi.Flag   = 0x55;

    // program your own
    SPI1C1 = //SPI1C1_SPIE_MASK     |
             SPI1C1_SPE_MASK        |
             //SPI1C1_SPTIE_MASK    |
             SPI1C1_MSTR_MASK       |
             //SPI1C1_CPOL_MASK     |
             //SPI1C1_CPHA_MASK     |
             //SPI1C1_SSOE_MASK     |
             //SPI1C1_LSBFE_MASK    |
             0;
    SPI1C2 = //SPI1C2_SPMIE_MASK    |
             //SPI1C2_SPIMODE_MASK  |
             //SPI1C2_MODFEN_MASK   |
             //SPI1C2_BIDIROE_MASK  |
             //SPI1C2_SPISWAI_MASK  |
             //SPI1C2_SPC0_MASK     |
             0;

  
    SPI1BR = //SPI1BR_SPPR2_MASK    |
             //SPI1BR_SPPR1_MASK      |
             //SPI1BR_SPPR0_MASK    | // BUSCLK divide by 3
             //SPI1BR_SPR2_MASK     |
             //SPI1BR_SPR1_MASK     |
             //SPI1BR_SPR0_MASK     | // divide by 2
             0;

}

//
static void Radio_Stack_Restore_Spi_F(void)
{
    if( Stack_Spi.Flag == 0x55 ) {
        //putch('=');
        SPI1C1 = Stack_Spi.Spi1C1;
        SPI1C2 = Stack_Spi.Spi1C2;
        SPI1BR = Stack_Spi.Spi1Br;
        Stack_Spi.Flag = 0;

        // deselect Radio Chip Select
        PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE0 output, SPI1 SS1 high
      //PTED_PTED0 = 0;                                 // Added by Jason Chen according to harry latest version, 2014.04.24     Radio CS

    }
}

byte spiFlashGetCurWtAddr(void)
{
	
    byte isTagValid = 0;
	//RADIO_CS_HI();
	spiFlashCurRdBuffer.accEntryAdr= SPIFLASH_DATA_START_ADR;
	spiFlashCurRdBuffer.lastSid =0;
	spiFlashCurRdBuffer.accEntryCnt = 0;
	
	spiFlashCurRdBuffer.accEntryMagicCnt = 0;           // Added by Jason Chen, 2013.12.11
	
	spiFlashCurWtAddr =0;
	Radio_Stack_Save_and_Prog_Spi_F();

	do
	{
        
		SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashEntry, (dword)spiFlashCurRdBuffer.accEntryAdr,ACC_ENTRY_HEADER_LENGH);
		isTagValid = (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_MAGIC)
		|| (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_START) 
		|| (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_END);
		if (isTagValid)	 
		{
			
			
			if(spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_MAGIC)   // Added by Jason Chen, 2013.12.11
			   spiFlashCurRdBuffer.accEntryMagicCnt++;
			
			spiFlashCurRdBuffer.accEntryAdr += sizeof(ACC_ENTRY);//ACC_ENTRY_HEADER_LENGH + ACC_HIT_DATA_LEN * sizeof(ACC_DATA);
			
			spiFlashCurWtAddr += sizeof(ACC_ENTRY);//ACC_ENTRY_HEADER_LENGH + ACC_HIT_DATA_LEN * sizeof(ACC_DATA);
			spiFlashCurRdBuffer.accEntryCnt++;
			spiFlashCurRdBuffer.lastSid = spiFlashCurRdBuffer.flashEntry.sid;
		}		
		__RESET_WATCHDOG(); 		 
	}while (isTagValid);	
  Radio_Stack_Restore_Spi_F();
	#if 0
  	pTemp = (byte *)&spiFlashCurRdBuffer.flashEntry;
  	
  	for(i =0; i< ACC_ENTRY_HEADER_LENGH;i++,pTemp++)
  	{
  		if(*pTemp == 0xFF) 
  		{
  		   spiFlashCurWtAddr+=i;
  		   break;
  		}
  	}
  	if(i< ACC_ENTRY_HEADER_LENGH)
      	return SPIFLASH_RD_SUCESS;	
	  else 
		    return SPIFLASH_RD_FAILD;
	#endif
    //usrProfile.SID = spiFlashCurRdBuffer.lastSid + 1;
	sessionID = spiFlashCurRdBuffer.lastSid + 1;
  curSessionIndex = (spiFlashCurRdBuffer.accEntryCnt > 0) ? spiFlashCurRdBuffer.accEntryCnt - 1 : 0;
   
	return SPIFLASH_RD_SUCESS;

}

void spiMyFlashGetCurWtAddr(void)                    // Added by Jason Chen, 2014.03.20 for the request of real time
{
	
  byte isTagValid = 0;
	spiFlashCurRdBuffer.accEntryAdr= SPIFLASH_DATA_START_ADR;
	//spiFlashCurRdBuffer.lastSid =0;
	spiFlashCurRdBuffer.accEntryCnt = 0;
	
	spiFlashCurRdBuffer.accEntryMagicCnt = 0;           // Added by Jason Chen, 2013.12.11	
	spiFlashCurWtAddr =0;
	
	Radio_Stack_Save_and_Prog_Spi_F();
	do
	{
        
		SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashEntry, (dword)spiFlashCurRdBuffer.accEntryAdr,ACC_ENTRY_HEADER_LENGH);
		isTagValid = (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_MAGIC)|| (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_START) 
		                                                                      || (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_END);
		if (isTagValid)	 
		{
			
			
			if(spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_MAGIC)   // Added by Jason Chen, 2013.12.11
			   spiFlashCurRdBuffer.accEntryMagicCnt++;
			
			spiFlashCurRdBuffer.accEntryAdr += sizeof(ACC_ENTRY);         //ACC_ENTRY_HEADER_LENGH + ACC_HIT_DATA_LEN * sizeof(ACC_DATA);
			
			spiFlashCurWtAddr += sizeof(ACC_ENTRY);                       //ACC_ENTRY_HEADER_LENGH + ACC_HIT_DATA_LEN * sizeof(ACC_DATA);
			spiFlashCurRdBuffer.accEntryCnt++;
			spiFlashCurRdBuffer.lastSid = spiFlashCurRdBuffer.flashEntry.sid;
		}
		
		__RESET_WATCHDOG(); 
		 
	}while (isTagValid);		
  Radio_Stack_Restore_Spi_F();
}


byte spiFlashWriteAccBuffer(ACC_ENTRY* pEntry)
{
	word size;
	word space;
	size  = sizeof(ACC_ENTRY);
	space = (pEntry->magic == ACC_ENTRY_END) ? 2*sizeof(ACC_ENTRY): 3*sizeof(ACC_ENTRY);//ACC_ENTRY_HEADER_LENGH + ACC_HIT_DATA_LEN * sizeof(ACC_DATA);
	if(spiFlashCurWtAddr + space >  SPI_FLASH_SIZE) 
		return 1; //not enough space 	
		
//RADIO_CS_HI();
//usrProfile.SID &= ~PROFILE_SID_STAT_MSK; 
	pEntry->sid = sessionID;//usrProfile.SID;
	Radio_Stack_Save_and_Prog_Spi_F();
    SPI_FLASH_BufferWrite( (uint8_t *)pEntry, spiFlashCurWtAddr, size);
    
	
	SPI_FLASH_BufferRead((uint8_t*)&spiFlashRdBuffer.flashEntry, spiFlashCurWtAddr, size);
  Radio_Stack_Restore_Spi_F();
	if(memcmp((void*)&spiFlashRdBuffer.flashEntry,(void*)pEntry, size))                // the comment adde by Jason Chen,Verify if this write is correct, 2014.05.06
	{
        
        //we screw up
	    //ACC_PWR_Off(); //poweroff acc 		
	    LED_RED_On();
		needEraseFlash = 1; //clear flash
		needReboot = 1;
        return 1;        
	}
	
	spiFlashCurWtAddr += size;
	spiFlashCurRdBuffer.accEntryCnt++;
	return 0;
}


byte  spiFlashReadFirstAccEntry(void)                               // not used until now, 2014.05.06
{
    //RADIO_CS_HI();
	spiFlashCurRdBuffer.accEntryAdr= SPIFLASH_DATA_START_ADR;
    Radio_Stack_Save_and_Prog_Spi_F();
	SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashEntry, (dword)spiFlashCurRdBuffer.accEntryAdr,ACC_ENTRY_HEADER_LENGH);
    Radio_Stack_Restore_Spi_F();
	if ((spiFlashCurRdBuffer.flashEntry.magic != ACC_ENTRY_MAGIC))
		return SPIFLASH_RD_NOMORE;
	//if ( spiFlashCurRdBuffer.flashEntry.size == 0)
	//	return  SPIFLASH_RD_NOMORE;
    return SPIFLASH_RD_SUCESS;	
}

byte  spiFlashReadNextAccEntry(void)                              // not used until now, 2014.05.06
{
	dword tmpAddr;
    //RADIO_CS_HI();
	tmpAddr = spiFlashCurRdBuffer.accEntryAdr + sizeof(ACC_ENTRY);//ACC_ENTRY_HEADER_LENGH + ACC_HIT_DATA_LEN * sizeof(ACC_DATA);
	Radio_Stack_Save_and_Prog_Spi_F();
	SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashEntry, tmpAddr,ACC_ENTRY_HEADER_LENGH);
    Radio_Stack_Restore_Spi_F();
	if ((spiFlashCurRdBuffer.flashEntry.magic != ACC_ENTRY_MAGIC))
		return  SPIFLASH_RD_NOMORE;
	//if ( spiFlashCurRdBuffer.flashEntry.size == 0)
	//	return  SPIFLASH_RD_NOMORE;
	spiFlashCurRdBuffer.accEntryAdr = tmpAddr;
    return SPIFLASH_RD_SUCESS;
}
//zero based indexing
byte  spiFlashReadAccEntry(word index)
{
	dword tmpAddr;
    //RADIO_CS_HI();
	tmpAddr = sizeof(ACC_ENTRY)*(dword)index;	
  Radio_Stack_Save_and_Prog_Spi_F();  
  SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashEntry, tmpAddr,sizeof(ACC_ENTRY));
  Radio_Stack_Restore_Spi_F();
  
	if ( (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_MAGIC)
		|| (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_START) 
		|| (spiFlashCurRdBuffer.flashEntry.magic == ACC_ENTRY_END))
	{
        
	  spiFlashCurRdBuffer.accEntryAdr = tmpAddr;
		return SPIFLASH_RD_SUCESS;
	}
    return  SPIFLASH_RD_NOMORE;
}

byte  spiFlashReadFirstAccData(void)                              // not used until now, 2014.05.06
{
	#if 1
      //RADIO_CS_HI();
  	if ((spiFlashCurRdBuffer.flashEntry.magic != ACC_ENTRY_MAGIC))
  		return  SPIFLASH_RD_NOMORE;
  	//if ( spiFlashCurRdBuffer.flashEntry.size == 0)
  	//	return  SPIFLASH_RD_NOMORE;
  	
  	spiFlashCurRdBuffer.accDataAdr= spiFlashCurRdBuffer.accEntryAdr + ACC_ENTRY_HEADER_LENGH;
  	spiFlashCurRdBuffer.accDataCnt = ACC_HIT_DATA_LEN-1;
  	Radio_Stack_Save_and_Prog_Spi_F();
  	SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashData, (dword)spiFlashCurRdBuffer.accDataAdr, sizeof(ACC_DATA));
      Radio_Stack_Restore_Spi_F();    
      return SPIFLASH_RD_SUCESS;
	#else //for testing
      SPI_FLASH_BufferRead((byte *)pData, 0, sizeof(ACC_DATA));
      return SPIFLASH_RD_SUCESS;
	#endif
	
}
byte  spiFlashReadNextAccData(void)                               // not used until now, 2014.05.06
{
	//RADIO_CS_HI();
	if (spiFlashCurRdBuffer.accDataCnt == 0)
		return  SPIFLASH_RD_NOMORE;
	spiFlashCurRdBuffer.accDataAdr += sizeof(ACC_DATA);
	spiFlashCurRdBuffer.accDataCnt--;
	Radio_Stack_Save_and_Prog_Spi_F();
	SPI_FLASH_BufferRead((byte *)&spiFlashCurRdBuffer.flashData, (dword)spiFlashCurRdBuffer.accDataAdr, sizeof(ACC_DATA));
    Radio_Stack_Restore_Spi_F();
    return SPIFLASH_RD_SUCESS;
}
byte spiFlashRead(byte * dst, dword adr, byte cnt)                // not used until now, 2014.05.06
{
  RADIO_CS_HI();
  Radio_Stack_Save_and_Prog_Spi_F();
  SPI_FLASH_BufferRead(dst, adr, (word)cnt);
  Radio_Stack_Restore_Spi_F();
  return SPIFLASH_RD_SUCESS; 

}
byte spiFlashEraseFlash(void)
{
	//uint32_t i;
    //RADIO_CS_HI();
	#if 0
	  for(i =0; i < spiFlashCurWtAddr; i += SPI_FLASH_SectorSize)
	  {
	 
	   SPI_FLASH_SectorErase(i);
	 
  	}
	#endif
    Radio_Stack_Save_and_Prog_Spi_F();
  	SPI_FLASH_BulkErase();
    Radio_Stack_Restore_Spi_F();
    
    return 0;
}

#if 0
void spiFlashCreateTimeStampEntry(byte mode)
{
	//borrow sipFlashCurRdbyuffer to save ram space s
	//RADIO_CS_HI();
    DisableInterrupts;
	  spiFlashCurRdBuffer.flashEntry.magic = mode;
	  if(mode == ACC_ENTRY_END)
         spiFlashCurRdBuffer.flashEntry.gyroActive.activeTime = gActiveTime;
	  else
	  	spiFlashCurRdBuffer.flashEntry.gyroActive.activeTime = 0;
	  if(!getRtcStamp()) 
       mLedState = LED_STATE_RED;
	  (void)memcpy((byte *)&spiFlashCurRdBuffer.flashEntry.t, (void *)&myRTC, sizeof(MY_RTC));
    EnableInterrupts;
	
    Radio_Stack_Save_and_Prog_Spi_F();
    (void)spiFlashWriteAccBuffer(&spiFlashCurRdBuffer.flashEntry);
    Radio_Stack_Restore_Spi_F();    
}
#else
void spiFlashCreateTimeStampEntry(byte mode)
{
	//borrow sipFlashCurRdbyuffer to save ram space s
	//RADIO_CS_HI();
    DisableInterrupts;
	  //memset(&spiFlashCurRdBuffer.flashEntry, 0, sizeof(ACC_ENTRY));//init gftInfo
	  spiFlashCurRdBuffer.flashEntry.magic = mode;
	  
	  if(mode == ACC_ENTRY_END)
	  {
         spiFlashCurRdBuffer.flashEntry.gyroActive.activeTime = gActiveTime;
		 (void)memcpy((byte *)(spiFlashCurRdBuffer.flashEntry.rtcAcc.oldRTC), (byte *)(&lostRTC), sizeof(MY_RTC));
		 
		 (void)memcpy((byte *)(&( spiFlashCurRdBuffer.flashEntry.rtcAcc.oldRTC[1])), (byte *)&newRTC, sizeof(MY_RTC));

	  }
	  else if(mode == ACC_ENTRY_START)
	  {
	    gActiveTime =0;
	  	spiFlashCurRdBuffer.flashEntry.gyroActive.activeTime = gActiveTime;
	  }
	  if(!getRtcStamp()) 
       mLedState = LED_STATE_RED;
	  (void)memcpy((byte *)&spiFlashCurRdBuffer.flashEntry.t, (void *)&myRTC, sizeof(MY_RTC));
	  spiFlashCurRdBuffer.flashEntry.t.StartMS = g_Tick1mS;	  		
      EnableInterrupts;
	
    Radio_Stack_Save_and_Prog_Spi_F();
    (void)spiFlashWriteAccBuffer(&spiFlashCurRdBuffer.flashEntry);
    Radio_Stack_Restore_Spi_F();   
	 
}
#endif

#define DATA_START_SECTOR     0x24
#define SESSION_START_SECTOR  0x0
#define SESSION_SECTOR_MAX    24
#define SESSION_ENTRY_SPACE   0x400

SESSION_ENTRY curSessionEntry;
uint8_t sessionEntryIndex;
uint32_t dataHeadAddr;
uint32_t dataTailAddr;


byte spiFlashGetCurSessionEntry(void)                               // not used until now, 2014.05.06
{
    SESSION_ENTRY se;
    uint32_t addr = SESSION_START_SECTOR;
    byte isTagValid = 0;
    byte i;
    //RADIO_CS_HI();
    for(i = 0; i < 23; i++)
	  {
       Radio_Stack_Save_and_Prog_Spi_F();
		   SPI_FLASH_BufferRead((uint8_t*)&se, addr, sizeof(SESSION_ENTRY));
       Radio_Stack_Restore_Spi_F();
		   isTagValid = (se.magic == SESSION_ENTRY_MAGIC);
		   if (isTagValid)	 
		   {
          //if(i ==0)
          //    dataHeadAddr = DATA_START_SECTOR * SPI_FLASH_SectorSize;
			    dataTailAddr = se.endAddr;
			    addr += SPI_FLASH_SectorSize;
		   }
        
		   __RESET_WATCHDOG(); 
       if(!isTagValid)
       {
           sessionEntryIndex = i;
           curSessionEntry.magic = SESSION_ENTRY_MAGIC;
           curSessionEntry.startAddr = dataTailAddr+1;
           curSessionEntry.accBits = 10;
           curSessionEntry.cnt =0;
           break;
       }
                
	  }
    return 1;
}

//mode 0 = start  1 = end

void spiFlashCreateSessionEntryStartTime(byte mode)                 // not used until now, 2014.05.06
{

    if(!getRtcStamp()) 
        mLedState = LED_STATE_RED;
    if(mode)
       (void)memcpy((byte *)&curSessionEntry.endTime, (void *)&myRTC, sizeof(MY_RTC));
    else
       (void)memcpy((byte *)&curSessionEntry.startTime, (void *)&myRTC, sizeof(MY_RTC)); 
}
word spiFlashGetCurrentSessionIndex(void)
{   
 
   return 0;
   
}

uint32_t spiFlashGetID(void)
{
      uint32_t tmp;
	  Radio_Stack_Save_and_Prog_Spi_F();
      tmp =SPI_FLASH_ReadID();
	  Radio_Stack_Restore_Spi_F();
	  return tmp;
	  

}

