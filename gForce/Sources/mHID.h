/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/
#ifndef _MHID_H
#define _MHID_H
#include "hid.h"
#include "mImpact.h"


extern char RecBuf[HID_INT_OUT_EP_SIZE];
extern byte gHardInfoFlash[6];

#define MHID_CMD_RESPONSE_BIT  0x80

enum 
{
	MHID_CMD_GET_ACC_ENTRY,           //0
	MHID_CMD_GET_ACC_ENTRY_CNT,       //1//
	MHID_CMD_GET_ACC_ENTRY_NEXT,      //
	MHID_CMD_GET_ACC_ENTRY_FIRST,
	MHID_CMD_GET_ACC_DATA_MAX,
	MHID_CMD_GET_ACC_DATA_NEXT,
	MHID_CMD_GET_ACC_DATA_FIRST,
	MHID_CMD_GET_ACC_DATA_LAST,
	MHID_CMD_SET_USR_PROFILE,
	MHID_CMD_GET_USR_PROFILE,         //0x9
	MHID_CMD_ERASE_ACC_DATA,          //0xa
	MHID_CMD_ERASE_USR_PROFILE,       //0xb
	MHID_CMD_SET_TIME,                //0xc
	MHID_CMD_START_BOOT,              //0xd
	MHID_CMD_REBOOT,                  //Bootldr & firmwre 0xe
	MHID_CMD_ERASE_FIRMWARE,          //Only used by bootloader  0xf
	MHID_CMD_PROG_FIRM,               //Only used by bootloader 0x10
	MHID_CMD_GET_BATTERY_STAT,	      //abandonded 
	MHID_CMD_GET_ACC_ENTRY_DATA_ALL,  //
	MHID_CMD_GET_ACC_ENTRY_DATA_STOP,
	MHID_CMD_TEST,                    //0x14
    MHID_CMD_CHECKBAT,                //0x15
	MHID_CMD_DEBUG,  
	MHID_CMD_GET_GID,                 //0x17
	MHID_CMD_GET_DEV_INFO,            //0x18
	MHID_CMD_GET_TIME,                //0x19
	MHID_CMD_SET_SN,                  //0x20
	MHID_CMD_GET_SN,                  //0x21
	MHID_CMD_RUN_FIRM,                //0x22
	MHID_CMD_LP_UNBIND,               //0x23
	MHID_CMD_CAL_START,               //0x24
	MHID_CMD_CAL_GET                  //0x25
	
};
//for the test command
enum 
{
	MFT_CMD_NULL,
	MFT_CMD_FLASH,
	MFT_CMD_LED,
	MFT_CMD_RTC,
	MFT_CMD_GYRO,
	MFT_CMD_ACCE,
	MFT_CMD_BUZZ,
	MFT_CMD_TORCH_LED,
	MFT_CMD_DEFAULT_SET,
	MFT_CMD_PROX,
	MFT_CMD_RADIO_WR,
	MFT_CMD_RADIO_RD,
	MFT_CMD_RADIO_TEMP,
	MFT_CMD_CHARGER,
	MFT_CMD_HIGH_G
};

enum 
{
	MHID_TASK_STAT_GET_ACC_START,
	MHID_TASK_STAT_GET_ACC_ENTRY_NEXT,
	MHID_TASK_STAT_GET_ACC_ENTRY_FIRST,
	MHID_TASK_STAT_GET_ACC_DATA_NEXT,
	MHID_TASK_STAT_GET_ACC_DATA_FIRST,
	MHID_TASK_STAT_GET_ACC_END
};




typedef struct 
{
	byte cmd;
	byte subCmd;
	byte data[50];     //total 8
} MHID_CMD;
typedef struct 
{
	byte cmd;
	byte subCmd;//or count for firmware data
	word addr;
	byte data[37];    
} MHID_CMD2;

typedef struct 
{
	byte h;
	byte cnt;
	byte data[62];    
} MHID_RPT;  //cmd packet

typedef struct 
{
	byte cmd;
	byte subCmd; // number out of seven
	word cnt;
	byte data[60];    
} MHID_RPT2; //data upload packet


void mHidInit(void);
void HidTask(void);


unsigned char UsbPlugIn(void);
void usrProfileInit(void);
byte spiFlashEraseFlash(void);

extern byte needSaveProfile;
extern byte needEraseFlash;
extern byte needReboot;
extern byte lpNeedUnbind;
extern byte needHibernate;
extern byte startCal;
extern byte pcConnected;
//user profile struct in internal flash 
//extern GFT_PROFILE usrProfileFlash;

byte myNvramWrite(byte *addr, byte *src, byte len);

byte getHardIDMajor(void);
byte getHardIDMinor(void);

//#define BIN2BCD(x) ((x%10) | ((x/10) << 4))
#define BCD2BIN(x) ((x&0xF) + ((x & 0xF0) >> 4)*10 )

#endif

