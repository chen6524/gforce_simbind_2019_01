/********************************************************************************
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
********************************************************************************/



#ifndef __UTILS_H
#define __UTILS_H


//timer
void    mTimer_TimeSet(unsigned int *pTimer);
unsigned char    mTimer_TimeExpired(unsigned int *pTimer, unsigned int time);

#define POLYNOM					                0x8408		
#define PRESET_VALUE			                0xFFFF
#define CHECK_VALUE				                0xF0B8

extern unsigned int CRC_Value;

void CalculateCRC(unsigned char * Buf, unsigned char Len);

void MakeCRC(unsigned char * Buf, unsigned char Len);
unsigned char VerifyCRC(unsigned char * Buf, unsigned char Len);

unsigned char GetTotalDaysInMonth(unsigned char ThisYear, unsigned char ThisMonth);


unsigned char IsValidDateTime(unsigned char tYear_BCD, unsigned char tMonth_BCD, unsigned char tDay_BCD, 
                              unsigned char tHour_BCD, unsigned char tMinute_BCD, unsigned char tSecond_BCD);

unsigned long ISqrt(unsigned long lv);

void rtcAdjust(unsigned char mSecond);
char rtcPwrOff(byte hour);


#endif

