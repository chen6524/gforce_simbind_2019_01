/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West
 * Markham, ON L3R 3J9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#if 0
#ifndef _LSM330DLC_H_
#define _LSM330DLC_H_
#include "Typedef.h"
#include "mcu.h"
#define BIT(x) ( 1<<(x) )
#define FIFO_DEPTH           32


#define Dummy_Byte  0xA5

typedef struct _sample_struct 
{
  int16_t X;
  int16_t Y;
  int16_t Z;
  
} sample_t;

typedef union _whole_FIFO 
{
  uint8_t     rawByte[FIFO_DEPTH*6];
  sample_t    samplet[FIFO_DEPTH];  
  uint8_t     rawData[FIFO_DEPTH][6];
}whole_FIFO;

enum
{
	//LSM330DLC Gyroscope Register Address	
	GYRO_WHO_AM_I        = 0x0F,                  // ID = 0xD4
  GYRO_CTRL_REG1       = 0x20,                  // Control register1
	GYRO_CTRL_REG2       = 0x21,                  // Control register2
	GYRO_CTRL_REG3       = 0x22,                  // Control register3
	GYRO_CTRL_REG4       = 0x23,                  // Control register4
	GYRO_CTRL_REG5       = 0x24,                  // Control register5
	GYRO_REFERENCE       = 0x25,
	GYRO_OUT_TEMP        = 0x26,                  // output
	GYRO_STATUS_REG      = 0x27,                  // output
	GYRO_OUT_X_L         = 0x28,                  // output
	GYRO_OUT_X_H         = 0x29,                  // output
	GYRO_OUT_Y_L         = 0x2A,                  // output
	GYRO_OUT_Y_H         = 0x2B,                  // output
	GYRO_OUT_Z_L         = 0x2C,                  // output
	GYRO_OUT_Z_H         = 0x2D,                  // output

  GYRO_FIFO_CTRL_REG   = 0x2E,
  GYRO_FIFO_SRC_REG    = 0x2F,                  // output
  GYRO_INT1_CFG        = 0x30,
  GYRO_INT1_SRC        = 0x31,                  // output
  GYRO_INT1_TSH_XH     = 0x32,
  GYRO_INT1_TSH_XL     = 0x33,
  GYRO_INT1_TSH_YH     = 0x34,
  GYRO_INT1_TSH_YL     = 0x35,
  GYRO_INT1_TSH_ZH     = 0x36,
  GYRO_INT1_TSH_ZL     = 0x37,
  GYRO_INT1_DURATION   = 0x38,	
};


//  FIFO CONFIGURATIONS
#define FIFO_CONTROL_WTMSAMP            (BIT(4) | BIT(3) | BIT(2) | BIT(1) | BIT(0))

#define DEF_FIFO_CTRL_BYPASS		        0x00
#define DEF_FIFO_CTRL_FIFO_MODE		      BIT(5)
#define DEF_FIFO_CTRL_STREAM		        BIT(6)
#define DEF_FIFO_CTRL_STREAM_TO_FIFO	  (BIT(6) | BIT(5))
#define DEF_FIFO_CTRL_BYPASS_TO_STREAM	BIT(7)

#define FIFO_SOURCE_SAMPLES             (BIT(4) | BIT(3) | BIT(2) | BIT(1) | BIT(0))
#define FIFO_SOURCE_EMPTY               BIT(5)
#define FIFO_SOURCE_OVRN                BIT(6)
#define FIFO_SOURCE_WTM                 BIT(7)


//    Control Register
//		Control Register 1
#define CTRL_REG1_ODR_760HZ             (BIT(7)|BIT(6))
#define CTRL_REG1_BW11                  (BIT(5)|BIT(4))
#define CTRL_REG1_NORMALMODE            BIT(3)
#define CTRL_REG1_DEFAULT               (BIT(2)|BIT(1)|BIT(0))

//		Control Register 2
#define CTRL_REG2_DEFAULT               0
#define EXTREN                          0x80
#define LVLEN                           0x40

//		Control Register 3
#define CTRL_REG3_DEFAULT               0   // Disable all interrupt

//		Control Register 4
#define CTRL_REG4_SCALE2000             (BIT(5)|BIT(4))        // FULL Scale --2000dps
#define CTRL_REG4_DEFAULT               0


//		Control Register 5
#define CTRL_REG5_DEFAULT               0
#define CTRL_REG5_FIFO_EN		            BIT(6)
#define CTRL_REG5_STOP_ON_WTM		        BIT(5)

//		INT_SRC
#define INT_SRC_IA		                  BIT(6)

//gyro define
#define GYRO_DATAREADY_BIT              3

enum
{
	//LSM330DLC Accelerometer Register Address	
  ACC_CTRL_REG1        = 0x20,                  // Control register1
	ACC_CTRL_REG2        = 0x21,                  // Control register2
	ACC_CTRL_REG3        = 0x22,                  // Control register3
	ACC_CTRL_REG4        = 0x23,                  // Control register4
	ACC_CTRL_REG5        = 0x24,                  // Control register5
	ACC_CTRL_REG6        = 0x25,
	ACC_REFERENCE        = 0x26,                  // 
	ACC_STATUS_REG       = 0x27,                  // 
	
	ACC_OUT_X_L          = 0x28,                  // output
	ACC_OUT_X_H          = 0x29,                  // output
	ACC_OUT_Y_L          = 0x2A,                  // output
	ACC_OUT_Y_H          = 0x2B,                  // output
	ACC_OUT_Z_L          = 0x2C,                  // output
	ACC_OUT_Z_H          = 0x2D,                  // output

  ACC_FIFO_CTRL_REG    = 0x2E,
  ACC_FIFO_SRC_REG     = 0x2F,                  // output
  ACC_INT1_CFG         = 0x30,
  ACC_INT1_SRC         = 0x31,                  // output
  ACC_INT1_THS         = 0x32,
  ACC_INT1_DURATION    = 0x33,
  ACC_INT2_CFG         = 0x34,
  ACC_INT2_SRC         = 0x35,                  // output
  ACC_INT2_THS         = 0x36,
  ACC_INT2_DURATION    = 0x37,  
  ACC_CLICK_CFG        = 0x38,
  ACC_CLICK_SRC        = 0x39,
  ACC_CLICK_THS        = 0x3A,
  ACC_TIME_LIMIT       = 0x3B,
  ACC_TIME_LATENCY     = 0x3C,
  ACC_TIME_WINDOW      = 0x3D,
  ACC_ACT_THS          = 0x3E,
  ACC_ACT_DUR          = 0x3F,
};


// ACC_CTRL_REG1
#define ACC_ODR_POWERDOWN       (0x00 << 4)
#define ACC_ODR_200HZ           (BIT(6)|BIT(5))
#define ACC_ODR_400HZ           (BIT(6)|BIT(5)|BIT(4))
#define ACC_ODR_LOWPOWER_1620HZ (0x08 << 4)
#define ACC_ODR_NORMAL_1344HZ   (0x09 << 4)
#define ACC_LOW_POWER           BIT(3)
#define ACC_XYZAXIS_EN          BIT(2)|BIT(1)|BIT(0)

// ACC_CTRL_REG3
#define INT1_AOI1               BIT(6)
#define INT1_DRDY1              BIT(4)
#define INT1_DRDY2              BIT(3)
#define INT1_FIFO_WATERMARK     BIT(2)
#define INT1_FIFO_OVERRUN       BIT(1)

// ACC_CTRL_REG4
#define ACC_FULLSCALE_02G       (0x00 << 4)
#define ACC_FULLSCALE_04G       (0x01 << 4)
#define ACC_FULLSCALE_08G       (0x02 << 4)
#define ACC_FULLSCALE_16G       (0x03 << 4)
#define ACC_HIGH_RESOLUTION     BIT(3)       // High-->12bits, Low--->8bits

// ACC_CTRL_REG5
#define ACC_FIFO_ENABLE         BIT(6)
#define ACC_LIR_INT1_ENABLE     BIT(3)

// ACC_CTRL_REG6
#define ACC_INT_ACTIVE_LOW      BIT(1)


// ACC_FIFO_CTRL_REG
#define FIFO_BYPASS_A           (0x00 << 6)
#define FIFO_MODE_A             (0x01 << 6)
#define FIFO_STREAM_A           (0x02 << 6)
#define FIFO_TRIGGER_A          (0x03 << 6)   // Stream to FIFO
#define FIFO_TRIGGER_INT2       BIT(6)        // Trigger event linked to trigger signal on INT2_A
#define FIFO_THR(x)             (x)

// INT1_CFG
#define ACC_INT1_CFG_6DIR_MOVE  BIT(6)
#define ACC_INT1_CFG_6DIR_POS   BIT(7)|BIT(6)

#define XYZ_UP_INT_ENABLE       BIT(5)|BIT(3)|BIT(1)   // 0x2A
#define XYZ_DOWN_INT_ENABLE     BIT(4)|BIT(2)|BIT(0)   // 0x15

// INT1_SRC
#define ACC_X_LOW_INT           BIT(0)
#define ACC_X_HIGH_INT          BIT(1)
#define ACC_Y_LOW_INT           BIT(2)
#define ACC_Y_HIGH_INT          BIT(3)
#define ACC_Z_LOW_INT           BIT(4)
#define ACC_Z_HIGH_INT          BIT(5)
#define ACC_INT_EVENT           BIT(6)


// ACC_ACT_THS, ACC_ACT_DUR
#define SLEEP_TO_WAKE_THR(x)  (x)           // x<127, 1LSB = 16mg
#define ACT_DURATION_TIME     (x)           // x<255, Real Duration time = (x+1)*8/ODR


extern void yro_SPIBufferRead(uint8_t* pBuffer, uint8_t readAddr, uint8_t numByteToRead);
extern byte gyro_SPIReadReg(uint8_t regAddr);
extern void gyro_SPIWriteReg(uint8_t regAddr, uint8_t regValue);
extern void gyro_SPIBufferWrite(uint8_t regAddr,uint8_t *writeBuf, uint8_t numByteToWrite); 

extern void gyro_FifoBypass(void);
extern void gyro_FifoStream(void);
extern void gyro_FifoMode(void);
extern byte gyro_FifoRead(whole_FIFO* pBuffer);
extern void gyro_FIFO_SPIGetRawData(uint8_t *pBuffer); 


extern void acc_SPIBufferRead(uint8_t* pBuffer, uint8_t readAddr, uint8_t numByteToRead);
extern void acc_FIFO_SPIGetRawData(byte *pBuffer);
extern byte acc_SPIReadReg(uint8_t regAddr); 
extern void acc_SPIWriteReg(uint8_t regAddr, uint8_t regValue);
extern void acc_SPIBufferWrite(uint8_t regAddr,uint8_t *writeBuf, uint8_t numByteToWrite); 

extern void SPI_MEMS_Acc_Config(byte mode);
extern void SPI_MEMS_Gyro_Config(void);

#endif
#endif
