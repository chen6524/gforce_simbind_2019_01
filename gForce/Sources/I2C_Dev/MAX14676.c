/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http://www.artaflex.com

  
 *
 ***************************************************************************/

#include "gft.h"
#include "I2C_driver.h"
#include "MAX14676.h"
#include <hidef.h> 

#include "mcu.h"
#include "kbi.h"



uint8_t max14676Exist =0;
uint16_t max14676Voltage =0x1FFF;
uint16_t max14676Voltage_old = 0x1FFF;                  // 2014.12.08
uint8_t max14676Charge = 0; //init to 50%
uint8_t max14676ChargeStat = 0;
uint8_t max14676_result;


void max14676_WriteReg(uint8_t regAddr,uint8_t regValue) 
{
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
  (void)I2C_driver_SelectSlave(MAX14676_DEV_ADDR);  
  (void)I2C_driver_SendBlock(send_Buf,2,&max14676_result);    
  (void)I2C_driver_SendStop();
}

uint8_t max14676_ReadReg(uint8_t* ptrVal,uint8_t regAddr) 
{
  uint8_t result = 0;
  uint8_t readValue[2];
   
  result  = I2C_driver_SelectSlave(MAX14676_DEV_ADDR);
  result |= I2C_driver_SendBlock(&regAddr,1,&max14676_result);  
  result |= I2C_driver_RecvBlock(readValue,1,&max14676_result);
  (void)I2C_driver_SendStop();     
  *ptrVal = readValue[0];
  return  result; 
}



void max14676_WriteReg2(uint8_t regAddr,uint16_t regValue) 
{
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
  send_Buf[2] = (uint8_t)(regValue>>8);
    
  (void)I2C_driver_SelectSlave(MAX14676_DEV_ADDR2);  
  (void)I2C_driver_SendBlock(send_Buf,3,&max14676_result);    
  (void)I2C_driver_SendStop();
}



uint8_t max14676_ReadReg2(uint16_t * pReadVal, uint8_t regAddr) 
{
  volatile uint8_t result = 0;
  uint8_t readValue[4];
   
  result  = I2C_driver_SelectSlave(MAX14676_DEV_ADDR2);
  result |= I2C_driver_SendBlock(&regAddr,1,&max14676_result);  
  result |= I2C_driver_RecvBlock(readValue,4,&max14676_result);
  (void)I2C_driver_SendStop();                                      
  
  *pReadVal = (uint16_t)readValue[2]*256 +(uint16_t)readValue[1]; 
  return result;
}







void max14676_init(void) 
{
   volatile uint16_t data=0; 
  
   
   max14676_GetVer();
   if(!max14676Exist) return;
   //LED_YELLOW_On();
   max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x80); 
   max14676_WriteReg(MAX14676_CHGCNTLB,0x10);
   max14676_WriteReg(MAX14676_ILIMCNTL_ADDR, 0x17);
   max14676_WriteReg(MAX14676_CHGCNTLA,0x14);//0x18
   //max14676_WriteReg(MAX14676_LDOCFG,0x40);//0x18
   //data = max14676_ReadReg(0x0);
   //data = max14676_ReadReg(0x05);
   //max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x80); 
   //max14676_WriteReg(0x11, 0x84); 
   max14676_WriteReg(0x10,0x6e); 
  //testing
   //data =max14676_ReadReg2(0x02);  
   //data =max14676_ReadReg2(0x04);		
   // LED_YELLOW_Off();
}

//return the percentage 
uint8_t max14676_GetBatteryCharge(void) 
{
	uint16_t data=0; 
    uint8_t result;
    result =max14676_ReadReg2(&data,0x04); 
	if(result ==0)
		return (data/256);
	else 
		return 0;
	 
}
uint16_t max14676_GetBatteryChargeVoltage( void) 
{
	uint16_t data=0; 
    uint8_t result;
    result =max14676_ReadReg2(&data,0x02); 
	
	return data;
}


void max14676_GetVer(void) 
{
  uint8_t result;
  uint8_t readValue;  
  
  result =  max14676_ReadReg(&readValue, 0);                      

  
  if((result == 0 )&&(readValue == 0x2E))
    max14676Exist = 2;  
  else 
  {
     max14676Exist = 0; 
  }
}
// assume max14676 exist

void max14676_process(void)
{
    uint8_t data;
	uint8_t status;
	uint8_t myCharge;
		
      	DisableInterrupts; 
      	max14676Voltage = max14676_GetBatteryChargeVoltage(); 
    	myCharge = max14676_GetBatteryCharge();
		
		status =  max14676_ReadReg(&data,0x2);
    	EnableInterrupts; 
		if(!max14676Charge)
        {
		      max14676Charge += myCharge;
		      max14676Charge >>1;
		}
		 max14676Charge = myCharge;   
		
		max14676ChargeStat   = (/*data == 0x5 || */ data == 0x6) ? 1 : 0;   //  //1 charged; 0 uncharged
      	if(max14676Voltage >= 9600)    //3V              
      	  max14676Voltage_old = max14676Voltage;    
      	else
          max14676Voltage = max14676Voltage_old;      
      	max14676Voltage = max14676Voltage/625 * 78;
	   
      
	
	
}


void max14676_poweroff(void)
{
 
	volatile uint16_t data=0; 
	
	for(;;)
	{
		max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x5);
	}

}

