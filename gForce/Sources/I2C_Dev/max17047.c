/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http://www.artaflex.com
 
 * Revision History:
 *    Sep,11 2012 First Release
 *    Nov.12 2013, Modified the driver to make it into whole structure according to Kashif design
  
  
 *
 ***************************************************************************/

#include "gft.h"
#include "I2C_driver.h"
#include "MAX17047.h"
#include <hidef.h> 

#include "mcu.h"
#include "kbi.h"
#if 1

uint16_t max17047Voltage = 0x1FFF;
uint16_t max17047Voltage_old = 0x1FFF;                  // 2014.12.08
uint16_t max17047Version = 0xFFFF;
//int16_t  max17047Current = 0x7FFF;
uint8_t  max17047Exist = 0;

//volatile word g_TickSecondBat;
//volatile byte battSampEnableFuel = 0;
//volatile byte battMsgXmit = 0;



volatile bool Max17047IntState = FALSE;  
word max17047_result;

void max17047_WriteReg(uint8_t regAddr,uint16_t regValue);
uint16_t max17047_ReadReg(uint8_t regAddr);


void max17047_WriteReg(uint8_t regAddr,uint16_t regValue) 
{
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
  send_Buf[2] = (uint8_t)(regValue>>8);
    
  (void)I2C_driver_SelectSlave(MAX17047_DEV_ADDR);  
  (void)I2C_driver_SendBlock(send_Buf,3,&max17047_result);    
  (void)I2C_driver_SendStop();
}



uint16_t max17047_ReadReg(uint8_t regAddr) 
{
  uint8_t result = 0;
  uint8_t readValue[2];
   
  result  = I2C_driver_SelectSlave(MAX17047_DEV_ADDR);
  result |= I2C_driver_SendBlock(&regAddr,1,&max17047_result);  
  result |= I2C_driver_RecvBlock(readValue,2,&max17047_result);
  (void)I2C_driver_SendStop();                                     // Added by Jason Chen, 2013.11.18
//result |= I2C_driver_SendStop();                                 // This Stop signal is sent by RecvBlock() automatically
  return (uint16_t)readValue[1]*256 +(uint16_t)readValue[0]; 
}


uint8_t max17047_ClearInterruptFlag(void) 
{
  return (uint8_t)max17047_ReadReg(MAX17047_STATUS);  
}

//static void max17047_enableInterupt(void) 
//{
  //max17047_ReadReg(MAX17047_STATUS,0x16);  
//}






void max17047_init(void) 
{
  volatile uint16_t data=0; 
  
  // max17047_WriteReg(MAX17047_C_GAIN,0x0000);
  // max17047_WriteReg(MAX17047_MISC_CFG,0x0003);
  // max17047_WriteReg(MAX17047_LEARN_CFG,0x0700);
   data = max17047_ReadReg(0);
   data = max17047_ReadReg(MAX17047_CONFIG);
   data = max17047_ReadReg(MAX17047_V_ALRT_THRESHOLD);
   data = max17047_ReadReg(MAX17047_CURRENT);
   data = max17047_ReadReg(MAX17047_VERSION);
   data = max17047_ReadReg(MAX17047_SHDN_TIMER);
   data = max17047_ReadReg(2);
   data = max17047_ReadReg(3);
   data = max17047_ReadReg(4);
   data = max17047_ReadReg(5);
   data = max17047_ReadReg(MAX17047_SHDN_TIMER);
  
   max17047_WriteReg(MAX17047_SHDN_TIMER,0xC123) ;
   data = max17047_ReadReg(MAX17047_SHDN_TIMER);
}


//uint8_t  max17047NewDataFlag = 0;


void max17047_GetVer(void) 
{
  uint8_t result;
  uint8_t readValue[2];  
  
  readValue[0] = MAX17047_C_GAIN;//MAX17047_VERSION;
   
  (void)I2C_driver_SelectSlave(MAX17047_DEV_ADDR);
  result  = I2C_driver_SendBlock(&readValue[0],1,&max17047_result);  
  result |= I2C_driver_RecvBlock(readValue,2,&max17047_result);
  (void)I2C_driver_SendStop();                                     // Added by Jason Chen, 2013.11.18

  max17047Version = (uint16_t)readValue[1]*256 +(uint16_t)readValue[0];
  if((result == ERR_OK)&&(max17047Version == 0x4000))
    max17047Exist = 1;  
  else 
  {
     max17047Exist = 0; 
  }
}


void max17047_process(void)
{
//volatile byte data = 0;
	
	if(max17047Exist) 
	{	  
  	DisableInterrupts; 
  	max17047Voltage = max17047_ReadReg(MAX17047_AVERAGE_V_CELL); 
  	max17047Voltage = max17047Voltage >> 3;
  	
  	if(max17047Voltage >= 4800)                  // Added by Jason Chen, 2014.12.08     The reading is at least 3.00V
  	  max17047Voltage_old = max17047Voltage;    // It's normal, 2014.12.08
  	else
      max17047Voltage = max17047Voltage_old;    // discard current reading, use previous one, 2014.12.08  	  
  	  	
  //max17047Current = (int16_t)max17047_ReadReg(MAX17047_AVERAGE_CURRENT);
  	EnableInterrupts; 
	} 
	else
	  max17047Voltage = 0;
	//max17047Current = 0;
}
#endif
