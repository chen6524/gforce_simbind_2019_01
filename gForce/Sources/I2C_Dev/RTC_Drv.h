/***************************************************************************
 *
 *            Copyright (c) 2011 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 215 Konrad Cres.
 * Markham, ON L3R 8T9
 * Canada
 *
 * Tel:   (905) 479-0148
 * Fax:   (905) 479-0149
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#ifndef _RTC_DRV_H_
#define _RTC_DRV_H_

//#include "hcc_types.h"
//#include "derivative.h"


#define MAX_LENGTH      10

#define RTC_POC         0x01
#define RTC_TEST        0x01
#define RTC_BLD         0x02

#define DEVICE_ADDRESS  0x30                                    //0x60

//#define SEND_COMPLETE    0x01
//#define RECV_COMPLETE    0x10
//extern volatile byte I2C_transfer_complete;

extern byte RTC_Buffer[MAX_LENGTH];

enum
{
	//RTC Register Address
	RTC_CTL_ST1           = DEVICE_ADDRESS|0,      // Control status 1
	RTC_CTL_ST2           = DEVICE_ADDRESS|1,      // Control status 2
	RTC_REAL_DATA1        = DEVICE_ADDRESS|2,      // Year, Month, Date, Weekday, Hour, Minute,Second
	RTC_REAL_DATA2        = DEVICE_ADDRESS|3,      // Hour, Minute, Second
  RTC_INT1_ALARM        = DEVICE_ADDRESS|4,      // WeekDay, Hour, Minute Alarm
  RTC_FREE_REG          = DEVICE_ADDRESS|4,      // Free register
  RTC_INT2_ALARM        = DEVICE_ADDRESS|5,      // WeekDay, Hour, Minute Alarm
  RTC_OUT_SETUP         = DEVICE_ADDRESS|5,      // Output of user-set frequency
 	RTC_CLOCK_CORRECTION  = DEVICE_ADDRESS|6,      // Clock Correctioon register
	RTC_FREE_REGISTER     = DEVICE_ADDRESS|7,      // Free register
};


#define BCD2BIN_t(x) ((x&0xF) + ((x & 0xF0) >> 4)*10 )

extern byte Initialize_RTC(void);
extern byte bin2bcd (byte x);
extern char Flip_Bits(char x);

extern byte Get_YearMonthDay(byte *date_time);
extern void Set_YearMonthDay(byte *date_str); 
extern void Change_12or24(bool hour12or24);
extern byte Register_Read(byte RTC_Register_Address);
extern void Register_Write(byte RTC_Register_Address, byte register_value);

extern void RTC_Init(byte mode);
extern byte rtcGetRegisterValue(byte registerAddr);
#endif