/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
  * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/

#include "gft.h"
#include "I2C_driver.h"
#include "MAX44000.h"
#include <hidef.h>

#include "mcu.h"
#include "kbi.h"

volatile bool ProxIntState = FALSE; // keep track of proxymity sensor interrupt status 
word prox_result;
//uint8_t send_Buf[16];


/*******************************************************************************
* Function Name  : prox_WriteReg
* Description    : write a Byte to I2C Bus
* Input          : deviceAddr is the I2C address of the device
*                  WriteAddr is the register address you want to write to
*                  pBuffer contains bytes to write
* Output         : None
* Return         : None
*******************************************************************************/
void prox_WriteReg(uint8_t regAddr,uint8_t regValue) 
{
  uint8_t send_Buf[2];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = regValue;
  
  (void)I2C_driver_SelectSlave(PROXIMITY_DEV_ADDR);  
  (void)I2C_driver_SendBlock(send_Buf,2,&prox_result);    
  (void)I2C_driver_SendStop();
  
}


/*******************************************************************************
* Function Name  : prox_ReadReg
* Description    : read a byte from I2C Bus
* Input          : deviceAddr is the I2C address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/
uint8_t prox_ReadReg(uint8_t regAddr) 
{
  uint8_t result = 0;
  uint8_t readValue;
  
  result  = I2C_driver_SelectSlave(PROXIMITY_DEV_ADDR);
  result |= I2C_driver_SendBlock(&regAddr,1,&prox_result);  
  result |= I2C_driver_RecvBlock(&readValue,1,&prox_result);
  (void)I2C_driver_SendStop();
//result |= I2C_driver_SendStop();   // This Stop signal is sent by RecvBlock() automatically
  return readValue; 
    
}

uint8_t ClearInterruptFlag(void) 
{
  return prox_ReadReg(PROX_INT_STATUS);  
}

void prox_enableInterupt(void) 
{
  (void)ClearInterruptFlag();
  prox_WriteReg(PROX_MAIN_CFG,0x32);  
}

void prox_disableInterupt(void) 
{
  (void)ClearInterruptFlag();
  prox_WriteReg(PROX_MAIN_CFG,0x30);  
}


void prox_shutdown(void) 
{
   (void)ClearInterruptFlag();
   prox_WriteReg(PROX_MAIN_CFG,0x20);
}


uint8_t prox_getpinstatus(void) 
{
     uint8_t ret;
     
     if(PTGD_PTGD1 == 0) 
     {
        ret = 1;
        (void)ClearInterruptFlag();         
     } else ret= 0;
     
     return ret;      
}

uint16_t prox_ReadWord(uint8_t regAddr) 
{
  uint8_t result = 0;
  uint8_t readValue[2];
   
  result  = I2C_driver_SelectSlave(PROXIMITY_DEV_ADDR);
  result |= I2C_driver_SendBlock(&regAddr,1,&prox_result);  
  result |= I2C_driver_RecvBlock(readValue,1,&prox_result);

  regAddr++;
  result |= I2C_driver_SendBlock(&regAddr,1,&prox_result);  
  result |= I2C_driver_RecvBlock(&readValue[1],1,&prox_result);
  (void)I2C_driver_SendStop();                                              // Added by Jason Chen, 2013.11.18
//result |= I2C_driver_SendStop();                                          // This Stop signal is sent by RecvBlock() automatically
  
  return (uint16_t)( ((uint16_t)(readValue[0] & 0x7F)<<8) + readValue[1]);
    
}

#define ALS_UPPER_THRESHOLD        5000u
#define ALS_LOWER_THRESHOLD        1000u

#define PROX_THRESHOLD_VALUE       200//32    Set 100 is based on Kashif's requirement of email

void prox_init(byte cur) 
{
  volatile byte data=0; 
#if 0
  volatile byte data=0; 

  // Step 1---> Clear all interrupt flags
  data = ClearInterruptFlag();
  
  // Step 2---> Set Threshold, and Threshold Persist Timer
  prox_WriteReg(PROX_ALS_UPTHR_H, (ALS_UPPER_THRESHOLD >> 8)&0x3F ); 
  prox_WriteReg(PROX_ALS_UPTHR_L, (ALS_UPPER_THRESHOLD >> 0)&0xFF ); 
  prox_WriteReg(PROX_ALS_LOTHR_H, (ALS_LOWER_THRESHOLD >> 0)&0x3F ); 
  prox_WriteReg(PROX_ALS_LOTHR_L, (ALS_LOWER_THRESHOLD >> 0)&0xFF );  
  prox_WriteReg(PROX_THR_INDICATOR,0x40);
  prox_WriteReg(PROX_THRESHOLD, PROX_THRESHOLD_VALUE );
  prox_WriteReg(PROX_THR_PERTIMER, 0x8); 
  // Step 3---> Set ALS 14bits mode
  prox_WriteReg(PROX_RECV_CFG, 0xF0); 
  // Step 4---> Set IR LED Current
  prox_WriteReg(PROX_XMIT_CFG, 0x8);    // 
  // Step 5---> Set ALS/PROX Mode, and Enable ALS, Proximity interrupt , Don't use default factory-trim settings
  prox_WriteReg(PROX_MAIN_CFG, 0x16);//MODE_PROX_ONLY); 16   
  // Step 6---> Set new green channel gains and infrared channel gains
  prox_WriteReg(PROX_GRN_TRIM, 0xFE);           // default value ---> 0x80
  prox_WriteReg(PROX_INFRARED_TRIM, 0x80);
  prox_WriteReg(PROX_MAIN_CFG, 0x16);//MODE_PROX_ONLY); 
  data = prox_ReadReg(PROX_INFRARED_TRIM);
  data = prox_ReadReg(PROX_MAIN_CFG);
  data = prox_ReadReg(PROX_THRESHOLD);
  data = prox_ReadReg(PROX_THR_PERTIMER);
  data = prox_ReadReg(PROX_RECV_CFG);
  data = prox_ReadReg(PROX_XMIT_CFG);
  data = prox_ReadReg(PROX_GRN_TRIM);
  data = prox_ReadReg(PROX_INFRARED_TRIM);
  data = prox_ReadReg(PROX_THR_INDICATOR);
  data =prox_ReadReg(PROX_INT_STATUS);  
#else
  
  // Step 1---> Clear all interrupt flags
  data = ClearInterruptFlag();
  
  // Step 2---> Set Threshold, and Threshold Persist Timer
  prox_WriteReg(PROX_ALS_UPTHR_H, (ALS_UPPER_THRESHOLD >> 8)&0x3F ); 
  prox_WriteReg(PROX_ALS_UPTHR_L, (ALS_UPPER_THRESHOLD >> 0)&0xFF ); 
  prox_WriteReg(PROX_ALS_LOTHR_H, (ALS_LOWER_THRESHOLD >> 0)&0x3F ); 
  prox_WriteReg(PROX_ALS_LOTHR_L, (ALS_LOWER_THRESHOLD >> 0)&0xFF ); 
   
  prox_WriteReg(PROX_THR_INDICATOR,0x40);
  prox_WriteReg(PROX_THRESHOLD, PROX_THRESHOLD_VALUE );
  
  prox_WriteReg(PROX_THR_PERTIMER, PROX_TRIGER_16CONSECUTIVE);   // 8 Consecutive trigger
  
  // Step 3---> Set ALS 8bits mode
  prox_WriteReg(PROX_RECV_CFG, 0xFC); 
  
  
  // Step 4---> Set IR LED Current
 if(cur ==1 )
  	data = PROX_DRIVER_80mA;
 else if(cur <= 0x0F)
  	data = cur;
 else data = PROX_DRIVER_80mA;
 
 prox_WriteReg(PROX_XMIT_CFG, data);    // Changed by Jason Chen for Show, 2015.01.06//  Set the current to 80mA   chain guard is 30mA
  
  // Step 5---> Set PROX only Mode, and Enable Proximity interrupt , and use default factory-programmed gains
  prox_WriteReg(PROX_MAIN_CFG, 0x32);//0x36 -- > Prox only, 0x32 -- > ALS/PROX
  
   
  // Step 6---> Set new green channel gains and infrared channel gains
  prox_WriteReg(PROX_GRN_TRIM,      0x5F);           // default value ---> 0x80
  prox_WriteReg(PROX_INFRARED_TRIM, 0xA6);
//prox_WriteReg(PROX_MAIN_CFG, 0x16);//MODE_PROX_ONLY); 
  

#endif  
}


uint16_t ALS_ADC_Data;
uint8_t  PROX_ADC_Data;
uint8_t  ALS_newData_Flag = 0;
uint8_t  PROX_newData_Flag = 0;


void processProx(void)
{
	uint8_t flagValue;
	volatile byte data = 0;
	if(ProxIntState)
	{
		ProxIntState = FALSE;
    DisableInterrupts; 		
		flagValue = ClearInterruptFlag();
    EnableInterrupts;
		if(flagValue & IRQ_STATUS_ALSINTS) 
		{
      DisableInterrupts; 
		  ALS_ADC_Data = prox_ReadWord(PROX_ADCALS_HIGH);
      EnableInterrupts;
		  if(ALS_ADC_Data & ALS_ADC_OFL) 
		  {
		    ALS_newData_Flag = 0xFF;      // Indicate Overflow
		  } 
		  else
		    ALS_newData_Flag = 1;    
		}

		if(flagValue & IRQ_STATUS_PRXINTS) 
		{
      DisableInterrupts; 
		  PROX_ADC_Data = prox_ReadReg(PROX_ADCPROX_BYTE);
      EnableInterrupts;
		       
		}
	    //enable the kbi for proximity
      DisableInterrupts; 
	    data = ClearInterruptFlag();
      EnableInterrupts;
      KBIPE_KBIPE1 =1;		
	}   
}

void poxReadADC(void)
{

PROX_ADC_Data = prox_ReadReg(PROX_ADCPROX_BYTE);


}
