/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
  * http:  www.artaflex.com

 *
 ***************************************************************************/
#ifndef _MAX14676_H_
#define _MAX14676_H_

#include "typedef.h"



#define MAX14676_DEV_ADDR 0x28 


#define MAX14676_DEV_ADDR2 0x36  


#define MAX14676_PWR_CFG_ADDR 					0x1E
#define MAX14676_ILIMCNTL_ADDR                  0x0A
#define MAX14676_CHGCNTLA                       0x0B
#define MAX14676_CHGCNTLB                       0x0C
#define MAX14676_LDOCFG                         0x16

#define MAX14676_STATUS 				        0x00

void max14676_init(void);
 void max14676_GetVer(void);

uint8_t max14676_ReadReg(uint8_t * ptr,uint8_t regAddr);


 uint8_t max14676_ReadReg2(uint16_t * pReadVal, uint8_t regAddr);


 void max14676_process(void);

 uint8_t max14676_GetBatteryCharge(void);
 uint16_t max14676_GetBatteryChargeVoltage( void);
 void max14676_poweroff(void);

extern uint8_t max14676Exist;
extern uint8_t max14676ChargeStat;
extern uint16_t max14676Voltage;
extern uint16_t max14676Voltage_old ;                  // 2014.12.08
extern uint8_t max14676Charge ;

#endif
