//
// File:        radio.h
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//
#ifndef _RADIO_H_
#define _RADIO_H_

#include "bcd_common.h"            // Jason Chen, SEP.11, 2013

//////////////////////////////////////////////////////////////////////////////////
// definitions/macros
#define RADIO_STACK_TIMER_UNITS         4//CYFISNP_NODE_TIMER_UNITS
//#define RADIO_STACK_TIMER_UNITS           10//CYFISNP_NODE_TIMER_UNITS                   // Added by Jason Chen, Testing Run period

// message types
#define MESSAGE_TYPE_POWER_OFF          10
#define MESSAGE_TYPE_POWER_ON           11
#define MESSAGE_TYPE_IMMEDIATE          12
#define MESSAGE_TYPE_HIT_DATA           13
#define MESSAGE_TYPE_UPLOAD_MORE        14
#define MESSAGE_TYPE_UPLOAD_LAST        15

// error codes when registering a message
#define RADIO_STACK_REGISTER_MESSAGE_ERR_USB            (unsigned char)-9    // 0xF7
#define RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND      (unsigned char)-8    // 0xF8
#define RADIO_STACK_REGISTER_MESSAGE_ERR_ALREADY_BOUND  (unsigned char)-7    // 0xF9
#define RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_2       (unsigned char)-6    // 0xFA
#define RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_1       (unsigned char)-5    // 0xFB
#define RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2        (unsigned char)-4    // 0xFC
#define RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_1        (unsigned char)-3    // 0xFD
#define RADIO_STACK_REGISTER_MESSAGE_ERR_USE            (unsigned char)-2    // 0xFE
#define RADIO_STACK_REGISTER_MESSAGE_ERR_INIT           (unsigned char)-1    // 0xFF
#define RADIO_STACK_REGISTER_MESSAGE_SUCCESS            0

// states
#define RADIO_STACK_IDLE                0
#define RADIO_STACK_BINDING             1
#define RADIO_STACK_NOT_BOUND           2
#define RADIO_STACK_CONNECTING          3
#define RADIO_STACK_PINGING             4
#define RADIO_STACK_DATA_MODE           5


extern 

//////////////////////////////////////////////////////////////////////////////////
// prototypes
                // must be called by the App after first power on and after every wake up
void            Radio_Stack_PowerOn_Init(void);
                // can be called by the App to register a message
unsigned char   Radio_Stack_Register_Message(unsigned char *psrc, unsigned int len, unsigned char type);
                // must be called periodically by the App (every 35-50 mSec)
#if ENABLE_BACK_CHANNEL_RECV                 
byte            Radio_Stack_Periodic(byte *recv_buffer);                
#else
void            Radio_Stack_Periodic(void);
#endif
                // can be called by the App to upload next message (UPLOAD_MORE, UPLOAD_LAST)
char            Radio_Stack_Upload_Ready(void);
                // can be called by the App to send next message (POWER_ON, POWER_OFF, IMMEDIATE, HIT)
unsigned char   Radio_Stack_State(void);
                // can be called by the App to know whether we are bound to the Hub or not
char            Radio_Stack_isBound(void);
                // can be called by the App to bind with the Hub, if we are not already bound
unsigned char   Radio_Stack_Bind(void);
                // can be called by the App to unbind from the Hub, good for when we want to bind to a new Hub
char            Radio_Stack_UnBind(void);
                // can be called by the App to display some info from the Stack
void            Radio_Stack_Info(void);
                // can be called by the App to get the network node number
unsigned char   Radio_Stack_Node_Number(void);
                // can be called by the App to set a time
void            Radio_Stack_TimeSet(unsigned int *pTimer, unsigned int time);
                // can be called by the App to check a timer
char            Radio_Stack_TimeExpired(unsigned int *pTimer);
                // can be called by the App to abort all radio/stack and turn the radio off
void            Radio_Stack_Abort(void);
                // can be called by the App to tell the stack to ignore Usb attachment
char            Radio_Stack_Usb_Ignore(void);
                // can be called by the App to tell the stack to resume detecting Usb attachment
char            Radio_Stack_Usb_Resume(void);
                // can be called by the App to know whether Sync'ing has been requested or not
char            Radio_Stack_Sync(void);
                // can be called to start getting temperature from sensor
char            Radio_Stack_Get_Temperature(void);
                // can be called to read temperature from sensor
char            Radio_Stack_Read_Temperature(unsigned int *pTemp, unsigned int *pVolt, unsigned char *pRssi);

				                // test the radio spi bus
byte             Radio_SPI_test(void);
				//test the radio 
byte            Radio_Stack_Loop_Test(void);
				
byte            Radio_Stack_Read_Lp_Test(byte *data1, byte *data2, byte *pRssi);

bool            Radio_Stack_Write_Lp_Test(byte data1, byte data2);
void            gResetRadio(void);
void            gRadioWakup(void);
void            gRadioSleep(void);


#endif  // #ifndef _RADIO_H_
