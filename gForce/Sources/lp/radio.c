//
// File:        radio.c
// Date:        July 2012
// Author:      Faraz Ossareh
// Company:     Artaflex Inc.
// Project:     gFT
// Micro:       MC9S08JM60, 48-Pin VQFN
// Compiler:    FreeScale CodeWarrior 6.3
//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//  Sep,30 2012 First Release
//

//////////////////////
// includes
#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "common.h"
#include "CYFISNP_NODE.h"
#include "CYFISNP_NODE_Protocol.h"
#include "CYFISNP_NODE_Time.h"
#include "radio.h"

#include "bcd_common.h"               // Jason Chen, SEP.11, 2013
#include "mcu.h"


////////////////////////
// definitions
#define ENABLE_6DS3

#ifdef fDEBUG
    #define CYFISNP_NODE_OutStr outstr
    #define CYFISNP_NODE_OutHex outhex
    #define CYFISNP_NODE_OutChar putch
#else
    #define CYFISNP_NODE_OutStr(msg)
    #define CYFISNP_NODE_OutHex(data)
    #define CYFISNP_NODE_OutChar(data)
#endif  // fDEBUG

// internal types - do not expose them in radio.h (other types start from 10)
#define MESSAGE_TYPE_BINDING            1
#define MESSAGE_TYPE_SHELF_MODE_U       2   // U1, U0 messages
#define MESSAGE_TYPE_SHELF_MODE_D       3   // D1, D0 messages

#define CHUNK_BINDING        0      // no packet transmission - internal use
#define CHUNK_SHELF_MODE_U   1      // payload size will be  2   // U1, U0 messages
#define CHUNK_SHELF_MODE_D   4      // payload size will be  5   // D1, D0 messages
#define CHUNK_POWER_OFF     10      // payload size will be 11
#define CHUNK_POWER_ON      11      // payload size will be 12
#define CHUNK_IMMEDIATE     12      // payload size will be 13
#define CHUNK_HIT_DATA      13      // payload size will be 14

#define STATE_IDLE          0
#define STATE_BUSY          1

#define USB_OFF             0
#define USB_ON              1

#define PERIODIC_TIME_LONG  (  32 / CYFISNP_NODE_TIMER_UNITS)
#define PERIODIC_TIME_SHORT (  16 / CYFISNP_NODE_TIMER_UNITS)
#define ACKNOWLEGE_TIME     ( 250 / CYFISNP_NODE_TIMER_UNITS)
#define USB_SENSE_TIME      (10500/ CYFISNP_NODE_TIMER_UNITS)
#define SYNC_WAIT_TIME      (15000/ CYFISNP_NODE_TIMER_UNITS)   // Hub sends Sync Beacons for 12.5 Sec

// idle -> Reg -> Ack -> Send -> Wait -> Ack -> ...
#define UPLOAD_STATE_IDLE               0
#define UPLOAD_STATE_REGISTERED         1
#define UPLOAD_STATE_ACKNOELEGED        2   // can be flagged momentarily
#define UPLOAD_STATE_SENDING_DATA       3   // can be flagged
#define UPLOAD_STATE_WAITING            4
#define UPLOAD_STATE_MASK               0x7F
#define UPLOAD_STATE_LAST_FLAG          0x80

#define USB_MODE_TIMER_IDLE             0
#define USB_MODE_TIMER_SET              1
#define USB_MODE_TIMER_EXPIRED          2

#define DATA_UPLOAD_END                 0
#define DATA_UPLOAD_CONTI               1

#define SYNC_MODE_IDLE                  0
#define SYNC_MODE_TIMER_SET             1
#define SYNC_MODE_TIMER_EXPIRED         2
#define SYNC_MODE_SYNCED                3

#define TEMP_SENSOR_IDLE                0
#define TEMP_SENSOR_START               1
#define TEMP_SENSOR_GET_DATA            2
#define TEMP_SENSOR_END                 3
#define TEMP_SENSOR_READ_DATA           4

#define TEMP_SENSE_CONSEC_CNT           3

////////////////////////
// internal vars
static CYFISNP_NODE_API_PKT txApiPkt={0}; 	        // Local Tx Data buffer
static CYFISNP_NODE_API_PKT *pRxApiPkt=(byte*)0;    // Ptr to Rx API packet
static struct {
    byte *pFirst;   // pointer to a pre-buffer to send, ahead of pSrc
    byte *pSrc;     // pointer to the buffer to send
    word ToSend;    // length of the buffer
    byte Type;      // type of the buffer
    //
    byte Chunk;     // chunk of data to transmit based on the Type
    word Sent;      // number of bytes Sent so far
    struct 
    {
        byte Flag;
        byte SeqNo;
        //word BufLen;                               // 09.11
        word impactCount;                            // 09.11
        word SessionCount;                          // 2017.04.03
    } Upload;       // upload state
    struct 
    {
        byte Flag;
        word Timer;
    } Sync;
    word AckTimer;  // acknowledge timer
    byte State;     // overall state
} Radio_Struct={(byte*)0,(byte*)0,0,0,0,0,0,0,0,0,0};

static word PeriodicTimer=0;
static struct {
    bool Mode;
    bool Flag;
    word Timer;
    bool IgnoreU1;  // will prevent U1 message
} Stack_Usb={0,0,0,0};
static struct {
    byte Flag;
    byte Spi1C1;
    byte Spi1C2;
    byte Spi1Br;
} Stack_Spi={0,0,0,0};
static byte const Usb_Start_Message[1]   = { 1 };            // U1
static byte const Usb_End_Message[1]     = { 0 };            // U0
static byte       Data_Upload_Message[4] = { 1, 0, 0, 0};    // D1/D0
static char       Build[13];  // 13 is the max of all CHUNKs, can be used for POWER_OFF, POWER_ON, IMMEDIATE and HIT_DATA/UPLOAD
static struct {
    byte Flag;
    word Temp;
    word Volt;
    byte Rssi;
    byte Abort;
    byte Buf[12];   // 0-1:flag, 2-3:temp, 4-5:volt, 6-11:mid
    byte Mid[6];
    byte Cnt;
} TempSense={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


#define LP_TEST_IDLE                0
#define LP_TEST_START               1
#define LP_TEST_GET_DATA            2
#define LP_TEST_END                 3
#define LP_TEST_READ_DATA           4
#define LP_TEST_SEND_DATA           5

static struct {
    byte Flag; 
    byte Rssi;
    byte Abort;
    byte BufTx[6];   
    byte BufRx[6];
    byte Mid[6];
    byte Cnt;
	byte status;
} LpTest={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

bool Usb_Mode=USB_OFF;

////////////////////////
// external vars
extern byte CYFISNP_NODE_Pwr_Type;

////////////////////////
// prototypes
extern bool netParamsPresent(void);
extern void ShelfMode(bool state);
extern void CYFISNP_NODE_PowerOn_Init(void);


#include "gLp.h"
extern LP_PWR_OFF_PKT Power_Off_Message;

#if ENABLE_RECV_WINDOW_TIMER
extern word RecvWindowTimer;
#endif

extern byte wakeup_poweroff;        // Added by Jason Chen, 2015.04.01

#if 1//SLEEP_MODE_RADIO_OFF

void USB_MODE_to_IDLE(void) 
{
  
   Radio_Struct.State = STATE_IDLE;
}

#endif


//////////////////////////////////////////////////////////////////////////////////
//
//
void Radio_Stack_PowerOn_Init(void)
{
    byte temp=0;
    
    // protect against Harry's BoootLoader (Dec 18, 12)
    static const byte *pFlash=(byte*)CYFISNP_NODE_EEP_PHY_ADR;
    static const byte ffFlash[8] = { 0xFF,0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF,0xFF };
    
    if( memcmp(pFlash, ffFlash, 8) == 0 ) 
    {
        outstr("- Found uninitialized 0xFF Flash -> Resetting to 0x00...\r\n");
        CYFISNP_NODE_Unbind();
    }
    outstr("\r\n- gsFlash: "); outhex(pFlash[0]); outhex(pFlash[1]); outhex(pFlash[2]); outhex(pFlash[3]);
                               outhex(pFlash[4]); outhex(pFlash[5]); outhex(pFlash[6]); outhex(pFlash[7]);
    outstr("\r\n");
    
    
    // hw-wise
#ifdef fDEBUG
  #if (PCB_TYPE==PCB_DEMOJM)
       
    //////////////////////////////////////////////////////////////////////////////////
    // SCI1 (shared with PTE0 and PTE1)
    SCI1BD = BUSCLK/16/SCIBAUD;     // SCI1BDH+SCI1BDL
    SCI1C1 = 0x00;    // |  LOOPS | SCISWAI |  RSRC  |    M    |  WAKE |  ILT  |   PE   |   PT   |
    SCI1C2 = //SCI1C2_TIE_MASK      |
             //SCI1C2_TCIE_MASK     |
             //SCI1C2_RIE_MASK      |
             //SCI1C2_ILIE_MASK     |
             SCI1C2_TE_MASK         |
             SCI1C2_RE_MASK         |
             //SCI1C2_RWU_MASK      |
             //SCI1C2_SBK_MASK      |
             0;       // | TIE(0) | TCIE(0) | RIE(0) | ILIE(0) | TE(1) | RE(1) | RWU(0) | SBK(0) |
    SCI1C3 = 0x00;    // |   R8   |   T8    | TXDIR  | TXINV   | ORIE  | NEIE  | FEIE   | PEIE   |

    // dummy read of sci status and data registers
    temp = SCI1S1;
    temp = SCI1D;

  #elif (PCB_TYPE==PCB_gFT)

    //////////////////////////////////////////////////////////////////////////////////
    // SCI2 (shared with PTC3 and PTC5)
    SCI2BD = BUSCLK/16/SCIBAUD;     // SCI2BDH+SCI2BDL
    SCI2C1 = 0x00;    // |  LOOPS | SCISWAI |  RSRC  |    M    |  WAKE |  ILT  |   PE   |   PT   |
    SCI2C2 = //SCI2C2_TIE_MASK      |
             //SCI2C2_TCIE_MASK     |
             //SCI2C2_RIE_MASK      |
             //SCI2C2_ILIE_MASK     |
             SCI2C2_TE_MASK         |
             SCI2C2_RE_MASK         |
             //SCI2C2_RWU_MASK      |
             //SCI2C2_SBK_MASK      |
             0;       // | TIE(0) | TCIE(0) | RIE(0) | ILIE(0) | TE(1) | RE(1) | RWU(0) | SBK(0) |
    SCI2C3 = 0x00;    // |   R8   |   T8    | TXDIR  | TXINV   | ORIE  | NEIE  | FEIE   | PEIE   |

    // dummy read of sci status and data registers
    temp = SCI2S1;
    temp = SCI2D;

  #endif  // PCB_TYPE
  
#endif  // fDEBUG  

    // SPI1 pins on PortE
#if (PCB_TYPE==PCB_DEMOJM)
    CYFISNP_NODE_Radio_Deselect();   PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, SPI1 SS1 high
#elif (PCB_TYPE==PCB_gFT)
    CYFISNP_NODE_Radio_Deselect();   PTEDD_PTEDD0 = OUTPUT;      // PTE0 output, SPI1 SS1 high
    PTED_PTED7 = 1;                  PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, Ext Flash Chip Select
    // PTF0 controls the power to the Radio
    RADIO_PWR_OFF();                 PTFDD_PTFDD0 = OUTPUT;      // PTF0 output, radio power control
    
    // PTE1 controls the power to the Sensor hardware
    //SENSOR_PWR_OFF();                PTEDD_PTEDD1 = OUTPUT;      // PTE1 output, sensor power control
#endif  // PCB_TYPE

    SpiCLK_Low();       PTEDD_PTEDD6 = OUTPUT;      // PTE6 output, SPI1 SCK1 low
    SpiMOSI_Low();      PTEDD_PTEDD5 = OUTPUT;      // PTE5 output, SPI1 MOSI1 low
    PTEPE_PTEPE4 = PULLUP_ON; PTEDD_PTEDD4 = INPUT; // PTE4 input,  pullup enabled on SPI1 MISO1

#ifdef fDEBUG
  #if (PCB_TYPE==PCB_DEMOJM)
    // SCI1 pins on PortE
    PTEPE_PTEPE1 = PULLUP_ON; PTEDD_PTEDD1 = INPUT; // PTE1 input,  pullup enabled on SCI1 RxD1
    PTED_PTED0   = HIGH;      PTEDD_PTEDD0 = OUTPUT;// PTE0 output, SCI1 TxD1 high
  #elif (PCB_TYPE==PCB_gFT)
    // SCI2 pins on PortC
    PTCPE_PTCPE5 = PULLUP_ON; PTCDD_PTCDD5 = INPUT; // PTC5 input,  pullup enabled on SCI2 RxD2
    //PTCD_PTCD3   = HIGH;      PTCDD_PTCDD3 = OUTPUT;// PTC3 output, SCI2 TxD2 high
  #endif  // PCB_TYPE
#endif  // fDEBUG

#if (PCB_TYPE==PCB_DEMOJM)
    // Irq on PortB
    PTBPE_PTBPE4 = PULLUP_ON;  PTBDD_PTBDD4 = INPUT; // PTB4 input,     with pullup
#elif (PCB_TYPE==PCB_gFT)
    // Irq on PortG
    PTGPE_PTGPE0 = PULLUP_ON;  PTGDD_PTGDD0 = INPUT; // PTG0 input,     with pullup
#endif  // PCB_TYPE
    
    // sw-wise
    (void)memset(&txApiPkt, 0, sizeof(txApiPkt));
    pRxApiPkt = (byte*)0;
    (void)memset(&Radio_Struct, 0, sizeof(Radio_Struct));
    PeriodicTimer = 0;
    (void)memset(&Stack_Usb, 0, sizeof(Stack_Usb));
    (void)memset(&Stack_Spi, 0, sizeof(Stack_Spi));
    (void)memset(Build, 0, sizeof(Build));
    (void)memset(&TempSense, 0, sizeof(TempSense));

    CYFISNP_NODE_PowerOn_Init();
}

//
#if (SLEEP_MODE_RADIO_OFF == 0)
//static 
#endif
void Radio_Stack_Save_and_Prog_Spi(void)
{
    if( Stack_Spi.Flag == 0x55 )
        return;

    //putch('+');
#if (PCB_TYPE==PCB_DEMOJM)
    // deselect Radio Chip Select
    PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, SPI1 SS1 high
#elif (PCB_TYPE==PCB_gFT)
    // deselect Radio Chip Select & Ext Flash Chip Select
    PTED_PTED0 = 1;     PTEDD_PTEDD0 = OUTPUT;      // PTE0 output, SPI1 SS1 high
    PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, Ext Flash Chip Select
#endif  // PCB_TYPE
    // save SPI regs here - set a flag that you did it
    Stack_Spi.Spi1C1 = SPI1C1;
    Stack_Spi.Spi1C2 = SPI1C2;
    Stack_Spi.Spi1Br = SPI1BR;
    Stack_Spi.Flag   = 0x55;

    // program your own
    SPI1C1 = //SPI1C1_SPIE_MASK     |
             SPI1C1_SPE_MASK        |
             //SPI1C1_SPTIE_MASK    |
             SPI1C1_MSTR_MASK       |
             //SPI1C1_CPOL_MASK     |
             //SPI1C1_CPHA_MASK     |
             //SPI1C1_SSOE_MASK     |
             //SPI1C1_LSBFE_MASK    |
             0;
    SPI1C2 = //SPI1C2_SPMIE_MASK    |
             //SPI1C2_SPIMODE_MASK  |
             //SPI1C2_MODFEN_MASK   |
             //SPI1C2_BIDIROE_MASK  |
             //SPI1C2_SPISWAI_MASK  |
             //SPI1C2_SPC0_MASK     |
             0;

  #if BUSCLK==24000000L
    #if SPICLK==4000000L
        SPI1BR = //SPI1BR_SPPR2_MASK    |
                 SPI1BR_SPPR1_MASK      |
                 //SPI1BR_SPPR0_MASK    | // BUSCLK divide by 3
                 //SPI1BR_SPR2_MASK     |
                 //SPI1BR_SPR1_MASK     |
                 //SPI1BR_SPR0_MASK     | // divide by 2
                 0;
    #elif SPICLK==2000000L
        SPI1BR = //SPI1BR_SPPR2_MASK    |
                 SPI1BR_SPPR1_MASK      |
                 //SPI1BR_SPPR0_MASK    | // BUSCLK divide by 3
                 //SPI1BR_SPR2_MASK     |
                 //SPI1BR_SPR1_MASK     |
                 SPI1BR_SPR0_MASK       | // divide by 4
                 0;
    #elif SPICLK==1000000L
        SPI1BR = //SPI1BR_SPPR2_MASK    |
                 SPI1BR_SPPR1_MASK      |
                 //SPI1BR_SPPR0_MASK    | // BUSCLK divide by 3
                 //SPI1BR_SPR2_MASK     |
                 SPI1BR_SPR1_MASK       |
                 //SPI1BR_SPR0_MASK     | // divide by 8
                 0;
    #endif // SPICLK
  #endif // BUSCLK
}

//
#if (SLEEP_MODE_RADIO_OFF == 0)
//static 
#endif
void Radio_Stack_Restore_Spi(void)
{
    if( Stack_Spi.Flag == 0x55 ) {
        //putch('=');
        SPI1C1 = Stack_Spi.Spi1C1;
        SPI1C2 = Stack_Spi.Spi1C2;
        SPI1BR = Stack_Spi.Spi1Br;
        Stack_Spi.Flag = 0;

        // deselect Radio Chip Select
#if (PCB_TYPE==PCB_DEMOJM)
        PTED_PTED7 = 1;     PTEDD_PTEDD7 = OUTPUT;      // PTE7 output, SPI1 SS1 high
#elif (PCB_TYPE==PCB_gFT)
        PTED_PTED0 = 1;     PTEDD_PTEDD0 = OUTPUT;      // PTE0 output, SPI1 SS1 high    for Radio
#endif  // PCB_TYPE
    }
}

//
char *MakeBuild(word len)
{
    static const char *const pmonth[1+12] = { "???", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    char n;
    
    // +-0-+-1-+-2-+-3-+-4-+-5-+-6-+-7-+-8-+-9-+-A-+-B-+-C-+
    // | S | e | p |   | 1 | 9 |   | 2 | 0 | 1 | 2 |\0 |\0 |
    // +---+---+---+---+---+---+---+---+---+---+---+---+---+
    // |Msb|Lsb| 0 | 9 | 1 | 9 | 2 | 0 | 1 | 2 |\0 |\0 |\0 |
    // +---+---+---+---+---+---+---+---+---+---+---+---+---+
    
    // already built?
    if( Build[2]==0 ) {
        // no, make it
        (void)strcpy(Build, __DATE__);  // This macro expands to a string constant that describes the date on which the preprocessor is being run.
        // The string constant contains eleven characters and looks like "Feb 12 1996". If the day of the month is less than 10, it is padded with a space on the left.
        // If GCC cannot determine the current date, it will emit a warning message (once per compilation) and __DATE__ will expand to "??? ?? ????".
    
        for(n=0; n<(1+12); n++) {
            if( memcmp(Build, pmonth[n], 3)==0 ) {
                Build[0] = len / 256;       // Msb
                Build[1] = len % 256;       // Lsb

                Build[2] = (n/10) + '0';    // month
                Build[3] = (n%10) + '0';    // month

                if( Build[4] == ' ' )       // single digit day?
                    Build[4] = '0';
                
                Build[6] = Build[7];
                Build[7] = Build[8];
                Build[8] = Build[9];
                Build[9] = Build[10];
                Build[10] = 0;
                break;
            }
        }
        if( n >=(1+12) ) {
            Build[0] = len / 256;       // Msb
            Build[1] = len % 256;       // Lsb
            (void)strcpy(&Build[2], "????????");
        }
    } else {
        // yes, just put in the len
        Build[0] = len / 256;       // Msb
        Build[1] = len % 256;       // Lsb
    }
    
    return Build;
}

//
static byte Radio_Stack_Register_Error(byte code)
{
    switch(code) {
        case RADIO_STACK_REGISTER_MESSAGE_ERR_USB:              // 0xF7
            outstr("Error F7: Usb mode waiting...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND:        // 0xF8
            outstr("Error F8: Not Bound...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_ALREADY_BOUND:    // 0xF9
            outstr("Error F9: Already Bound...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_2:         // 0xFA
            outstr("Error FA: Upload state 2...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_1:         // 0xFB
            outstr("Error FB: Upload state 1...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2:          // 0xFC
            outstr("Error FC: Bad parameter 2...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_1:          // 0xFD
            outstr("Error FD: Bad parameter 1...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_USE:              // 0xFE
            outstr("Error FE: Already registered...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_ERR_INIT:             // 0xFF
            outstr("Error FF: Radio init...\r\n");
            break;
        case RADIO_STACK_REGISTER_MESSAGE_SUCCESS:              // 0x00
            break;
        default:
            outstr("Error ??: Unknown...\r\n");
            break;
    }
    return code;
}

//
byte Radio_Stack_Register_Message(byte *psrc, word len, byte type)
{
    BOOL badPara=FALSE;
    byte chunk=0;
    byte *pfirst=(byte*)0;

    // check input parameters
    switch(type) {
	    case MESSAGE_TYPE_BINDING:      // 1
            len = 0;    // must be, so that (Radio_Struct.ToSend - Radio_Struct.Sent) is zero and no packet gets transmitted
            break;
	    case MESSAGE_TYPE_SHELF_MODE_U: // 2                    1 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(1*CHUNK_SHELF_MODE_U)) )  // max 1 packet at a time (1 bytes)
                badPara = TRUE;
            break;
	    case MESSAGE_TYPE_SHELF_MODE_D: // 3                    4 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(1*CHUNK_SHELF_MODE_D)) )  // max 1 packet at a time (4 bytes)
                badPara = TRUE;
            break;
	    case MESSAGE_TYPE_POWER_OFF:    // 10                   10 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(10*CHUNK_POWER_OFF)) )  // max 10 packets at a time (100 bytes)
                badPara = TRUE;
            break;
	    case MESSAGE_TYPE_POWER_ON:     // 11                   11 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(10*CHUNK_POWER_ON)) )   // max 10 packets at a time (110 bytes)
                badPara = TRUE;
            break;
	    case MESSAGE_TYPE_IMMEDIATE:    // 12                   12 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(5*CHUNK_IMMEDIATE)) )   // max 5 packets at a time (60 bytes)
                badPara = TRUE;
            break;
	    case MESSAGE_TYPE_HIT_DATA:     // 13                   13 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(48*CHUNK_HIT_DATA)) )   // max 48 packets at a time (624 bytes)    48*13=39*16=624
                badPara = TRUE;
            break;
      case MESSAGE_TYPE_UPLOAD_MORE:  // 14
      case MESSAGE_TYPE_UPLOAD_LAST:  // 15                   13 bytes
            if( (psrc == (byte*)0) || (len==0)  || (len>(32*CHUNK_HIT_DATA)) )   // max 32 packets at a time (416 bytes)    32*13=26*16=416
                badPara = TRUE;
            break;
	    default:
	        badPara = TRUE;
	        break;
    }
    
    if( badPara )
        return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_1);

    // check whether already registered
    if( Radio_Struct.State != STATE_IDLE )
        return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_USE);
  
    // check whether we are in Usb timeout
    if( Stack_Usb.Flag == USB_MODE_TIMER_SET )
        return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_USB);

    // cancel Temperature Sensor if active
    if( TempSense.Flag != TEMP_SENSOR_IDLE ) {
        TempSense.Abort = 1;
        (void)Radio_Stack_Get_Temperature();
    }
            
    // register the message
	switch( type ) {
	    case MESSAGE_TYPE_BINDING:      // 1	    
	        if(CYFISNP_NODE_ForceBindingMode() == 0)                                                 // Added by Jason Chen, 2019.01.25
	        if( netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_ALREADY_BOUND);

	        // this message can be issued irrespective of Usb cable
            //if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
            //    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);

            chunk = CHUNK_BINDING;      // 0
            break;

	    case MESSAGE_TYPE_SHELF_MODE_U: // 2
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

	        // this message can be issued irrespective of Usb cable
            //if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
            //    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);

            chunk = CHUNK_SHELF_MODE_U; // 1
            break;

	    case MESSAGE_TYPE_SHELF_MODE_D: // 3
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

	        // this message can be issued irrespective of Usb cable
            //if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
            //    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);

            chunk = CHUNK_SHELF_MODE_D; // 4
            break;

	    case MESSAGE_TYPE_POWER_OFF:    // 10
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

	        // this message can be issued irrespective of Usb cable
            //if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
            //    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);

            chunk = CHUNK_POWER_OFF;    // 10
            //chunk = 14;                   //CHUNK_POWER_OFF;    // 10   // Changed from CHUNK_POWER_OFF to 14, 2015.01.05
            //pfirst = MakeBuild(len);              // <-- prolog   // no need, do NOT use (it is a single packet message)
            break;

	    case MESSAGE_TYPE_POWER_ON:     // 11
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

	        // this message can be issued irrespective of Usb cable
            //if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
            //    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);

            chunk = CHUNK_POWER_ON;     // 11
            pfirst = MakeBuild(len);                // <-- prolog
            break;

	    case MESSAGE_TYPE_IMMEDIATE:    // 12
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

	        // this message can be issued irrespective of Usb cable
            //if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
            //    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);

            chunk = CHUNK_IMMEDIATE;    // 12
            //pfirst = MakeBuild(len);              // <-- prolog   // no need, do NOT use (it is a single packet message)
            break;

	    case MESSAGE_TYPE_HIT_DATA:     // 13
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);
	 
#if (ENABLE_BACK_CHANNEL_RECV == 0)
            // in COIN mode
            if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )
                return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_2);                       
#endif            
               
            chunk = CHUNK_HIT_DATA;     // 13
            //pfirst = MakeBuild(len);                // <-- prolog
            break;

        case MESSAGE_TYPE_UPLOAD_MORE:  // 14
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

            // in WALL mode
            if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_COIN )
                return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_2);

            // we must have received a Beacon to us to proceed
            if( Radio_Struct.Upload.Flag != UPLOAD_STATE_ACKNOELEGED )
                return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_1);

            chunk = CHUNK_HIT_DATA;     // 13
            //pfirst = MakeBuild(len);                // <-- prolog
            // first the buffer is registered (and Tx'd) then D1 (meaning more to come)
            break;

        case MESSAGE_TYPE_UPLOAD_LAST:  // 15
	        if( !netParamsPresent() )
	            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_NOT_BOUND);

            // in WALL mode
            if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_COIN )
                return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_2);

            // we must have received a Beacon to us to proceed
            if( Radio_Struct.Upload.Flag != UPLOAD_STATE_ACKNOELEGED )
                return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_UPLOAD_1);

            chunk = CHUNK_HIT_DATA;     // 13
            //pfirst = MakeBuild(len);                // <-- prolog
            Radio_Struct.Upload.Flag |= UPLOAD_STATE_LAST_FLAG;  // is followed by D0 instead of D1
            // first the buffer is registered (and Tx'd) then D0 (meaning this was the last)
            break;

        default:
            return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_PARAM_1);
	}

    // start the radio    
  //if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL ) 
   {    
        RADIO_PWR_ON();
        CYFISNP_NODE_PowerOn_Init();

      	outstr("\r\nStarting Radio... ");
      	{   //-----------------------------;
            Radio_Stack_Save_and_Prog_Spi();
            if( CYFISNP_NODE_Start() == FALSE ) 
            {
                Radio_Stack_Restore_Spi();
                RADIO_PWR_OFF();
				
                return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_INIT);
            }
            Radio_Stack_Restore_Spi();
      	}   //-----------------------;
       	outstr("\r\n");
   }

	// all ok, register the message
	  Radio_Struct.pFirst  = pfirst;
	  Radio_Struct.pSrc    = psrc;
	  Radio_Struct.ToSend  = len;
	  Radio_Struct.Type    = type;
    Radio_Struct.Chunk   = chunk;
	  Radio_Struct.Sent    = 0;


    if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_COIN ) 
    {
        Radio_Struct.Upload.Flag  = UPLOAD_STATE_IDLE;
    } 
    else 
    {
        if( ((Radio_Struct.Type == MESSAGE_TYPE_UPLOAD_MORE) || (Radio_Struct.Type == MESSAGE_TYPE_UPLOAD_LAST)) &&
            ((Radio_Struct.Upload.Flag & UPLOAD_STATE_MASK) == UPLOAD_STATE_ACKNOELEGED) ) // Ack -> Send
        { 
            
            // Ack -> Send 
            Radio_Struct.Upload.Flag  = (Radio_Struct.Upload.Flag & UPLOAD_STATE_LAST_FLAG) + UPLOAD_STATE_SENDING_DATA;
            // first the registered message is transmitted and after that D1/D0 goes off.
       	    //Radio_Struct.Upload.SeqNo = 0;    // this will be reset at the time of U1 message (see below)
         //  	Radio_Struct.Upload.BufLen = Radio_Struct.ToSend;   // this is the length of the buffer that we transmit at this session, used for D1/D0 messages (see below)
        }
    }
    
                   
    Radio_Struct.AckTimer = 0;
    Radio_Struct.State    = STATE_BUSY;

    return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_SUCCESS);
}

void Delay1ms(word time_count) 
{
   while(time_count--) 
   {
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();      
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();      
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();  
     CYFISNP_NODE_Delay100uS();  
     
   }  
}


word ack_timer = 0;                                //09.11
byte upload_impact_ack = 0xFF;                     //09.11, Changed by Jason Chen from 0x00 to 0xFF
word impact_count = 0;                             //09.11
word Session_count= 0;                             //2017.04.03
extern void mTimer_TimeSet (unsigned int *pTimer );//09.11
extern word imSleepTimer;                          //09.11
#if ENABLE_BACK_CHANNEL_RECV    
byte Radio_Stack_Periodic(byte *recv_buffer)
#else
//
void Radio_Stack_Periodic(void)
#endif
{
    word LeftToSend;
#if ENABLE_BACK_CHANNEL_RECV        
    byte retValue_1 = 0;
#endif
    

    // [1]
    // not sooner than every 32/16 mSec - this timer in combination with the main() timer makes 32/48 mSec timing
    if( CYFISNP_NODE_TimeExpired(&PeriodicTimer) != TRUE )   
      #if ENABLE_BACK_CHANNEL_RECV    
          return 0;
      #else
          return;
      #endif
    
    CYFISNP_NODE_TimeSet(&PeriodicTimer, ((CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_COIN) ? PERIODIC_TIME_LONG : PERIODIC_TIME_SHORT));
    //                                                                                           COIN mode             WALL mode

    // [2]=[6]
    // nothing to do if not already bound ( [3],[4],[5],   ,[7],[8] )
    if( !netParamsPresent() ) {
        //-----------------------------;
        Radio_Stack_Save_and_Prog_Spi();
	    CYFISNP_NODE_Run(); // Poll SNP machine
        Radio_Stack_Restore_Spi();
        //-----------------------;
    #if ENABLE_BACK_CHANNEL_RECV    
        return 0;
    #else        
        return;
    #endif
    }

    // [5]              
    // how much is left to send?
    if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58)        // 09.11                                       1
       ||(Radio_Struct.Type == MESSAGE_TYPE_IMMEDIATE)                                     // 2016.03.14    
#if ENABLE_SESSION_START_END_RETRY    
       ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)      // Added by Jason Chen, 2015.09.24
       ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A)      // Added by Jason Chen, 2015.09.24      
#endif       
      )
    {      
        //if(!upload_flag) 
        {      
            LeftToSend = Radio_Struct.ToSend - Radio_Struct.Sent;
            // can't send more than one Chunk of data
            if( LeftToSend > (word)Radio_Struct.Chunk )
                LeftToSend = (word)Radio_Struct.Chunk;

            // last packet sent? ready to send more?
            if( (LeftToSend != 0) && (CYFISNP_NODE_eProtState == CYFISNP_NODE_DATA_MODE)         // in Data Mode
        		  		          && (CYFISNP_NODE_TxDataPend() == FALSE)/*&&(!upload_flag)*/ )            // no Tx pending
        		  		                                                    //09.11
        		{   			             		  		  		  		        
          	    // empty Tx packet in case (LeftToSend < Radio_Struct.Chunk), which can happen on the last packet
          	    if( LeftToSend < (word)Radio_Struct.Chunk )
          	        (void)memset(txApiPkt.payload, 0x00, CYFISNP_NODE_FCD_PAYLOAD_MAX);
          	                                     //0xFF
           	    // first byte is sequence number + number of valid bytes in the packet (important for the last packet)
       	        txApiPkt.payload[0] = ((Radio_Struct.Sent / Radio_Struct.Chunk)<<4) + (byte)LeftToSend;       // sequence number (0-15) / Len (1-13)
       	        // copy next chunk of data
       	        (void)memcpy(&txApiPkt.payload[1], &Radio_Struct.pSrc[Radio_Struct.Sent], LeftToSend);
       	        
       	         if((Radio_Struct.Sent + LeftToSend) >= Radio_Struct.ToSend) 
       	         {       	                 	         
       	            //txApiPkt.payload[0] = ((Radio_Struct.Sent / Radio_Struct.Chunk)<<4) + (byte)LeftToSend;       // sequence number (0-15) / Len (1-13)
#if ENABLE_SESSION_START_END_RETRY           	            
       	            if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))                  // Added by Jason Chen, 2015.09.24
#endif       	            
       	            {       	              
       	              Radio_Struct.Upload.impactCount = impact_count;                                                 // Keep previous Count for resending, 09.11       	            
       	              impact_count++;
       	              txApiPkt.payload[12] = (byte)((impact_count >> 8)&0xff);
       	              txApiPkt.payload[13] = (byte)((impact_count >> 0)&0xff);
       	            } 
       	            else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)
       	                  ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))      // Added by Jason Chen, 2017.04.03      
       	            {
       	            
       	              Radio_Struct.Upload.SessionCount = Session_count;                                          // Keep previous Count for resending, 2017.04.03
       	              Session_count++;                                                  //2017.04.03
       	              txApiPkt.payload[12] = (byte)((Session_count >> 8)&0xff);          //2017.04.03
       	              txApiPkt.payload[13] = (byte)((Session_count >> 0)&0xff);          //2017.04.03       	              
       	            }
       	         }
       	        

          	    // set the length & type and register the packrt
              	txApiPkt.length = 1 + Radio_Struct.Chunk;       // packet length is determined by Chunk
              	if( (Radio_Struct.Type == MESSAGE_TYPE_SHELF_MODE_U) && (txApiPkt.payload[1] == 1) )    // U1 message (this takes care of a newly programmed hub to send Unbind)
          	        txApiPkt.type = CYFISNP_NODE_API_TYPE_CONF_BCDR;
          	    else                                                                                    // all others 	    
          	        txApiPkt.type = CYFISNP_NODE_API_TYPE_CONF;          // _BCDR
          	      	    
              	(void)CYFISNP_NODE_TxDataPut(&txApiPkt);              	                               	
              	              	
                 // update how much is sent
                 Radio_Struct.Sent += LeftToSend;
                 
                 Radio_Stack_TimeSet(&RecvWindowTimer, 8000/CYFISNP_NODE_TIMER_UNITS);      // Added by Jason Chen, 2017.09.27, leave the 2500ms window for receiving  
                 if(Radio_Struct.Sent >= Radio_Struct.ToSend) 
                 {                  
                  //Radio_Stack_TimeSet(&ack_timer, 350);                                   // Added by Jason Chen, 2014.09.11
                  Radio_Stack_TimeSet(&ack_timer, 2500/CYFISNP_NODE_TIMER_UNITS);           // Changed by Jason Chen, 2016.03.28                    
                 }
        	  }
        }
    } 
    else 
    {
        LeftToSend = Radio_Struct.ToSend - Radio_Struct.Sent;
        // can't send more than one Chunk of data
        if( LeftToSend > (word)Radio_Struct.Chunk )
            LeftToSend = (word)Radio_Struct.Chunk;

        // last packet sent? ready to send more?
        if( (LeftToSend != 0) && (CYFISNP_NODE_eProtState == CYFISNP_NODE_DATA_MODE)   // in Data Mode
    		  		          && (CYFISNP_NODE_TxDataPend() == FALSE) )                      // no Tx pending
    		{   			             		  		  		  		        
      	    // empty Tx packet in case (LeftToSend < Radio_Struct.Chunk), which can happen on the last packet
      	    if( LeftToSend < (word)Radio_Struct.Chunk )
      	        (void)memset(txApiPkt.payload, 0x00, CYFISNP_NODE_FCD_PAYLOAD_MAX);
      	                                     //0xFF
      	    // any pFirst ahead of pSrc?
      	    if( Radio_Struct.pFirst != (byte*)0 ) 
      	    {
          	    // first byte is sequence number + number of valid bytes in the packet
      	        txApiPkt.payload[0] = 0x00; // low nibble is zero which is impossible for any other packet
      	        // copy next chunk of data
      	        (void)memcpy(&txApiPkt.payload[1], Radio_Struct.pFirst, Radio_Struct.Chunk);
      	    } 
      	    else 
      	    {
          	    // first byte is sequence number + number of valid bytes in the packet (important for the last packet)
      	        txApiPkt.payload[0] = ((Radio_Struct.Sent / Radio_Struct.Chunk)<<4) + (byte)LeftToSend;       // sequence number (0-15) / Len (1-13)
      	        // copy next chunk of data
      	        (void)memcpy(&txApiPkt.payload[1], &Radio_Struct.pSrc[Radio_Struct.Sent], LeftToSend);
      	    }

      	    // set the length & type and register the packrt
       	    //if(Radio_Struct.Type == MESSAGE_TYPE_POWER_OFF)    // Added by Jason Chen, 2015.01.05
      	    //   txApiPkt.length = 1 + MESSAGE_TYPE_POWER_OFF;   // Added by Jason Chen, 2015.01.05
      	    //else                                               // Added by Jason Chen, 2015.01.05
          	   txApiPkt.length = 1 + Radio_Struct.Chunk;       // packet length is determined by Chunk
          	if( (Radio_Struct.Type == MESSAGE_TYPE_SHELF_MODE_U) && (txApiPkt.payload[1] == 1) )    // U1 message (this takes care of a newly programmed hub to send Unbind)
      	        txApiPkt.type = CYFISNP_NODE_API_TYPE_CONF_BCDR;
      	    else                                                                                    // all others 	    
      	        txApiPkt.type = CYFISNP_NODE_API_TYPE_CONF;          // _BCDR
      	      	    
          	(void)CYFISNP_NODE_TxDataPut(&txApiPkt);
          	            
      	    // any pFirst ahead of pSrc?
      	    if( Radio_Struct.pFirst != (byte*)0 ) 
      	    {
      	        // yes, cancel
      	        Radio_Struct.pFirst = (byte*)0;
      	    } 
      	    else 
      	    {
                  // update how much is sent
                Radio_Struct.Sent += LeftToSend;
                
      	    }   	      	      	      	    	                 
    	  }    
    }

    // [6]
    // run the state machine
         
    {   //-----------------------------;
        Radio_Stack_Save_and_Prog_Spi();
	      CYFISNP_NODE_Run(); // Poll SNP machine
        Radio_Stack_Restore_Spi();
    }   //-----------------------;

    // [7]
    // check if anything is received

    
#if 1    
if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58)        /*&&upload_flag*/     // 09.11                          2
   ||(Radio_Struct.Type == MESSAGE_TYPE_IMMEDIATE)                                     // 2016.03.14
#if ENABLE_SESSION_START_END_RETRY    
   ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)      // Added by Jason Chen, 2015.09.24
   ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A)      // Added by Jason Chen, 2015.09.24  
#endif
  )
{ 
  	if( CYFISNP_NODE_RxDataPend() ) 
  	{	
  		pRxApiPkt = CYFISNP_NODE_RxDataGet();

      #if ENABLE_BACK_CHANNEL_RECV            
      if(( pRxApiPkt->length == 0x1)&&(pRxApiPkt->type == CYFISNP_NODE_API_TYPE_CONF ))    
      {                                        
           if(pRxApiPkt->payload[0] == 0x56)       // 86
           {                      
              retValue_1 = 0;                      // got ACK                  
              upload_impact_ack = 0x56;
           } 
           else if(pRxApiPkt->payload[0] == 0x15)  // 21
           {
              recv_buffer[0] = pRxApiPkt->payload[0];
              retValue_1 = 1; 
              upload_impact_ack = 0x15;// 21;
           }
           else if(pRxApiPkt->payload[0] == 0x16)      // 22, Power off
           {
              recv_buffer[0] = pRxApiPkt->payload[0];
              retValue_1 = 1; 
              upload_impact_ack = 0x16;                
           }                                 
           else if(pRxApiPkt->payload[0] == 0xD6)  // 214-->means noHead received, 2014.09.11
           {                    

              Radio_Struct.Sent = 0;     // Error, resend the current impact data, 2014.09.11

        	    upload_impact_ack = 0xD6;
        	    retValue_1 = 0;                          // 	      	      	      	    	                 

           }
           else
             retValue_1 = 0;           
      } else 
      #endif
      if(( pRxApiPkt->payload[0] == CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedMsb )&&( pRxApiPkt->payload[1] == CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedLsb ) ) 
      {                
          // must be in WALL mode to Upload/Sync
          if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN ) {
              // is it destined to me?
              if( pRxApiPkt->payload[2] == 0x00 ) {                                       // 0
                  ; // wait
              } else if( pRxApiPkt->payload[2] == CYFISNP_NODE_EEP_NET_REC_ADR->devId )   // 1-55
              { 
                  // yes
                  // state REGISTERED happens after sensing U1 and WAITING happens after sending D1.
                  if( (Radio_Struct.Upload.Flag == UPLOAD_STATE_REGISTERED) || (Radio_Struct.Upload.Flag == UPLOAD_STATE_WAITING) ) {
                          Radio_Struct.Upload.Flag = UPLOAD_STATE_ACKNOELEGED; // Reg -> Ack  or  Wait -> Ack
                          // to prevent racing between the application and my sending D1, set a timer here (D1 has priority)
                          CYFISNP_NODE_TimeSet(&Radio_Struct.AckTimer, ACKNOWLEGE_TIME);  // 250 mSec
                  }
              } else if( pRxApiPkt->payload[2] > CYFISNP_NODE_MAX_NODES ) {               // 64-255, Sync'ing?
                  if( (Radio_Struct.Sync.Flag == SYNC_MODE_IDLE) || (Radio_Struct.Sync.Flag == SYNC_MODE_SYNCED) ) {
                 	    CYFISNP_NODE_OutStr("Snc1... ");
                      CYFISNP_NODE_TimeSet(&Radio_Struct.Sync.Timer, SYNC_WAIT_TIME);     // 15 Sec
                      Radio_Struct.Sync.Flag = SYNC_MODE_TIMER_SET; // node may go into PING mode too
                  }
              }
          }

      } 
      #if ENABLE_BACK_CHANNEL_RECV                    
      else
           retValue_1 = 0;
      #endif      
  		CYFISNP_NODE_RxDataRelease(); // Must release buffer when done
  	} 
    #if ENABLE_BACK_CHANNEL_RECV            	
	  else
	    retValue_1 = 0;
    #endif  	
} 
else   
#endif
{
	if( CYFISNP_NODE_RxDataPend() ) 
	{	
		pRxApiPkt = CYFISNP_NODE_RxDataGet();

#if ENABLE_BACK_CHANNEL_RECV            
        if(( pRxApiPkt->length == 0x1)&&(pRxApiPkt->type == CYFISNP_NODE_API_TYPE_CONF ))    
        {                     
           if( recv_buffer != 0) 
           {            
             recv_buffer[0] = pRxApiPkt->payload[0];
             retValue_1 = 1;
           } 
           else
             retValue_1 = 0;           
        } else
#endif         
        // is it a Beacon from our own Hub?
        if(( pRxApiPkt->payload[0] == CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedMsb )&&( pRxApiPkt->payload[1] == CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedLsb ) ) 
        {
                if(wakeup_poweroff)
                   __asm("BGND"); 
                           // yes
                //Led3_Toggle();
                //LED_RED_On();
          	    CYFISNP_NODE_OutChar('B');
               	CYFISNP_NODE_OutHex(pRxApiPkt->payload[2]);
               	CYFISNP_NODE_OutChar(' ');

                // must be in WALL mode to Upload/Sync
                if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN ) {
                    // is it destined to me?
                    if( pRxApiPkt->payload[2] == 0x00 ) {                                       // 0
                        ; // wait
                    } else if( pRxApiPkt->payload[2] == CYFISNP_NODE_EEP_NET_REC_ADR->devId )   // 1-55
                    { 
                        // yes
                        // state REGISTERED happens after sensing U1 and WAITING happens after sending D1.
                        if( (Radio_Struct.Upload.Flag == UPLOAD_STATE_REGISTERED) || (Radio_Struct.Upload.Flag == UPLOAD_STATE_WAITING) ) {
                                Radio_Struct.Upload.Flag = UPLOAD_STATE_ACKNOELEGED; // Reg -> Ack  or  Wait -> Ack
                                // to prevent racing between the application and my sending D1, set a timer here (D1 has priority)
                                CYFISNP_NODE_TimeSet(&Radio_Struct.AckTimer, ACKNOWLEGE_TIME);  // 250 mSec
                        }
                    } else if( pRxApiPkt->payload[2] > CYFISNP_NODE_MAX_NODES ) {               // 64-255, Sync'ing?
                        if( (Radio_Struct.Sync.Flag == SYNC_MODE_IDLE) || (Radio_Struct.Sync.Flag == SYNC_MODE_SYNCED) ) {
                       	    CYFISNP_NODE_OutStr("Snc1... ");
                            CYFISNP_NODE_TimeSet(&Radio_Struct.Sync.Timer, SYNC_WAIT_TIME);     // 15 Sec
                            Radio_Struct.Sync.Flag = SYNC_MODE_TIMER_SET; // node may go into PING mode too
                        }
                    }
                }
        } 
#if ENABLE_BACK_CHANNEL_RECV                    
        else
           retValue_1 = 0;
#endif
        
        
		CYFISNP_NODE_RxDataRelease(); // Must release buffer when done
	} 
#if ENABLE_BACK_CHANNEL_RECV            	
	else
	   retValue_1 = 0;
#endif

}
    // [8]
    // check if all data has been sent?
if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58)         // 09.11                 3
   ||(Radio_Struct.Type == MESSAGE_TYPE_IMMEDIATE)                                     // 2016.03.14
#if ENABLE_SESSION_START_END_RETRY    
   ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)       // Added by Jason Chen, 2015.09.24
   ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A)       // Added by Jason Chen, 2015.09.24
#endif
  )
{
  
    if( (LeftToSend == 0) && (CYFISNP_NODE_eProtState == CYFISNP_NODE_DATA_MODE)   // in Data Mode
                          && (CYFISNP_NODE_TxDataPend() == FALSE))                 // no Tx pending

    {            
       if(upload_impact_ack)                                                       // current impact successful, 09.11
       {    
          mTimer_TimeSet( &imSleepTimer );      // Added by Jason Chen for clearing Sleep timer, 09.11                            
          if(upload_impact_ack == 0x56)         // 86
          {            
            Radio_Struct.State = STATE_IDLE;
            Radio_Struct.Upload.SeqNo = 0;      // Clear after success, 09.11
            Radio_Struct.Type         = 0x00;                                      // Added by Jason Chen to clear TX Buffer, 2016.03.14
          } 
          else if(upload_impact_ack == 0x15)    // 21
          {
            recv_buffer[0] = pRxApiPkt->payload[0];      //Reboot, 09.11
            retValue_1 = 1;   
            impact_count = 0;
            Session_count= 0;                               //2017.04.03
            Radio_Struct.Type         = 0x00;                                      // Added by Jason Chen to clear TX Buffer, 2016.03.14
          }
          else if(upload_impact_ack == 0x16)    // 22
          {
            recv_buffer[0] = pRxApiPkt->payload[0];                                //Power Off, 2016.03.28
            retValue_1 = 1;   
            impact_count = 0;
            Session_count= 0;                               //2017.04.03
            Radio_Struct.Type         = 0x00;                                      // Added by Jason Chen to clear TX Buffer, 2016.03.14
          }                             
          else if(upload_impact_ack == 0xD6)    // 214
          {
            
            Radio_Struct.Sent = 0;              // Error, resend the current impact data, 2014.09.11
            //impact_count--;
#if ENABLE_SESSION_START_END_RETRY           	            
       	    if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))                  // Added by Jason Chen, 2015.09.24
#endif       	                        
                  impact_count = Radio_Struct.Upload.impactCount;
            else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)       // Added by Jason Chen, 2017.04.03
                  ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))      // Added by Jason Chen, 2017.04.03
            {
                  Session_count = Radio_Struct.Upload.SessionCount;                    
            }

       	    retValue_1 = 0;    
       	    
            Radio_Struct.Upload.SeqNo +=1;
            if(Radio_Struct.Upload.SeqNo > 9)         // Reboot after resending 20 times, 09.11, 44  --> 9
            {
            #if 0
                recv_buffer[0] = 21;                          // Reboot
                retValue_1 = 1;   
                impact_count = Radio_Struct.Upload.impactCount = 0;
                Radio_Struct.Upload.SeqNo  = 0;
            #else   
#if ENABLE_SESSION_START_END_RETRY           	            
       	        if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))           // Added by Jason Chen, 2017.04.04
#endif       	                        
                    impact_count++;                                                                        // Added by Jason Chen, 2017.04.04              
                else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)       // Added by Jason Chen, 2017.04.04
                      ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))      // Added by Jason Chen, 2017.04.04
                    Session_count++;                                                                        // Added by Jason Chen, 2017.04.04                          
                Radio_Struct.State = STATE_IDLE;
                Radio_Struct.Upload.SeqNo = 0;          // Clear after success, 09.11
                Radio_Struct.Type         = 0x00;       // Added by Jason Chen to clear TX Buffer, 2016.03.14
                Radio_Struct.pFirst = (byte*)0;         // Dropped it after resending 44 times,    2016.03.14
                Radio_Struct.ToSend = 0;
                Radio_Struct.Sent   = 0;                    
            #endif
            
            }       	                       // 	      	      	      	    	                           
          }
          else 
          {
             if(Radio_Stack_TimeExpired(&ack_timer))                    
             {
                Radio_Struct.Sent = 0;                // Don't get ack, resend the current impact data, 2014.09.11
                //impact_count--;
#if ENABLE_SESSION_START_END_RETRY           	            
       	        if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))                  // Added by Jason Chen, 2015.09.24
             #endif       	                            
                      impact_count = Radio_Struct.Upload.impactCount;
                else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)
                      ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))             // Added by Jason Chen, 2017.04.03
                {
                     Session_count = Radio_Struct.Upload.SessionCount;                                            // Added by Jason Chen, 2017.04.03
                }
                    
                //retValue_1 = 0;                       // 	      	      	      	    	                                                      
                
            	  Radio_Struct.Upload.SeqNo +=1;
            	  if(Radio_Struct.Upload.SeqNo > 44)        // Reboot after resending 20 times, 09.11
            	  {
                   recv_buffer[0] = 21;                          // Reboot
                   retValue_1 = 1;   
                 //impact_count = Radio_Struct.Upload.impactCount = 0;
                   Radio_Struct.Upload.SeqNo  = 0;
                #if ENABLE_SESSION_START_END_RETRY           	            
       	           if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))           // Added by Jason Chen, 2017.04.04
                #endif       	                        
                      impact_count++;                                                                         // Added by Jason Chen, 2017.04.04              
                   else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)       // Added by Jason Chen, 2017.04.04
                        ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))       // Added by Jason Chen, 2017.04.04
                      Session_count++;                                                                        // Added by Jason Chen, 2017.04.04                                                 
            	  }                
             }                    
          }
                                                                    
          #if ENABLE_RECV_WINDOW_TIMER
              if((upload_impact_ack != 0xD6)&&(Radio_Stack_TimeExpired(&RecvWindowTimer)))                    // 09.11
              {    
                if(( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL )&&(!KBI_USB_ON))   // Don't need stop radio when usb connected
                {
                    // stop the radio, until next time
                	  //-----------------------------;
                    Radio_Stack_Save_and_Prog_Spi();              
                    CYFISNP_NODE_Stop();                     // GFT can't receive any packet when LP goes to Sleep mode
                    //CYFISNP_NODE_spiSleepforMCUSleep();              
                    Radio_Stack_Restore_Spi();
                	  //-----------------------;
                }            
              }
              #endif          
          upload_impact_ack = 0;
          
       } 
       else 
       {
           if(Radio_Stack_TimeExpired(&ack_timer)) 
           {
               mTimer_TimeSet( &imSleepTimer );      // Added by Jason Chen for clearing Sleep timer, 09.11
               Radio_Struct.Sent = 0;                // Don't get ack, resend the current impact data, 2014.09.11
            	 upload_impact_ack = 0;                       	 
            	 //impact_count--;                     // Removed by Jason Chen, 2015.09.28   
#if ENABLE_SESSION_START_END_RETRY           	            
       	       if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))                  // Added by Jason Chen, 2015.09.24
#endif       	                        	 
            	     impact_count = Radio_Struct.Upload.impactCount;
               else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)
                     ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))             // Added by Jason Chen, 2017.04.04
               {
                   Session_count = Radio_Struct.Upload.SessionCount;                                               // Added by Jason Chen, 2017.04.04
               }
            	 
            	 Radio_Struct.Upload.SeqNo +=1;
            	 if(Radio_Struct.Upload.SeqNo > 5)        // Reboot after resending 20 times, 09.11,     44 ---> 9
            	 {
            	 #if 0
                  recv_buffer[0] = 21;                   // Reboot
                  retValue_1 = 1;   
                  impact_count = Radio_Struct.Upload.impactCount = 0;
                  Radio_Struct.Upload.SeqNo  = 0;
               #else
               #if ENABLE_SESSION_START_END_RETRY           	            
         	        if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x58))           // Added by Jason Chen, 2017.04.04
               #endif       	                        
                    impact_count++;                                                                          // Added by Jason Chen, 2017.04.04              
                 else if((Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x49)        // Added by Jason Chen, 2017.04.04
                       ||(Radio_Struct.Type == MESSAGE_TYPE_HIT_DATA)&&(Radio_Struct.pSrc[0] == 0x4A))       // Added by Jason Chen, 2017.04.04
                    Session_count++;                                                                          // Added by Jason Chen, 2017.04.04                             
                  Radio_Struct.State = STATE_IDLE;
                  Radio_Struct.Upload.SeqNo = 0;          // Clear after success, 09.11
                  Radio_Struct.Type         = 0x00;       // Added by Jason Chen to clear TX Buffer, 2016.03.14
                  Radio_Struct.pFirst = (byte*)0;         // Dropped it after resending 44 times,    2016.03.14
                  Radio_Struct.ToSend = 0;
                  Radio_Struct.Sent   = 0;               
               #endif
            	 }
            	 
           }        
       }
    }
} 
else 
{
    if( (LeftToSend == 0) && (CYFISNP_NODE_eProtState == CYFISNP_NODE_DATA_MODE)   // in Data Mode
                          && (CYFISNP_NODE_TxDataPend() == FALSE))                  // no Tx pending                          
    {                                                                                
        
    #if ENABLE_RECV_WINDOW_TIMER
        if(Radio_Stack_TimeExpired(&RecvWindowTimer)) 
        {    
          if(( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL )&&(!KBI_USB_ON))   // Don't need stop radio when usb connected
          {
              // stop the radio, until next time
          	  //-----------------------------;
              Radio_Stack_Save_and_Prog_Spi();              
              CYFISNP_NODE_Stop();                     // GFT can't receive any packet when LP goes to Sleep mode
            //CYFISNP_NODE_spiSleepforMCUSleep();              
              Radio_Stack_Restore_Spi();
          	  //-----------------------;
          }            
        }
    #endif
        Radio_Struct.State = STATE_IDLE;        
    }
  
}
    
#if ENABLE_BACK_CHANNEL_RECV                
    return retValue_1;
#endif    
}

// can be called by the App to upload next message (UPLOAD_MORE, UPLOAD_LAST)
bool Radio_Stack_Upload_Ready(void)
{
    if( (Radio_Struct.State == STATE_IDLE) &&
            (CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN) &&
                (Radio_Struct.Upload.Flag == UPLOAD_STATE_ACKNOELEGED) &&
                    CYFISNP_NODE_TimeExpired(&Radio_Struct.AckTimer) )      // 250 mSec
            return TRUE;
    else
            return FALSE;
}

// can be called by the App to send next message (POWER_ON, POWER_OFF, IMMEDIATE, HIT)
byte Radio_Stack_State(void)
{
    switch(CYFISNP_NODE_eProtState) 
    {
    		default:
    		case CYFISNP_NODE_STOP_MODE:
    		    return RADIO_STACK_IDLE;        // ready for next message

    		case CYFISNP_NODE_UNBOUND_MODE:
    		    return RADIO_STACK_NOT_BOUND;

    		case CYFISNP_NODE_PING_MODE:
    		case CYFISNP_NODE_PING_MODE_TIMEOUT:
    		    return RADIO_STACK_PINGING;

    		case CYFISNP_NODE_CON_MODE:
    		case CYFISNP_NODE_CON_MODE_TIMEOUT:
    			  return RADIO_STACK_CONNECTING;

    		case CYFISNP_NODE_BIND_MODE:
    		    return RADIO_STACK_BINDING;

    		case CYFISNP_NODE_DATA_MODE:
            if( Radio_Struct.State == STATE_IDLE )  // good if in WALL mode
        		    return RADIO_STACK_IDLE;    // ready for next message
    			  return RADIO_STACK_DATA_MODE;
    }
}

// can be called by the App to know whether we are bound to the Hub or not
bool Radio_Stack_isBound(void)
{
    if(CYFISNP_NODE_ForceBindingMode() == 1)                                               // Added by Jason Chen, 2019.01.25
      return(FALSE);                                                                       // Added by Jason Chen, 2019.01.25
    else                                                                                   // Added by Jason Chen, 2019.01.25
      return (netParamsPresent());
}

// can be called by the App to bind with the Hub, if we are not already bound
byte Radio_Stack_Bind(void)
{
    // this check is done in Register function, but I need to to it again ahead of Idling the State, below.
    if(CYFISNP_NODE_ForceBindingMode() == 0)                                               // Added by Jason Chen, 2019.01.25
    if( netParamsPresent() )
        return Radio_Stack_Register_Error(RADIO_STACK_REGISTER_MESSAGE_ERR_ALREADY_BOUND);
    
    Radio_Struct.State = STATE_IDLE;    // in case we received Unbind while registering U1
    return Radio_Stack_Register_Message((byte*)0, 0, MESSAGE_TYPE_BINDING);
}

// can be called by the App to unbind from the Hub, good for when we want to bind to a new Hub
bool Radio_Stack_UnBind(void)
{
  if (!KBI_USB_ON)                                       // Added by Jason Chen, 2014.0619
  {    
    // only when the stack is stopped
    if( (CYFISNP_NODE_eProtState != 0) && (CYFISNP_NODE_eProtState != CYFISNP_NODE_STOP_MODE) )
        return FALSE;
  
    // and already unbound
    if( !Radio_Stack_isBound() )
        return FALSE;
    
    CYFISNP_NODE_Unbind();  // now we must re-bind again with a new Hub
  } 
  else 
  {
      // and already unbound
    if( !Radio_Stack_isBound() )
        return FALSE;
    CYFISNP_NODE_Unbind();  // Added by Jason Chen, 2014.0619
  }
  
  return TRUE;
}

//
static void showSignon(void)
{
	CYFISNP_NODE_OutStr("\r\n\n-------------------------------------------\r\n");
	CYFISNP_NODE_OutStr("CYFISNP_NODE: ");

	CYFISNP_NODE_OutStr(" on "); CYFISNP_NODE_OutStr(__DATE__);
	CYFISNP_NODE_OutStr(", ");   CYFISNP_NODE_OutStr(__TIME__);

	CYFISNP_NODE_OutStr("\r\nExtPA=YES");

    if( CYFISNP_NODE_Pwr_Type == CYFISNP_NODE_PWR_WALL ) {
	    CYFISNP_NODE_OutStr(", PwrSrc=WALL\r\n");
    } else {
	    CYFISNP_NODE_OutStr(", PwrSrc=COIN\r\n");
    }
}

//
static void showNetRecord(void)
{
	CYFISNP_NODE_OutStr("\r\nSopIdx:"); CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->sopIdx);
	CYFISNP_NODE_OutStr(", chBase:");   CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->chBase);
	CYFISNP_NODE_OutStr(", chHop:");    CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->chHop);
	CYFISNP_NODE_OutStr(", hubSeed:");  CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedMsb);
										CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->hubSeedLsb);
	CYFISNP_NODE_OutStr(", devId:");    CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->devId);
	CYFISNP_NODE_OutStr(", nodeSeed:"); CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->nodeSeedMsb);
										CYFISNP_NODE_OutHex(CYFISNP_NODE_EEP_NET_REC_ADR->nodeSeedLsb);
	CYFISNP_NODE_OutStr("\r\n");
}

// can be called by the App to display some info from the Stack
void Radio_Stack_Info(void)
{
    // Show compiler, date, time
	showSignon();

	// display network parameters
	showNetRecord(); 	// Show SopIdx, crcSeed, etc used by Node
}

// can be called by the App to get the network node number
byte Radio_Stack_Node_Number(void)
{
    return CYFISNP_NODE_EEP_NET_REC_ADR->devId;
}

// can be called by the App to set a time
void Radio_Stack_TimeSet(word *pTimer, word time)
{
    CYFISNP_NODE_TimeSet(pTimer, time);
}

// can be called by the App to check a timer
bool Radio_Stack_TimeExpired(word *pTimer)
{
    return CYFISNP_NODE_TimeExpired(pTimer);
}

// can be called by the App to abort all radio/stack and turn the radio off
void Radio_Stack_Abort(void)
{
    {   //-----------------------------;
        Radio_Stack_Save_and_Prog_Spi();
        CYFISNP_NODE_Stop();
        Radio_Stack_Restore_Spi();
    }   //-----------------------;

  #if DISABLE_STACK_POWERON_INIT
  //Radio_Stack_PowerOn_Init();                                                               // Removed by Jason Chen, 2015.09.01
  #else
    Radio_Stack_PowerOn_Init();
  #endif
    //RADIO_PWR_OFF();
}

// can be called by the App to tell the stack to ignore Usb attachment
bool Radio_Stack_Usb_Ignore(void)
{
    if( Stack_Usb.Flag != USB_MODE_TIMER_SET )  // can be called during 7.5 Sec wait period
        return FALSE;

    Stack_Usb.IgnoreU1 = TRUE;    // will prevent U1 message
    return TRUE;
}

// can be called by the App to tell the stack to resume detecting Usb attachment
bool Radio_Stack_Usb_Resume(void)
{
    if( Stack_Usb.IgnoreU1 == FALSE )
        return FALSE;           // invalid situation

    Stack_Usb.IgnoreU1 = FALSE;
    return TRUE;
}

// can be called by the App to know whether Sync'ing has been requested or not
bool Radio_Stack_Sync(void)
{
    if( Radio_Struct.Sync.Flag == SYNC_MODE_SYNCED ) {
        Radio_Struct.Sync.Flag = SYNC_MODE_IDLE;
        outstr("Snc4... ");
        return TRUE;
    } else
        return FALSE;
}


extern void setHubSeed(void);
//
bool Radio_Stack_Get_Temperature(void)
{
    byte radioState, rxLength, rxCfg;

    // only in Coin mode and Radio Stack idle
    if( TempSense.Abort )
        TempSense.Flag = TEMP_SENSOR_END;   // this leaves Temp, Volt and Rssi at zero
    //else if( CYFISNP_NODE_Pwr_Type != CYFISNP_NODE_PWR_COIN )      // removed by Jason Chen, 2015.04.30
    //    return FALSE;
    else if( Radio_Struct.State != STATE_IDLE )
        return FALSE;
    
    switch(TempSense.Flag) {
        case TEMP_SENSOR_IDLE:  // doesn't matter if we are in Usb timeout or not, when U1 is being registered, this one will cancel
            RADIO_PWR_ON();
            // start the radio
           	{   //-----------------------------;
                Radio_Stack_Save_and_Prog_Spi();
                if( CYFISNP_NODE_Start() == FALSE ) {
                    Radio_Stack_Restore_Spi();
                    RADIO_PWR_OFF();
					
                    return FALSE;
                }
                Radio_Stack_Restore_Spi();
   	        }   //-----------------------;

   	        TempSense.Temp = 0;
   	        TempSense.Volt = 0;
   	        TempSense.Rssi = 0;
   	        TempSense.Abort = 0;
   	        TempSense.Flag = TEMP_SENSOR_START;             // goto next state
   	        return TRUE;
        
        case TEMP_SENSOR_START:
            // set Channel/PnCode/CrcSeed
           	{   //-----------------------------;
                Radio_Stack_Save_and_Prog_Spi();
                CYFISNP_NODE_SetChannel(88);
                CYFISNP_NODE_SetSopPnCode(8);
                CYFISNP_NODE_SetCrcSeed(0xC0DE);
                // disable LNA
                rxCfg = CYFISNP_NODE_Read(CYFISNP_NODE_RX_CFG_ADR);
                rxCfg &= ~CYFISNP_NODE_LNA_EN;
                CYFISNP_NODE_Write(CYFISNP_NODE_RX_CFG_ADR, rxCfg);

                // start the receiver
                CYFISNP_NODE_SetPtr      (&TempSense.Buf[0]);
                CYFISNP_NODE_SetLength   (12);
                CYFISNP_NODE_StartReceive();
                Radio_Stack_Restore_Spi();
   	        }   //-----------------------;
   	        TempSense.Flag = TEMP_SENSOR_GET_DATA;          // goto next state
   	        return TRUE;
        
        case TEMP_SENSOR_GET_DATA:
           	{   //-----------------------------;
                Radio_Stack_Save_and_Prog_Spi();
                radioState = CYFISNP_NODE_GetReceiveState();

                // ---------------------------------------------------------------
                // Monitor for Rx Packet Error
                // ---------------------------------------------------------------
                if (radioState & CYFISNP_NODE_ERROR) {
                    (void)CYFISNP_NODE_EndReceive();
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                
                    CYFISNP_NODE_StartReceive();            // stay at current state
                // ---------------------------------------------------------------
                // Monitor for Rx Packet Success
                // ---------------------------------------------------------------
                } else if (radioState & CYFISNP_NODE_COMPLETE) {
                    rxLength = CYFISNP_NODE_EndReceive();
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
    
                    if( (rxLength == 12) && (TempSense.Buf[0] == 0x55) && (TempSense.Buf[1] == 0xAA) ) {
                        if( memcmp(&TempSense.Mid[0], &TempSense.Buf[6], 6) == 0 ) {
                            if( ++TempSense.Cnt >= TEMP_SENSE_CONSEC_CNT )
                                TempSense.Cnt = TEMP_SENSE_CONSEC_CNT;  // locked to this sensor
                            TempSense.Temp = TempSense.Buf[2]*256 + TempSense.Buf[3];
                            TempSense.Volt = TempSense.Buf[4]*256 + TempSense.Buf[5];
                            TempSense.Rssi = CYFISNP_NODE_GetRssi() & CYFISNP_NODE_RSSI_LVL_MSK;
               	            TempSense.Flag = TEMP_SENSOR_END;       // goto next state
               	            Led4_On();
                        } else {
                            if( TempSense.Cnt != TEMP_SENSE_CONSEC_CNT ) {  // already locked to a Sensor?
                                // no, learn it
                                TempSense.Cnt = 1;  // first time
                                (void)memcpy(&TempSense.Mid[0], &TempSense.Buf[6], 6);
                                outstr("Lrn ");
                            } // else ignore
                            CYFISNP_NODE_StartReceive();    // stay at current state
                        }
                    } else
                        CYFISNP_NODE_StartReceive();        // stay at current state
                }
                Radio_Stack_Restore_Spi();
   	        }   //-----------------------;
            return TRUE;
        
        case TEMP_SENSOR_END:
            // restore Channel/PnCode/CrcSeed
           	{   //-----------------------------;
                Radio_Stack_Save_and_Prog_Spi();
                setHubSeed();
                CYFISNP_NODE_SetSopPnCode(CYFISNP_NODE_EEP_NET_REC_ADR->sopIdx);
                CYFISNP_NODE_SetChannel(CYFISNP_NODE_bCurrentChannel);
                // enable LNA
                rxCfg = CYFISNP_NODE_Read(CYFISNP_NODE_RX_CFG_ADR);
                rxCfg |= CYFISNP_NODE_LNA_EN;
                CYFISNP_NODE_Write(CYFISNP_NODE_RX_CFG_ADR, rxCfg);
                //Radio_Stack_Restore_Spi();
   	        }   //-----------------------;
            
            {   //-----------------------------;
                //Radio_Stack_Save_and_Prog_Spi();
                CYFISNP_NODE_Stop();
                Radio_Stack_Restore_Spi();
            }   //-----------------------;
            RADIO_PWR_OFF();
			
   	        
            if( TempSense.Abort )
                TempSense.Flag = TEMP_SENSOR_IDLE;              // ready to start over
            else
                TempSense.Flag = TEMP_SENSOR_READ_DATA;         // goto next state
            return TRUE;
        
        case TEMP_SENSOR_READ_DATA:
            return TRUE;

        default:
            return FALSE;
    }
}

//
bool Radio_Stack_Read_Temperature(word *pTemp, word *pVolt, byte *pRssi)
{
    if( (pTemp==(byte*)0) || (pVolt==(byte*)0) || (pRssi==(byte*)0) )
        return FALSE;
    
    if( (TempSense.Flag == TEMP_SENSOR_READ_DATA) || TempSense.Abort ) {
        *pTemp = TempSense.Temp;
        *pVolt = TempSense.Volt;
        *pRssi = TempSense.Rssi;
        TempSense.Abort = 0;
        TempSense.Flag = TEMP_SENSOR_IDLE;
        Led4_Off();
        return TRUE;
    }
    
    return FALSE;
}

#if 1   // moved from hw.c
//
#define CMD_BPROG   0x20
#define CMD_ERASE   0x40
#define ERR_SUCCESS 0
#define ERR_FLASH   1
#define ERR_VERIFY  2
#define BINDING_ADR (byte*)0x1200   // Binding data is stored at here
typedef byte(*pFct) (byte,byte*,byte);
#if FIX_FLASHWRITE_BUG
   extern byte FIMG_ADDR[64];
#else   
   #define FIMG_ADDR   0x100 /* see prm file */                                    // Removed by Jason Chen, 2015.09.08
#endif
   
#define FIMG_SIZE   64    /* 0x3f=63 */

static  byte flash(byte cmd, byte *adr, byte dat);

byte myFlashWrite(byte *src, byte len)
{
    byte *dst, i;
    pFct pflash;

    // check if already the same
    if( memcmp(BINDING_ADR, src, len)==0 )
        return ERR_SUCCESS;
    
    #pragma MESSAGE DISABLE C1805

    // copy the function to RAM
    pflash = (pFct)FIMG_ADDR;
    (void)memcpy((void*)pflash, (void*)flash, FIMG_SIZE);

    // Step 1: Erase page block 9 (512 bytes)
    if( (*pflash)(CMD_ERASE, BINDING_ADR, 0) )
        return ERR_FLASH;

    // Step 2: Write the data
    dst = BINDING_ADR;
    for(i=0; i<len; i++,dst++)
        if( (*pflash)(CMD_BPROG, dst, src[i]) )
            return ERR_FLASH;

    // Step 3: Verify
    if( memcmp(BINDING_ADR, src, len) )
        return ERR_VERIFY;

    return ERR_SUCCESS;
}

//
static byte flash(byte cmd, byte *adr, byte dat) /* 0x3f=63 */
{
    DisableInterrupts;
    while( FSTAT_FCBEF==0 )
        ;   // ensure the address, command and data buffers are empty
    // clear any errors
    FSTAT = FSTAT_FACCERR_MASK | FSTAT_FPVIOL_MASK;
    *adr = dat;         // buffer the address and data
    FCMD = cmd;         // issue the command
    FSTAT_FCBEF = 1;    // reset the flag
    // check whether the command sequence was ok
    if( FSTAT_FACCERR || FSTAT_FPVIOL ) {
        EnableInterrupts;
        return ERR_FLASH;
    }
    while( FSTAT_FCCF==0 )
        ;   // wait until CCF is cleared and CBEF is set
    EnableInterrupts;
    return 0;
}
#endif  // 0/1

byte Radio_SPI_test(void)
{
   byte result =1;
   RADIO_PWR_ON();
   LpTest.status = 2; //set to failed first
   LpTest.BufRx[0] = 0;
   LpTest.BufRx[1] = 0;
   
   Radio_Stack_Save_and_Prog_Spi();
   CYFISNP_NODE_WriteRaw(0x0,0x55);
   if (CYFISNP_NODE_ReadRaw(0) == 0x55)
   {
   	 result = 0;
   }
   Radio_Stack_Restore_Spi();
   RADIO_PWR_OFF();
    
	PTED_PTED5 = 0;			   //MOSI1 setup as 0	
   
   return result;

}
static byte radioStateCopy;


byte Radio_Stack_Loop_Test(void)
{
    byte radioState, rxLength;
	byte fiStat =0;
	
    switch(LpTest.Flag) 
	{
        case LP_TEST_IDLE: 
			
			LpTest.Flag = LP_TEST_START;
            fiStat = 0;
            break;
        case LP_TEST_START:
			 
			Radio_Stack_PowerOn_Init();
			RADIO_PWR_ON();
			Radio_Stack_Save_and_Prog_Spi();
			(void)CYFISNP_NODE_Radio_Start();
			CYFISNP_NODE_SetFrameConfig(0xee);
		    CYFISNP_NODE_SetTxConfig(0xf);
   	        LpTest.Rssi = 0;
   	        LpTest.Abort = 0;
			LpTest.Flag = LP_TEST_SEND_DATA;
            CYFISNP_NODE_SetChannel(46);
            CYFISNP_NODE_SetSopPnCode(6);
			fiStat = 0;
			Radio_Stack_Restore_Spi();
			
            break;
	    case LP_TEST_SEND_DATA:		
               
			    Radio_Stack_Save_and_Prog_Spi();
				CYFISNP_NODE_SetPtr(&LpTest.BufTx[0]);
				CYFISNP_NODE_StartTransmit(0, 2);
				
				while ((CYFISNP_NODE_State & CYFISNP_NODE_COMPLETE) == 0) 
				 {
                 	(void)CYFISNP_NODE_GetTransmitState();
					
                 }
				 radioStateCopy = CYFISNP_NODE_State;
    			 CYFISNP_NODE_EndTransmit();
				 
		       // -------------------------------------------------------------------
		       // If AutoAck failed, power-down radio
		       // -------------------------------------------------------------------
		       if ((radioStateCopy & CYFISNP_NODE_ERROR) != 0) 
			   {
		           CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
		           //dynPaUpdate(0);               // we are changing channels here, what is this for then?    // -- //
		           fiStat = 2;
		       }
		       // -------------------------------------------------------------------
		       // Else if AutoAck PASSED, then open ConRsp receive window
		       //  and spend a little more time on this channel
		       // -------------------------------------------------------------------
		       else 
		       {
                 // start the receiver
                 CYFISNP_NODE_SetPtr(&LpTest.BufRx[0]);
                 CYFISNP_NODE_SetLength(6);
                 CYFISNP_NODE_StartReceive();
				 fiStat = 0;
               
                 LpTest.Flag = LP_TEST_GET_DATA;          // goto next state  	     
   	           }           
   	           Radio_Stack_Restore_Spi();
            break;
        
        case LP_TEST_GET_DATA:
			    
           	    //-----------------------------;
                Radio_Stack_Save_and_Prog_Spi();
                radioState = CYFISNP_NODE_GetReceiveState();

                // ---------------------------------------------------------------
                // Monitor for Rx Packet Error
                // ---------------------------------------------------------------
                if (radioState & CYFISNP_NODE_ERROR) 
				{

                    (void)CYFISNP_NODE_EndReceive();
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                    
                    CYFISNP_NODE_StartReceive();            // stay at current state
                    fiStat = 2;
                // ---------------------------------------------------------------
                // Monitor for Rx Packet Success
                // ---------------------------------------------------------------
                } 
				else if (radioState & CYFISNP_NODE_COMPLETE) 
			    {
                    rxLength = CYFISNP_NODE_EndReceive();
                    CYFISNP_NODE_ForceState(CYFISNP_NODE_END_STATE_SLEEP);
                        
                    LpTest.Rssi = CYFISNP_NODE_GetRssi() & CYFISNP_NODE_RSSI_LVL_MSK;
               	    LpTest.Flag = LP_TEST_END;       // goto next state
               	    
                    fiStat = 1;
                }
				else
					fiStat = 0;
               Radio_Stack_Restore_Spi();
   	                 
            break;
        
        case LP_TEST_END:
            // restore Channel/PnCode/CrcSeed
           	   //-----------------------------;
                Radio_Stack_Save_and_Prog_Spi();
                setHubSeed();
                CYFISNP_NODE_SetSopPnCode(CYFISNP_NODE_EEP_NET_REC_ADR->sopIdx);
                CYFISNP_NODE_SetChannel(CYFISNP_NODE_bCurrentChannel);
               
                
                CYFISNP_NODE_Stop();
                Radio_Stack_Restore_Spi();
               //-----------------------;
           
            LpTest.Flag = LP_TEST_IDLE;  //game over           
            fiStat = 1;
            break;
        default:
            fiStat = 1;
            break;
    }
	LpTest.status = fiStat;
	return fiStat;
}

//
byte Radio_Stack_Read_Lp_Test(byte *data1, byte *data2, byte *pRssi)
{
     
    
        *data1 = LpTest.BufRx[0];
        *data2 = LpTest.BufRx[1];
        *pRssi = LpTest.Rssi;
        LpTest.Abort = 0;
        return LpTest.status;
       
}
bool Radio_Stack_Write_Lp_Test(byte data1, byte data2)
{
    
    
    if( !LpTest.Abort ) 
	{
        LpTest.BufTx[0] = data1;
        LpTest.BufTx[1] = data2;
        LpTest.Abort = 0;
        LpTest.Flag = LP_TEST_IDLE;
        
        return TRUE;
    }
	else
		return FALSE;
    
}

void gRadioSleep(void)                               // Added by Jason chen, 2014.04.10
{               			   
     Radio_Stack_Save_and_Prog_Spi();
     CYFISNP_NODE_Stop();
     CYFISNP_NODE_spiSleepforMCUSleep();             // Added by Jason chen, 2014.04.16
     Radio_Stack_Restore_Spi();     
	 
    USB_MODE_to_IDLE();   
          
    Cpu_Delay100US(100);     
               
}

void gRadioWakup(void) 
{
  PTED_PTED0  = 1;        // Radio CS, output = 1
  PTED_PTED6  = 1;        // SPI1 CLK, output = 1
  PTED_PTED5  = 1;        // SPI1 MOSI,output = 1
  
  
  PTEDD_PTEDD4 = 0;       // SPI1 MISO,Data Direction --> input          
  PTCDD_PTCDD3 = 0;       // IRQ_RF  setup as input ,    Added Jason Chen, 2014.04.14
  #ifdef  ENABLE_6DS3
	   PTBDD_PTBDD5 = 0; 
#endif
  PTFD_PTFD0 = 1;			    // RF Power Control output = 1;
                         
  (void)SPI_FLASH_Init(1);        
  
  Radio_Stack_Save_and_Prog_Spi();
  CYFISNP_NODE_Stop();
  CYFISNP_NODE_spiSleepforMCUSleep();             // Added by Jason chen, 2014.05.02  
  Radio_Stack_Restore_Spi();     
  USB_MODE_to_IDLE();        
}


void gResetRadio(void)                               // Added by Jason chen, 2014.04.10
{               			   
	   //(void)SPI_FLASH_Init(0);
	   PTFDD_PTFDD0 = 1;             //RF      Power Control setup as output
	   PTEDD_PTEDD0 = 1;             //RF      Chip Select setup as output
     PTEDD_PTEDD6 = 1;             //SCK1    setup as output
     PTEDD_PTEDD5 = 1;             //MOSI1   setup as output
     PTEDD_PTEDD4 = 1;             //MISO1   setup as output
     PTCDD_PTCDD3 = 1;             //IRQ_RF  setup as output               
     #ifdef  ENABLE_6DS3
	   PTBDD_PTBDD5 = 0; //
     #endif
	   PTFD_PTFD0 = 0;			         //RF Power Control output = 0;
	   PTED_PTED0 = 0;               //RF Chip Select setup as 0
     PTED_PTED6 = 0;               //SCK1  setup as 0
     PTED_PTED5 = 0;               //MOSI1 setup as 0	   	   	   	   	   	       
     PTED_PTED4 = 0;               //MISO1 setup as 0          
     PTCD_PTCD3 = 0;               //output = 0;
     
#ifdef  ENABLE_6DS3
			PTBD_PTBD5 = 0; 
#endif
	   Cpu_Delay100US(100);

     PTEDD_PTEDD4 = 0;             //MISO1 setup as input    
     PTCDD_PTCDD3 = 0;             //IRQ_RF  setup as input
     #ifdef  ENABLE_6DS3
	   PTBDD_PTBDD5 = 0; 
	 #endif
	   PTEDD_PTEDD0 = 1;             //RF Chip   Select setup as output
	   PTED_PTED0   = 1;             //RF Chip   Select Control ouput = 1
	   PTFDD_PTFDD0 = 1;             //RF Power Control setup as output
	   PTFD_PTFD0   = 1;			       //RF Power Control output = 1;	   	   	   
}


