/*************************************************************************
 * DISCLAIMER *
 * Services performed by FREESCALE in this matter are performed          *
 * AS IS and without any warranty. CUSTOMER retains the final decision   *
 * relative to the total design and functionality of the end product.    *
 * FREESCALE neither guarantees nor will be held liable by CUSTOMER      *
 * for the success of this project. FREESCALE disclaims all warranties,  *
 * express, implied or statutory including, but not limited to,          *
 * implied warranty of merchantability or fitness for a particular       *
 * purpose on any hardware, software ore advise supplied to the project  *
 * by FREESCALE, and or any product resulting from FREESCALE services.   *
 * In no event shall FREESCALE be liable for incidental or consequential *
 * damages arising out of this agreement. CUSTOMER agrees to hold        *
 * FREESCALE harmless against any and all claims demands or actions      *
 * by anyone on account of any damage, or injury, whether commercial,    *
 * contractual, or tortuous, rising directly or indirectly as a result   *
 * of the advise or assistance supplied CUSTOMER in connection with      *
 * product, services or goods supplied under this Agreement.             *
 *************************************************************************/
/*************************************************************************************************
 * File name   : Hid.h
 *
 * Description : This file is the header file of HID.c
 *               
 *
 * History     :
 * 04/01/2007  : Initial Development
 * 
 *************************************************************************************************/

#ifndef HID_H
#define HID_H
#define SELF_POWER
//#include "mHid.h"
/*HID class requests */
#define GET_REPORT      0x01
#define GET_IDLE        0x02
#define GET_PROTOCOL    0x03
#define SET_REPORT      0x09
#define SET_IDLE        0x0A
#define SET_PROTOCOL    0x0B

/* Class descriptor*/
#define DSC_HID         0x21
#define DSC_RPT         0x22
#define DSC_PHY         0x23

/*HID class protocol type*/
#define BOOT_PROTOCOL   0x00
#define RPT_PROTOCOL    0x01


/*HID interface class code*/
#define HID_INTF                    0x03

/*SubClass interface*/
#define BOOT_INTF_SUBCLASS          0x01

/*HID  class protocol */
#define HID_PROTOCOL_NONE           0x00
#define HID_PROTOCOL_KEYBOAD        0x01
#define HID_PROTOCOL_MOUSE          0x02

/* the endpoint for HID*/
#define HID_INTF_ID             0x00
#define HID_UEP_OUT             EPCTL2
#define HID_BD_OUT              Bdtmap.ep2Bio

#define HID_INT_OUT_EP_SIZE     64
#define HID_INT_IN_EP_SIZE      64

#define HID_UEP_IN              EPCTL1
#define HID_BD_IN               Bdtmap.ep1Bio
 
#define HID_NUM_OF_DSC          1
#define HID_RPT_SIZE            25//52



#define USBGetHIDDscAddr(ptr)                      \
{                                                  \
    if(Usb_Active_Cfg == 1)                        \
        ptr = (byte*)&Cfg_01.Hid_Dsc_Intf00Alt00;  \
}

#define USBGetHIDRptDscAddr(ptr)                   \
{                                                  \
    if(Usb_Active_Cfg == 1)                        \
        ptr = (byte*)&Hid_Rpt;                     \
}

#define USBGetHIDRptDscSize(count)                 \
{                                                  \
    if(Usb_Active_Cfg == 1)                        \
        count = sizeof(Hid_Rpt);                   \
}



/*Structure definition*/
typedef struct _USB_HID_DSC_HEADER
{
    byte bDscType;
    word wDscLength;
} USB_HID_DSC_HEADER;

typedef struct _USB_HID_DSC
{
    byte bLength;
    byte bDscType;
    word bcdHID;
    byte bCountryCode;
    byte bNumDsc;
    USB_HID_DSC_HEADER hid_dsc_header[HID_NUM_OF_DSC];
} USB_HID_DSC;



extern char Hid_Rpt_Rx_Len;
extern char RptBuf[HID_INT_IN_EP_SIZE];

extern void Init_EP_For_HID(void);
extern void HIDClass_Request_Handler(void);

extern void HID_Transfer_Rpt(char *buffer, byte len);
extern byte HID_Receive_Rpt(char *buffer, byte len);
extern void HID_Rec_Data(void);

#endif 
