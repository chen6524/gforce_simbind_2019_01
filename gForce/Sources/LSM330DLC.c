/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West
 * Markham, ON L3R 3J9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http:  www.artaflex.com
 * email: Jason_Chen@artaflex.com
 *
 ***************************************************************************/
#if 0

#include "I2C_driver.h"
#include "LSM330DLC.h"

#ifdef GYROS_SPI_CS
  #include "GYRO_SPI_CS.h" 
#endif
#include "mcu.h"
#include "typedef.h"

#include "bcd_common.h"                          // Added by Jason Chen, 2014.05.07


static char FIFO_Src       = 0;
static char FIFO_Ovr       = 0;

/*******************************************************************************
* Function Name  : SPI_BYTE_IO
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI2 bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
byte SPI2_BYTE_IO(byte dataByte)
{
#if 1
  while( !SPI2S_SPTEF );   // SPI Transmit Buffer Empty Flag
  SPI2DL = dataByte;      // send the data

  // wait while no data available in the receive data buffer
  while( !SPI2S_SPRF );    // SPI Read Buffer Full Flag
  return (byte)SPI2DL;     // return the received data
#else
  byte Chr;
  while(SPIM_Acc_SendChar(data_byte)!=ERR_OK);
  while(SPIM_Acc_GetCharsInRxBuf() == 0);
  while(SPIM_Acc_RecvChar(&Chr)!= ERR_OK);
  return Chr;
#endif
  
}


/*******************************************************************************
* Function Name  : Gyroscope SPI_BYTE/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/
void gyro_SPIBufferRead(byte* pBuffer, byte readAddr, byte numByteToRead)
{ 
  byte i;
  
  readAddr |= 0x80;
  if(numByteToRead > 1) 
      readAddr |= 0x40;           // Auto Incremental register Address
  
  GYRO_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(readAddr);
  for(i = 0;i<numByteToRead;i++) pBuffer[i] = SPI2_BYTE_IO(Dummy_Byte);  
  GYRO_SPI_CS_HIGH();
}

void gyro_FIFO_SPIGetRawData(byte *pBuffer) 
{
  byte data;
  
  gyro_SPIBufferRead(pBuffer, GYRO_OUT_X_L, 6); 
  
  data       = pBuffer[1];
  pBuffer[1] = pBuffer[0];
  pBuffer[0] = data;
  
  data       = pBuffer[3];
  pBuffer[3] = pBuffer[2];
  pBuffer[2] = data;  
  
  data       = pBuffer[5];
  pBuffer[5] = pBuffer[4];
  pBuffer[4] = data;  
}

byte gyro_SPIReadReg(byte regAddr) 
{
  byte readValue;
  
  gyro_SPIBufferRead(&readValue, regAddr, 1);
  
  return readValue; 
    
}


/*******************************************************************************
* Function Name  : Gyroscope SPI_Byte/Buffer Write
* Description    : write a Byte to SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  WriteAddr is the register address you want to write to
*                  pBuffer contains bytes to write
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_SPIWriteReg(byte regAddr, byte regValue)
{    
  GYRO_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(regAddr);
  (void)SPI2_BYTE_IO(regValue);  
  GYRO_SPI_CS_HIGH();
 }


void gyro_SPIBufferWrite(byte regAddr,byte *writeBuf, byte numByteToWrite) 
{
  byte i;
  
  if(numByteToWrite > 1)
    regAddr |= 0x40;
  
  GYRO_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(regAddr);
  for(i = 0;i<numByteToWrite;i++) (void)SPI2_BYTE_IO(writeBuf[i]);  
  GYRO_SPI_CS_HIGH();
}

/*******************************************************************************
* Function Name  : FifoBypass
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_FifoBypass(void) 
{ 
  byte data; 
  
  // Clear Interrupt if LIR enabled
  do {
    data = gyro_SPIReadReg(GYRO_INT1_SRC);    
  } while( (data & INT_SRC_IA) );
  
  
  // BYPASS Mode
  data = gyro_SPIReadReg(GYRO_FIFO_CTRL_REG);
  data = (data & FIFO_CONTROL_WTMSAMP) | DEF_FIFO_CTRL_BYPASS;
  gyro_SPIWriteReg(GYRO_FIFO_CTRL_REG,data);
  
  // Disable FIFO
  data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  data = data & (~CTRL_REG5_FIFO_EN);
  gyro_SPIWriteReg(GYRO_CTRL_REG5,data);
    
/*
  data = gyro_ReadReg(GYRO_CTRL_REG5);
  if( data & CTRL_REG5_STOP_ON_WTM) 
  {
    data = gyro_ReadReg(GYRO_FIFO_CTRL_REG);
    Fifo_depth = (data & FIFO_CONTROL_WTMSAMP) + 1;    
  }  
  else 
  { 
    Fifo_depth = 32;
  }*/
}


/*******************************************************************************
* Function Name  : FifoStream
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_FifoStream(void) 
{

  //volatile byte data;
  volatile byte  dataA[6];
  //data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  Cpu_Delay100US(1000);
  gyro_SPIWriteReg(GYRO_CTRL_REG5,CTRL_REG5_FIFO_EN);
  //data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  //while(data !=CTRL_REG5_FIFO_EN)
  //	  gyro_SPIWriteReg(GYRO_CTRL_REG5,data);
 // Cpu_Delay100US(500);
  gyro_SPIWriteReg(GYRO_CTRL_REG5,0);
  gyro_SPIWriteReg(GYRO_FIFO_CTRL_REG,DEF_FIFO_CTRL_STREAM);
  gyro_SPIWriteReg(GYRO_CTRL_REG5,CTRL_REG5_FIFO_EN);
 
  
  dataA[0] = gyro_SPIReadReg(0x20);
  dataA[1] = gyro_SPIReadReg(0x21);
  dataA[2] = gyro_SPIReadReg(0x22);
  dataA[3] = gyro_SPIReadReg(0x23);
  dataA[4] = gyro_SPIReadReg(0x24);
  dataA[5] = gyro_SPIReadReg(0x2E);

}

/*******************************************************************************
* Function Name  : FifoMode
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_FifoMode(void) 
{
  byte data;
  
  data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  data = (data | CTRL_REG5_FIFO_EN);
  gyro_SPIWriteReg(GYRO_CTRL_REG5,data);
    
  data = gyro_SPIReadReg(GYRO_FIFO_CTRL_REG);
  data = ((data & FIFO_CONTROL_WTMSAMP) | DEF_FIFO_CTRL_FIFO_MODE);
  gyro_SPIWriteReg(GYRO_FIFO_CTRL_REG,data);
  
}


/*******************************************************************************
* Function Name  : FifoRead
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
byte gyro_FifoRead(whole_FIFO *pBuffer) 
{
    byte data,i;
  //FIFO READ : The system send 32 accelerations samples 
  //through the USB interface
  //(FIFO content)
  data = gyro_SPIReadReg(GYRO_FIFO_SRC_REG);
  FIFO_Src = data;
  FIFO_Ovr = FIFO_Src & FIFO_SOURCE_OVRN;

 // FIFO_count = 0;
  if (FIFO_Ovr) 
  {  
    for(i = 0;i< FIFO_DEPTH; i++) 
    {      
      gyro_FIFO_SPIGetRawData(&(pBuffer->rawData[i][0])); 
    }
    return 0;    
  }
  return 1;
}

void gyro_SPIGetDiff(whole_FIFO *pBuffer)
{
  byte i;
  
  for(i=0; i< FIFO_DEPTH-1; i++) 
  {
      pBuffer->samplet[i].X = pBuffer->samplet[i+1].X - pBuffer->samplet[i].X;
      pBuffer->samplet[i].Y = pBuffer->samplet[i+1].Y - pBuffer->samplet[i].Y;
      pBuffer->samplet[i].Z = pBuffer->samplet[i+1].Z - pBuffer->samplet[i].Z;
  }  
}


#if 0
/*******************************************************************************
* Function Name  : FifoBypassToStream
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void gyro_BypassToStream(void) 
{
  byte data;
  
  // Clear Interrupt if LIR enabled
  //do {
    data = gyro_SPIReadReg(GYRO_INT1_SRC);    
  //} while( (data & INT_SRC_IA) );
  
  // Interrupt spend 1/2 samples to go low!!!
			
  data = gyro_SPIReadReg(GYRO_CTRL_REG5);
  data = data | CTRL_REG5_FIFO_EN;
  gyro_SPIWriteReg(GYRO_CTRL_REG5,data);
  
  data = gyro_SPIReadReg(GYRO_FIFO_CTRL_REG);
  data = (data & FIFO_CONTROL_WTMSAMP) | DEF_FIFO_CTRL_BYPASS_TO_STREAM;
  gyro_SPIWriteReg(GYRO_FIFO_CTRL_REG,data);  
}

#endif

 /*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Read
* Description    : read a numByteToRead bytes from SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  readAddr is the register address you want to read from
*                  numByteToRead is the number of bytes to read
* Output         : pBuffer is the buffer that contains bytes read
* Return         : None
*******************************************************************************/

void acc_SPIBufferRead(byte* pBuffer, byte readAddr, byte numByteToRead)
{ 
  byte i;
  
  readAddr |= 0x80;
  if(numByteToRead > 1) 
      readAddr |= 0x40;           // Auto Incremental register Address
  
  ACC_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(readAddr);
  for(i = 0;i<numByteToRead;i++) pBuffer[i] = SPI2_BYTE_IO(Dummy_Byte);  
  ACC_SPI_CS_HIGH();
}

void acc_FIFO_SPIGetRawData(byte *pBuffer) 
{
  byte data;
  
  acc_SPIBufferRead(pBuffer, ACC_OUT_X_L, 6); 
  
  data       = pBuffer[1];
  pBuffer[1] = pBuffer[0];
  pBuffer[0] = data;
  
  data       = pBuffer[3];
  pBuffer[3] = pBuffer[2];
  pBuffer[2] = data;  
  
  data       = pBuffer[5];
  pBuffer[5] = pBuffer[4];
  pBuffer[4] = data; 
  
    //appogg = (int8_t)GyroAccDataBuffer[1];
    //data16 = (int16_t)appogg;
    //data16 = data16<<8;
    //data16 = data16 | (uint16_t)GyroAccDataBuffer[0];
    //data16 = data16>>4;  
  
  
   
}

byte acc_SPIReadReg(byte regAddr) 
{
  byte readValue;
  
  acc_SPIBufferRead(&readValue, regAddr, 1);
  
  return readValue; 
    
}

/*******************************************************************************
* Function Name  : Accelerometer SPI_Byte/Buffer Write
* Description    : write a Byte to SPI Bus
* Input          : deviceAddr is the SPI address of the device
*                  WriteAddr is the register address you want to write to
*                  pBuffer contains bytes to write
* Output         : None
* Return         : None
*******************************************************************************/
void acc_SPIWriteReg(byte regAddr, byte regValue)
{    
  ACC_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(regAddr);
  (void)SPI2_BYTE_IO(regValue);  
  ACC_SPI_CS_HIGH();
 }


void acc_SPIBufferWrite(byte regAddr,byte *writeBuf, byte numByteToWrite) 
{
  byte i;
  
  if(numByteToWrite > 1)
    regAddr |= 0x40;
  
  ACC_SPI_CS_LOW();
  (void)SPI2_BYTE_IO(regAddr);
  for(i = 0;i<numByteToWrite;i++) (void)SPI2_BYTE_IO(writeBuf[i]);  
  ACC_SPI_CS_HIGH();
}



/*******************************************************************************
* Function Name  : SPI2_MEMS_Init
* Description    : I2C Initialization
* Input          : None 
* Output         : None
* Return         : None
*******************************************************************************/
#define INT1_THRESHOLD_VALUE   12                                   //  125 * 12/1000 = 1.5g
void SPI_MEMS_Acc_Config(byte mode)
{  
  //ACC_CTRL_REG2 filter setting
 if(mode == 0)
 {
   acc_SPIWriteReg(ACC_CTRL_REG5,0x80);   // Reboot this acc each time restart, 2014.05.07
   Cpu_Delay100US(1000);                  // wait a while make it working.
   
   // CTRL_REG1
   acc_SPIWriteReg(ACC_CTRL_REG1,ACC_ODR_NORMAL_1344HZ|ACC_XYZAXIS_EN);
   
   // CTRL_REG2
 //acc_SPIWriteReg(ACC_CTRL_REG2,0x09);                            // Enable High-pass filter on data and int1, 2014.05.08

   //ACC_CTRL_REG3 interrupt pin assignment
   acc_SPIWriteReg(ACC_CTRL_REG3,INT1_AOI1); 
   
  //ACC_CTRL_REG4 mode 16G 4
  //acc_SPIWriteReg(ACC_CTRL_REG4,ACC_FULLSCALE_16G); 
  #if ENABLE_FAKE_IMPACT_FILTER
  acc_SPIWriteReg(ACC_CTRL_REG4,ACC_FULLSCALE_16G| ACC_HIGH_RESOLUTION);
  #else
  acc_SPIWriteReg(ACC_CTRL_REG4,ACC_FULLSCALE_16G);  
  #endif

 //ACC_CTRL_REG5 fifo enable 
  #if ENABLE_FAKE_IMPACT_FILTER&&USE_INTERRUPT_FILTER
  acc_SPIWriteReg(ACC_CTRL_REG5,ACC_FIFO_ENABLE|ACC_LIR_INT1_ENABLE );   
  #else
  acc_SPIWriteReg(ACC_CTRL_REG5,ACC_FIFO_ENABLE);     
  #endif
  
  //ACC_CTRL_REG6 interrupt polarity adn INT2_A assignement
  acc_SPIWriteReg(ACC_CTRL_REG6,0);//ACC_INT_ACTIVE_LOW); 
  
    
  //INT1_THS_A
  //acc_SPIWriteReg(ACC_INT1_THS,0x8);                              // How much G is it?
  #if (ENABLE_FAKE_IMPACT_FILTER&&USE_INTERRUPT_FILTER)
    acc_SPIWriteReg(ACC_INT1_THS,INT1_THRESHOLD_VALUE);              
  #else
    acc_SPIWriteReg(ACC_INT1_THS,8);                                  //  8 => 125 * 8/1000 = 1 g
  #endif
  
  
  // Dummy read to force th HP filter to current acceleration/tilt value
  //(void)acc_SPIReadReg() 
    
  //ACC_INT1_CFG interrupt mode
  acc_SPIWriteReg(ACC_INT1_CFG,ACC_INT1_CFG_6DIR_MOVE|XYZ_UP_INT_ENABLE|XYZ_DOWN_INT_ENABLE); 
  //acc_SPIWriteReg(ACC_INT1_DURATION,0x0); 
  
#if (((ENABLE_FAKE_IMPACT_FILTER == 1)&&(USE_INTERRUPT_FILTER==0))||(ENABLE_ACC_DATA_OUTPUT))
   //ACC_FIFO_CTRL_REG fifo into Stream                                          // Added by Jason Chen, 2014.05.08
  acc_SPIWriteReg(ACC_FIFO_CTRL_REG,FIFO_STREAM_A);   
#endif

  
  (void)acc_SPIReadReg(ACC_INT1_SRC);                               // Clear all interrupt flags, 2014.05.08
  
 }
 else if(mode == 1)
 {
    //acc_SPIWriteReg(ACC_CTRL_REG1,ACC_ODR_400HZ|ACC_XYZAXIS_EN);
    acc_SPIWriteReg(ACC_CTRL_REG4,ACC_FULLSCALE_04G | ACC_HIGH_RESOLUTION);     
 } 
 #if ENABLE_FAKE_IMPACT_FILTER&&USE_INTERRUPT_FILTER 
 else if(mode == 2) 
 {
    (void)acc_SPIReadReg(ACC_INT1_SRC);                      // Clear all Interrupt flags
    //acc_SPIWriteReg(ACC_CTRL_REG3,INT1_AOI1);
    //acc_SPIWriteReg(ACC_CTRL_REG4,ACC_FULLSCALE_16G); 
    acc_SPIWriteReg(ACC_CTRL_REG5,0);//ACC_FIFO_ENABLE);
    //acc_SPIWriteReg(ACC_INT1_CFG,ACC_INT1_CFG_6DIR_MOVE|XYZ_UP_INT_ENABLE|XYZ_DOWN_INT_ENABLE);       
    acc_SPIWriteReg(ACC_INT1_THS,0x08);                              // How much G is it?            Jason Chen, 2014.05.07    
    Cpu_Delay100US(60);
 }
 else 
 {
    //INT1_THS_A
    (void)acc_SPIReadReg(ACC_INT1_SRC);                      // Clear all Interrupt flags
    acc_SPIWriteReg(ACC_CTRL_REG4,ACC_FULLSCALE_16G| ACC_HIGH_RESOLUTION); 
    acc_SPIWriteReg(ACC_CTRL_REG5,ACC_FIFO_ENABLE|ACC_LIR_INT1_ENABLE );
    acc_SPIWriteReg(ACC_INT1_THS,INT1_THRESHOLD_VALUE);                              //  40 = 125 * 40/1000 = 5 g      Jason Chen, 2014.05.07    
 }
 #endif
  
 #if 0
  data[0] =  acc_SPIReadReg(ACC_CTRL_REG1);
  data[1] =  acc_SPIReadReg(ACC_CTRL_REG3);
  data[2] =  acc_SPIReadReg(ACC_CTRL_REG4);
  data[3] =  acc_SPIReadReg(ACC_CTRL_REG5);
  data[4] =  acc_SPIReadReg(ACC_CTRL_REG6);
  data[5] =  acc_SPIReadReg(ACC_INT1_CFG);
 #endif
    
}

void SPI_MEMS_Gyro_Config(void)
{  
  
  
  //gyro
  gyro_SPIWriteReg(GYRO_CTRL_REG5,0x80);  
  
  // ODR 760Hz, Normal Mode,  X, Y, Z Axis enbaled
  gyro_SPIWriteReg(GYRO_CTRL_REG1, CTRL_REG1_ODR_760HZ|CTRL_REG1_BW11|CTRL_REG1_NORMALMODE|CTRL_REG1_DEFAULT);
   
  // Continues updata, full scale 2000dps
  gyro_SPIWriteReg(GYRO_CTRL_REG4, CTRL_REG4_SCALE2000|CTRL_REG4_DEFAULT);  
  
  gyro_SPIWriteReg(GYRO_INT1_CFG, 0x3F);  
  gyro_SPIWriteReg(GYRO_INT1_DURATION, 0x0);  
  gyro_SPIWriteReg(GYRO_INT1_TSH_XH, 0x20);  
  gyro_SPIWriteReg(GYRO_INT1_TSH_XL, 0x20);
  gyro_SPIWriteReg(GYRO_INT1_TSH_YH, 0x20);
  gyro_SPIWriteReg(GYRO_INT1_TSH_YL, 0x20);
  gyro_SPIWriteReg(GYRO_INT1_TSH_ZH, 0x20);
  gyro_SPIWriteReg(GYRO_INT1_TSH_ZL, 0x20);
}
#endif
