/******************** (C) COPYRIGHT 2011 ARTAFLEX INC **************************
* File Name          : spi_flash.c
* Author             : Jason Chen
* Version            : V2.0.0
* Date               : 20/07/2011
* Description        : This file provides a set of functions needed to manage the
*                      communication between SPI peripheral and SPI M25P32 FLASH,
*                      or M25P64, or M25P128
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "spi_flash.h"

#pragma MESSAGE DISABLE C2705
/* Private define ------------------------------------------------------------*/
#define ST_WREN     0x06  /* Write enable instruction */
#define ST_WDIS     0x04  /* Write disable instruction */
#define ST_RDID     0x9F  /* Read identification */
#define ST_RDSR     0x05  /* Read Status Register instruction  */
#define ST_WRSR     0x01  /* Write Status Register instruction */
#define ST_READ     0x03  /* Read from Memory instruction */
#define ST_FASTREAD 0x0B  /* Fash Read from Memory instruction */
#define ST_WRITE    0x02  /* Write to Memory instruction */
#define ST_SE       0xD8  /* Sector Erase instruction */
#define ST_BE       0xC7  /* Bulk Erase instruction */


#define WIP_Flag    0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte  0xA5
  
#ifdef USE_SPI1
   #define SPIS           SPI1S
   #define SPIDL          SPI1DL
   #define SPIS_SPTEF     SPI1S_SPTEF
   #define SPIS_SPRF      SPI1S_SPRF
   #define SPIBR          SPI1BR
   #define SPIC1          SPI1C1
   #define SPIC2          SPI1C2
   #define SPIC1_SPE      SPI1C1_SPE
#else
   #define SPIS           SPI2S
   #define SPIDL          SPI2DL
   #define SPIS_SPTEF     SPI2S_SPTEF
   #define SPIS_SPRF      SPI2S_SPRF
   #define SPIBR          SPI2BR
   #define SPIC1          SPI2C1
   #define SPIC2          SPI2C2   
   #define SPIC1_SPE      SPI2C1_SPE   
#endif

dword mMFT_FLASH_ID; 
dword mSPI_FLASH_SectorNum;
dword mSPI_FLASH_SectorSize;
dword mSPI_FLASH_SIZE;
byte mST_DP;
byte mST_RES;



/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
* Function Name  : SPI_FLASH_Init
* Description    : Initializes the peripherals used by the SPI FLASH driver.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
byte SPI_FLASH_Init(bool Enable_SPI)
{
  //dword flashID;
  byte err =0;
   /* FLASH CS Initialization */    
      
  FLASH_CSPE      = 0;                 // Disable Pullup resister
  FLASH_CSDD      = 1;                 // Data Direction --> output
  FLASH_CSSE      = 0;                 // Disable slew rate control
  FLASH_CSDS      = 1;                 // High output drive strength
  
  FLASH_CS_PIN    = 1;                 // Deselect Flash Memory

  #pragma MESSAGE DISABLE C4002        /* Disable warning C4002 "Result not used" */
  
  (void)SPIS;                          /* Read the status register */
  (void)SPIDL;                         /* Read the device register */

  SPI1BR = 0x00 
		   //| SPI2BR_SPPR2_MASK   //prescale
		   //| SPI2BR_SPPR1_MASK     // 1-8
		   //| SPI2BR_SPPR0_MASK
		   //| SPI2BR_SPR2_MASK    //divisor 
		   //| SPI2BR_SPR1_MASK    // 2,4,8,16,32,64,128,256 
		   //| SPI2BR_SPR0_MASK    //
		   ; 
   
  /* SPIxC2: SPMIE=0,SPIMODE=0,??=0,MODFEN=0,BIDIROE=0,??=0,SPISWAI=0,SPC0=0 */
  setReg8(SPIC2, 0x00);               /* Configure the SPI port - control register 2 */ 
  /* SPIxC1: SPIE=0,SPE=0,SPTIE=0,MSTR=1,CPOL=0,CPHA=0,SSOE=0,LSBFE=0 */
  setReg8(SPIC1, 0x10);               /* Configure the SPI port - control register 1 */ 
  
  /* Enable/disable device according to the status flags */
  if (Enable_SPI) 
  {
    SPIC1_SPE = 1;                    /* Enable device */
    #ifdef ENABLE_DEEP_PWR
    err = SPI_FLASH_ResumeFromDeepPowerDown();
    #endif
	#if 0
    flashID = SPI_FLASH_ReadID();
				    
	if(flashID == (dword)MFT_FLASH_ID)
	{
	  mMFT_FLASH_ID = 0xC82016    
      mSPI_FLASH_SectorNum   =0x400
      mSPI_FLASH_SectorSize = 0x1000    
      mSPI_FLASH_SIZE = 0x1000000      
      mST_DP  = 0xB9         
      mST_RES = 0xAB          
    }
	#endif
	// 
	
  }
  else 
  {
    
    SPIC1_SPE = 0;                    /* Disable device */    
	FLASH_CS_PIN = 0;
  }
  return err;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SectorErase
* Description    : Erases the specified FLASH sector.
* Input          : SectorAddr: address of the sector to erase.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_SectorErase(uint32_t SectorAddr)
{
  uint8_t FLASH_Status = 0;
  /* Send write enable instruction */
  SPI_FLASH_WriteEnable();

  /* Sector Erase */
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  /* Send Sector Erase instruction */
  (void)SPI_FLASH_SendByte(ST_SE);
  /* Send SectorAddr high nibble address byte */
  (void)SPI_FLASH_SendByte((SectorAddr & 0xFF0000) >> 16);
  /* Send SectorAddr medium nibble address byte */
  (void)SPI_FLASH_SendByte((SectorAddr & 0xFF00) >> 8);
  /* Send SectorAddr low nibble address byte */
  (void)SPI_FLASH_SendByte(SectorAddr & 0xFF);
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
  
  #if 0 //def SPI_WATCHDOG_ENABLE
  
  SPI_FLASH_CS_LOW();
  (void)SPI_FLASH_SendByte(ST_RDSR);
  /* Wait the end of Flash writing */
  do
  {
    SRS = 0x55;                         /* Reset watchdog counter write 55, AA */
    SRS = 0xAA;
  
    /* Send a dummy byte to generate the clock needed by the FLASH
    and put the value of the status register in FLASH_Status variable */
    FLASH_Status = SPI_FLASH_SendByte(Dummy_Byte);
    
  }
  while ((FLASH_Status & WIP_Flag) == 0x01/*ST_SET*/); /* Write in progress */
     SPI_FLASH_CS_HIGH();
  
  #else
  
    
	/* Wait the end of Flash writing */
	SPI_FLASH_WaitForWriteEnd();

  #endif
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BulkErase
* Description    : Erases the entire FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BulkErase(void)
{
  uint8_t FLASH_Status = 0;
  
  /* Send write enable instruction */
  SPI_FLASH_WriteEnable();

  /* Bulk Erase */
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  /* Send Bulk Erase instruction  */
  (void)SPI_FLASH_SendByte(ST_BE);
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
 #ifdef SPI_WATCHDOG_ENABLE
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read Status Register" instruction */
  (void)SPI_FLASH_SendByte(ST_RDSR);

  /* Loop as long as the memory is busy with a write cycle */
  do
  {
    SRS = 0x55;                         /* Reset watchdog counter write 55, AA */
    SRS = 0xAA;
  
    /* Send a dummy byte to generate the clock needed by the FLASH
    and put the value of the status register in FLASH_Status variable */
    FLASH_Status = SPI_FLASH_SendByte(Dummy_Byte);
    
  }
  while ((FLASH_Status & WIP_Flag) == 0x01/*ST_SET*/); /* Write in progress */

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
#else    
  SPI_FLASH_WaitForWriteEnd();
#endif
}

/*******************************************************************************
* Function Name  : SPI_FLASH_PageWrite
* Description    : Writes more than one byte to the FLASH with a single WRITE
*                  cycle(Page WRITE sequence). The number of byte can't exceed
*                  the FLASH page size.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH,
*                    must be equal or less than "SPI_FLASH_PageSize" value.
* Output         : None
* Return         : None
*******************************************************************************/
//extern void Cpu_Delay100US(word us100);

void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  /* Enable the write access to the FLASH */
  SPI_FLASH_WriteEnable();

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  /* Send "Write to Memory " instruction */
  (void)SPI_FLASH_SendByte(ST_WRITE);
  /* Send WriteAddr high nibble address byte to write to */
  (void)SPI_FLASH_SendByte((WriteAddr & 0xFF0000) >> 16);
  /* Send WriteAddr medium nibble address byte to write to */
  (void)SPI_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
  /* Send WriteAddr low nibble address byte to write to */
  (void)SPI_FLASH_SendByte(WriteAddr & 0xFF);

  /* while there is data to be written on the FLASH */
  while (NumByteToWrite--)
  {
    /* Send the current byte */
    (void)SPI_FLASH_SendByte(*pBuffer);
    /* Point on the next byte to be written */
    pBuffer++;
  }

  /* Deselect the FLASH: Chip Select high */
  //Cpu_Delay100US(10);
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferWrite
* Description    : Writes block of data to the FLASH. In this function, the
*                  number of WRITE cycles are reduced, using Page WRITE sequence.
* Input          : - pBuffer : pointer to the buffer  containing the data to be
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  uint8_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

  Addr        = WriteAddr % SPI_FLASH_PageSize;
  count       = SPI_FLASH_PageSize - Addr;
  NumOfPage   = NumByteToWrite / SPI_FLASH_PageSize;
  NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

  if (Addr == 0)        /* WriteAddr is SPI_FLASH_PageSize aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
    {
      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
    }
    else /* NumByteToWrite > SPI_FLASH_PageSize */
    {
      while (NumOfPage--)
      {
	  	__RESET_WATCHDOG();
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
        WriteAddr +=  SPI_FLASH_PageSize;
        pBuffer += SPI_FLASH_PageSize;
      }

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
    }
  }
  else /* WriteAddr is not SPI_FLASH_PageSize aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PageSize */
    {
      if (NumOfSingle > count) /* (NumByteToWrite + WriteAddr) > SPI_FLASH_PageSize */
      {
        temp = NumOfSingle - count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
        WriteAddr +=  count;
        pBuffer += count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, temp);
      }
      else
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
      }
    }
    else /* NumByteToWrite > SPI_FLASH_PageSize */
    {
      NumByteToWrite -= count;
      NumOfPage =  NumByteToWrite / SPI_FLASH_PageSize;
      NumOfSingle = NumByteToWrite % SPI_FLASH_PageSize;

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
      WriteAddr +=  count;
      pBuffer += count;

      while (NumOfPage--)
      {
	  	__RESET_WATCHDOG();
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PageSize);
        WriteAddr +=  SPI_FLASH_PageSize;
        pBuffer += SPI_FLASH_PageSize;
      }

      if (NumOfSingle != 0)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
      }
    }
  }
}
 
//uint16_t startWriteSector;
uint32_t startWriteAddr;                        // Aligned Sector Boundary
uint32_t lastWritteAddr   = SPI_FLASH_SIZE;     // no data wrriten
//uint16_t lastWritteSector = 0;

/*******************************************************************************
* Function Name  : SPI_FLASH_BufferRead
* Description    : Reads a block of data from the FLASH.
* Input          : - pBuffer : pointer to the buffer that receives the data read
*                    from the FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - NumByteToRead : number of bytes to read from the FLASH.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read from Memory " instruction */
  (void)SPI_FLASH_SendByte(ST_READ);

  /* Send ReadAddr high nibble address byte to read from */
  (void)SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
  /* Send ReadAddr medium nibble address byte to read from */
  (void)SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
  /* Send ReadAddr low nibble address byte to read from */
  (void)SPI_FLASH_SendByte(ReadAddr & 0xFF);

  while (NumByteToRead--) /* while there is data to be read */
  {
    /* Read a byte from the FLASH */
    *pBuffer = SPI_FLASH_SendByte(Dummy_Byte);
    /* Point to the next location where the byte read will be saved */
    pBuffer++;
  }

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadID
* Description    : Reads FLASH identification.
* Input          : None
* Output         : None
* Return         : FLASH identification
*******************************************************************************/
uint32_t SPI_FLASH_ReadID(void)
{
  uint32_t Temp = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "RDID " instruction */
  (void)SPI_FLASH_SendByte(0x9F);

  /* Read a byte from the FLASH */
  Temp0 = SPI_FLASH_SendByte(Dummy_Byte);

  /* Read a byte from the FLASH */
  Temp1 = SPI_FLASH_SendByte(Dummy_Byte);

  /* Read a byte from the FLASH */
  Temp2 = SPI_FLASH_SendByte(Dummy_Byte);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  Temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;

  return Temp;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_StartReadSequence
* Description    : Initiates a read data byte (READ) sequence from the Flash.
*                  This is done by driving the /CS line low to select the device,
*                  then the READ instruction is transmitted followed by 3 bytes
*                  address. This function exit and keep the /CS line low, so the
*                  Flash still being selected. With this technique the whole
*                  content of the Flash is read with a single READ instruction.
* Input          : - ReadAddr : FLASH's internal address to read from.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read from Memory " instruction */
  (void)SPI_FLASH_SendByte(ST_READ);

  /* Send the 24-bit address of the address to read from -----------------------*/
  /* Send ReadAddr high nibble address byte */
  (void)SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
  /* Send ReadAddr medium nibble address byte */
  (void)SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
  /* Send ReadAddr low nibble address byte */
  (void)SPI_FLASH_SendByte(ReadAddr & 0xFF);
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadByte
* Description    : Reads a byte from the SPI Flash.
*                  This function must be used only if the Start_Read_Sequence
*                  function has been previously called.
* Input          : None
* Output         : None
* Return         : Byte Read from the SPI Flash.
*******************************************************************************/
uint8_t SPI_FLASH_ReadByte(void)
{
  return (SPI_FLASH_SendByte(Dummy_Byte));
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendByte
* Description    : Sends a byte through the SPI interface and return the byte
*                  received from the SPI bus.
* Input          : byte : byte to send.
* Output         : None
* Return         : The value of the received byte.
*******************************************************************************/
uint8_t SPI_FLASH_SendByte(uint8_t data_byte)
{
#if 1
  while( !SPIS_SPTEF );   // SPI Transmit Buffer Empty Flag
  SPIDL = data_byte;      // send the data

  // wait while no data available in the receive data buffer
  while( !SPIS_SPRF );    // SPI Read Buffer Full Flag
  return (byte)SPIDL;     // return the received data
#else
  byte Chr;
  while(SPIM_Radio_SendChar(data_byte)!=ERR_OK);
  while(SPIM_Radio_GetCharsInRxBuf() == 0);
  while(SPIM_Radio_RecvChar(&Chr)!= ERR_OK);
  return Chr;
#endif
  
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteEnable
* Description    : Enables the write access to the FLASH.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WriteEnable(void)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Write Enable" instruction */
  (void)SPI_FLASH_SendByte(ST_WREN);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
}

#ifdef ENABLE_DEEP_PWR
/*******************************************************************************
* Function Name  : SPI_FLASH_DeepPowerDown
* Description    : Make flash into Deep power down mode
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_DeepPowerDown(void)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Deep Power Down" instruction */
  (void)SPI_FLASH_SendByte(ST_DP);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
}
#define M25_RES_SIGNATURE 0x15
/*******************************************************************************
* Function Name  : SPI_FLASH_ResumeFromDeepPowerDown
* Description    : Make flash wake up frpm Deep power down mode
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
byte  SPI_FLASH_ResumeFromDeepPowerDown(void)
{
  static byte tmp;
  static byte mySig =0;
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  //Cpu_Delay100US(1);
  /* Send "Resume from Deep Power Down" instruction */
  (void)SPI_FLASH_SendByte(ST_RES);
  (void)SPI_FLASH_SendByte(0x55);
  (void)SPI_FLASH_SendByte(0x55);
  (void)SPI_FLASH_SendByte(0x55);
  mySig = SPI_FLASH_SendByte(0x55);
  //Cpu_Delay100US(1);
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
  tmp = M25_RES_SIGNATURE == mySig ? 1 : 0;
  return tmp;
}

#endif

/*******************************************************************************
* Function Name  : SPI_FLASH_WaitForWriteEnd
* Description    : Polls the status of the Write In Progress (WIP) flag in the
*                  FLASH's status  register  and  loop  until write  opertaion
*                  has completed.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WaitForWriteEnd(void)
{
  uint8_t FLASH_Status = 0;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read Status Register" instruction */
  (void)SPI_FLASH_SendByte(ST_RDSR);

  /* Loop as long as the memory is busy with a write cycle */
  do
  {
    /* Send a dummy byte to generate the clock needed by the FLASH
    and put the value of the status register in FLASH_Status variable */
    FLASH_Status = SPI_FLASH_SendByte(Dummy_Byte);

  }
  while ((FLASH_Status & WIP_Flag) == 0x01/*ST_SET*/); /* Write in progress */

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
}

/*******************************************************************************
* Function Name  : SPI_FLASH_ReadStatusReg
* Description    : Read flash status register
* Input          : None
* Output         : None
* Return         : Value of Status register
*******************************************************************************/
uint8_t SPI_FLASH_ReadStatusReg(void)
{
  uint8_t FLASH_Status = 0;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read Status Register" instruction */
  (void)SPI_FLASH_SendByte(ST_RDSR);

  FLASH_Status = SPI_FLASH_SendByte(Dummy_Byte);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
  
  return FLASH_Status;
}

/*******************************************************************************
* Function Name  : SPI_FLASH_WriteStatusReg
* Description    : Write flash status register
* Input          : Value writen
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_WriteStatusReg(uint8_t statusValue)
{
  /* Enable the write access to the FLASH */
  SPI_FLASH_WriteEnable();

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Status Register CMD and value" instruction */
  (void)SPI_FLASH_SendByte(ST_WRSR);
  (void)SPI_FLASH_SendByte(statusValue);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();
  
  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();  
}

/******************* (C) COPYRIGHT 2011 ARTAFLEX INC *****END OF FILE****/
