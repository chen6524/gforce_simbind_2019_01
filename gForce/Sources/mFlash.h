/******************************************************************************
**
*  
*
* History:
*
* 07/04/2011 -- first version, ver.1.0  H.YAN
*
*
*******************************************************************************
*/

#ifndef _MFLASH_H
#define _MFLASH_H
#include "mImpact.h"
#include "Spi_flash.h"

#define SPIFLASH_RD_NOMORE 0x0
#define SPIFLASH_RD_SUCESS 0x1
#define SPIFLASH_RD_FAILD  0x3

#define SPIFLASH_DATA_START_ADR 0x0 

#define MFLSH_SECTER_USR_PROFILE 0x0

typedef struct 
{
  	ACC_DATA  flashData;
  	ACC_ENTRY flashEntry;
  	dword     accDataAdr;
    dword     accEntryAdr;
  	byte      accDataCnt;
    word      accEntryCnt; 
  	byte      lastSid;	
  	
  	word      accEntryMagicCnt;              // Added by Jason Chen, 2013.12.11
  	
} SPI_FLASH_CUR_RD_BUFF;

extern SPI_FLASH_CUR_RD_BUFF spiFlashCurRdBuffer;
extern dword spiFlashCurWtAddr;
extern word curSessionIndex;
byte spiFlashGetCurWtAddr(void);

void spiMyFlashGetCurWtAddr(void);           // Added by Jason Chen, 2014.03.20

byte  spiFlashReadFirstAccEntry(void);

byte  spiFlashReadNextAccEntry(void);

byte  spiFlashReadFirstAccData(void);

byte  spiFlashReadNextAccData(void);

byte spiFlashWriteAccBuffer(ACC_ENTRY* pEntry);

byte spiFlashEraseFlash(void);

byte spiFlashRead(byte * dst, dword adr, byte cnt);

byte  spiFlashReadAccEntry(word index);


void profileSave(void);
uint32_t spiFlashGetID(void);


#endif
